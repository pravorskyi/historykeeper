#ifndef MESSAGEMODEL_H
#define MESSAGEMODEL_H

#include "../core/contact.h"
#include "../core/message.h"
#include <QtCore/QAbstractListModel>

class MessageModel: public QAbstractListModel
{
    Q_OBJECT
public:
    explicit MessageModel(QObject* parent = 0);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

    void setMessages(const QVector<Message>&);
    void setContacts(const QHash<AccountID, QHash<ContactID, Contact>>&);

    QVector<Message> messages() const { return _messages; }
    
    
private:
    QVector<Message> _messages;
    QHash<AccountID, QHash<ContactID, Contact>> _contacts;
};

#endif

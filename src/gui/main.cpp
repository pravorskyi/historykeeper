#include "mainwindow.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>
#include <iostream>
#include <qrichlogger.h>
#include <unistd.h>

const char APP_NAME[] = "HistoryKeeper";

int main(int argc, char *argv[])
{
    QApplication::setApplicationName(APP_NAME);
    QApplication a(argc, argv);

    MainWindow w;
    if(!w.init()) {
        LOG(LOG_EMERG, "Can't init MainWindow");
        //qWarning("Can't init MainWindow");
        //QMessageBox::critical(nullptr, "Error", "Can't init MainWindow");
        return EXIT_FAILURE;
    }
    w.show();

    return a.exec();
}

#ifndef METACONTACT_H
#define METACONTACT_H
#include "hkcore_global.h"

#include <QtCore/QHash>
#include <QtCore/QSet>
#include <QtCore/QString>

#include "typedefs.h"

//TODO: make profiling with and withou d-pointer

class HK_EXPORT Metacontact
{
public:
    Metacontact();
    int id;
    QString name;
    QString description;
    
    QHash<AccountID, QSet<ContactID>> contacts;
};

#endif // METACONTACT_H

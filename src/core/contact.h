#ifndef CONTACT_H
#define CONTACT_H

#include "typedefs.h"
#include <QtCore/QHash>
#include <QtCore/QString>
#include <QtCore/QVariant>


class Contact
{    
public:    
    ContactID id;
    QString login;
    QString name;
    QHash<int32_t, QVariant> attributes;
};

#endif
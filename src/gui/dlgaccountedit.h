#ifndef DLGACCOUNTEDIT_H
#define DLGACCOUNTEDIT_H

#include "accountsettings.h"
#include <QtWidgets/QDialog>

class WpgAccountCommonSettings;
class DlgPluginSettings;

namespace Ui {
class DlgAccountEdit;
}

class DlgAccountEdit : public QDialog
{
    Q_OBJECT

public:
    explicit DlgAccountEdit(QWidget *parent = 0);
    ~DlgAccountEdit();
    
    void setSettings(const AccountSettings&);
    void setDlgPluginSettings(DlgPluginSettings*);
    
    AccountSettings settings() const;

private:
    Ui::DlgAccountEdit *ui;
    WpgAccountCommonSettings* _pgCommon = nullptr;
    DlgPluginSettings* _pgPlugin = nullptr;
};

#endif // DLGACCOUNTEDIT_H

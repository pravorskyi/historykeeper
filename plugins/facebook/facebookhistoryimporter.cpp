#include "facebookhistoryimporter.h"
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtCore/QJsonParseError>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonValue>

enum RequestAttr {
    RequestAttrType = 1000
};


enum RequestType {
    RequestTypeMe,
    RequestTypeInbox
};

FacebookHistoryImporter::FacebookHistoryImporter(const IFactoryPlugin *parent) :
    IHistoryImporter(parent)
{
    _nam = new QNetworkAccessManager(this);
}

void FacebookHistoryImporter::init()
{
}


void FacebookHistoryImporter::sync()
{
    getUserID();
}


void FacebookHistoryImporter::setSettings(const QMap<QByteArray, QString> &settings)
{
    _token = settings.value("Token", "").toLatin1();
}


void FacebookHistoryImporter::getUserID()
{
    QUrl url(QString("https://graph.facebook.com/me?access_token=%1").arg(QString::fromLatin1(_token)));
    QNetworkRequest request(url);
    request.setAttribute((QNetworkRequest::Attribute)RequestAttrType, RequestTypeMe);
    
    QNetworkReply *reply = _nam->get(request);
    connect(reply, &QNetworkReply::finished, this, &FacebookHistoryImporter::onRequestFinished);
    connect(reply, static_cast<void (QNetworkReply:: *)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
        this, &FacebookHistoryImporter::onRequestError);
}


void FacebookHistoryImporter::getMessages()
{
    QUrl url(QString("https://graph.facebook.com/me/threads?access_token=%1").arg(QString::fromLatin1(_token)));
    QNetworkRequest request(url);
    request.setAttribute((QNetworkRequest::Attribute)RequestAttrType, RequestTypeInbox);
    
    QNetworkReply *reply = _nam->get(request);
    connect(reply, &QNetworkReply::finished, this, &FacebookHistoryImporter::onRequestFinished);
    connect(reply, static_cast<void (QNetworkReply:: *)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
        this, &FacebookHistoryImporter::onRequestError);
}


void FacebookHistoryImporter::onRequestFinished()
{
    QNetworkReply *r = static_cast<QNetworkReply*>(sender());
    QByteArray data = r->readAll();
    qDebug(data.constData());
    
    QJsonParseError jsonParseError;
    const QJsonDocument jsonDoc = QJsonDocument::fromJson(data, &jsonParseError);
    if(jsonParseError.error != QJsonParseError::NoError) {
        qDebug("Error! Parse JSON fail — %s. Data:\n\n%s", qPrintable(jsonParseError.errorString()), data.constData());
        return;
    }
       
    const QJsonObject jsonObj = jsonDoc.object();
    
    switch(r->request().attribute((QNetworkRequest::Attribute)RequestAttrType).toInt()) {
    case RequestTypeMe:
        onApiMe(jsonObj);
        break;
    case RequestTypeInbox:
        onApiInbox(jsonObj);
        break;
    }
    r->deleteLater();
}


void FacebookHistoryImporter::onRequestError()
{
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    qDebug("QUERY %s", qPrintable(reply->request().url().toString()));
    qDebug("ERROR CODE %s", qPrintable(reply->errorString()));
    reply->deleteLater();
}


void FacebookHistoryImporter::onApiMe(const QJsonObject &obj)
{
    qDebug(Q_FUNC_INFO);
    _userID = obj.value("id").toString().toLongLong();
    
    getMessages();
}


void FacebookHistoryImporter::onApiInbox(const QJsonObject &obj)
{
    //QJsonDocument doc(obj);
    //qDebug(doc.toJson(QJsonDocument::Indented).constData());
    QVector<PluginMessage> messages;
    QVector<PluginContact> contacts;
    
    QJsonArray threads = obj.value("data").toArray();
    for(const auto &v : threads) {
        QJsonObject t = v.toObject();
        //QJsonDocument doc(t);
        //qDebug(doc.toJson(QJsonDocument::Indented).constData());
        //qDebug("===============================================================");
        
        QJsonObject jmessagesObj = t.value("messages").toObject();
        QJsonArray jmessages = jmessagesObj.value("data").toArray();
        
        for(const auto &v_m : jmessages) {
            QJsonObject jmessage = v_m.toObject();
            QJsonDocument doc(jmessage);
            //qDebug(doc.toJson(QJsonDocument::Indented).constData());
            //qDebug("===============================================================");
            
            PluginMessage message;
            // Message ID
            {
                QString strID = jmessage.value("id").toString();
                if(!strID.startsWith("m_id.")) {
                    qDebug("%s:%d strange message id: '%s'", Q_FUNC_INFO, __LINE__, qPrintable(strID));
                    continue;
                }
                strID = strID.right(strID.size() - 5);
                bool ok = false;
                message.id = strID.toLongLong(&ok);
                if(!ok) {
                    qDebug("%s:%d non-int message id: '%s'", Q_FUNC_INFO, __LINE__, qPrintable(strID));
                    continue;
                }
                
            }
            QString dateStr = jmessage.value("created_time").toString();
            message.date = QDateTime::fromString(dateStr, Qt::ISODate);
            
            message.body = jmessage.value("message").toString();
            
            PluginContact contact;
            // From
            {
                QJsonObject jfrom = jmessage.value("from").toObject();
                ContactID contact_id = jfrom.value("id").toString().toLongLong();
                if(contact_id != _userID) {
                    message.out = false;
                    message.contact_id = contact_id;
                    contact.id = message.contact_id;
                    contact.name = jfrom.value("name").toString();
                } else {
                    message.out = true;
                    QJsonObject toObj = jmessage.value("to").toObject();
                    QJsonArray receivers = toObj.value("data").toArray();
                    for(const auto &receiver : receivers) {
                        QJsonObject receiverObj = receiver.toObject();
                        contact_id = receiverObj.value("id").toString().toLongLong();
                        if(contact_id == _userID)
                            continue;
                        message.contact_id = contact_id;
                        contact.id = message.contact_id;
                        contact.name = receiverObj.value("name").toString();
                        break;
                    }
                }
                
            }
            
            messages.append(message);
            contacts.append(contact);
        }
        
    }
    
    
    qDebug(Q_FUNC_INFO);
    emit newContacts(contacts);
    emit newMessages(messages);
    emit importFinished();
}

#ifndef DLGVKSETTINGS_H
#define DLGVKSETTINGS_H

#include "IPlugin.h"

namespace Ui {
class DlgVkSettings;
}

class DlgVkSettings : public DlgPluginSettings
{
    Q_OBJECT

public:
    explicit DlgVkSettings(QWidget *parent = 0);
    ~DlgVkSettings();
    
    bool isValid() const override;
    QMap<QByteArray, QString> settings() const override;
    
    void setSettings(const QMap<QByteArray, QString> &settings) override;

private:
    Ui::DlgVkSettings *ui;
    
    void onAuthTypeChanged();
    void onDataChanged();
    void onTokenRequest();
};

#endif // DLGVKSETTINGS_H

#ifndef DLGPSISETTINGS_H
#define DLGPSISETTINGS_H

#include "IPlugin.h"

namespace Ui {
class DlgPsiSettings;
}

class DlgPsiSettings : public DlgPluginSettings
{
    Q_OBJECT
    
public:
    explicit DlgPsiSettings(QWidget *parent = 0);
    ~DlgPsiSettings() override;
    
    bool isValid() const override;
    QMap<QString, QString> settings() const override;
    
    void setSettings(const QMap<QString, QString> &settings) override;
    
private:
    Ui::DlgPsiSettings *ui;
};

#endif // DLGPSISETTINGS_H

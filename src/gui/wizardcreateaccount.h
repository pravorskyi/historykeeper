#ifndef WIZARDCREATEACCOUNT_H
#define WIZARDCREATEACCOUNT_H

#include <QtWidgets/QWizard>
#include "typedefs.h"
#include "IPlugin.h"

class WpgAccountCommonSettings;
class WpgAccountPluginSettings;
class WpgCreateAccount;

class WizardCreateAccount: public QWizard
{
    Q_OBJECT
public:
    explicit WizardCreateAccount(const QHash<PluginID, IFactoryPlugin*> &plugins, QWidget *parent = 0);
    
    PluginID pluginId();
    QMap<QByteArray, QString> commonSettings() const;
    QMap<QByteArray, QString> pluginSettings() const;
protected:
    void initializePage(int id) override;

private:
    enum PAGE_INDEX
    {
        PG_CREATE               = 0,
        PG_COMMON_SETTINGS      = 1,
        PG_PLUGIN_SETTINGS      = 2
    };
    
    
    QHash<PluginID, IFactoryPlugin*> _plugins;
    WpgCreateAccount* _wpgCreateAccount                 = nullptr;
    WpgAccountCommonSettings* _wpgCommonSettings        = nullptr;
    WpgAccountPluginSettings* _wpgPluginSettings        = nullptr;
};

#endif // WIZARDCREATEACCOUNT_H
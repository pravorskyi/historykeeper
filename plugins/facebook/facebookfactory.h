#ifndef FACEBOOKFACTORY_H
#define FACEBOOKFACTORY_H

#include "IPlugin.h"

class FacebookFactory : public QObject, IFactoryPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "com.HistoryKeeper.FactoryPlugin/1.0")
    Q_INTERFACES(IFactoryPlugin)
public:
    virtual ~FacebookFactory() { };
    bool hasFeature(HistoryImporter::Features feature) const override;
    IHistoryImporterPtr create() override;
    const QString       prettyName() const override;
    const QString       description() const override;
    const QByteArray    systemName() const override;
    const QString       version() const override;
    
    DlgPluginSettings*  dlgSettings()    const   override;
    QIcon*              icon()           const   override;
};

#endif // FACEBOOKFACTORY_H
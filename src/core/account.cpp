#include "account.h"

Account::Account(AccountID a_id, const IHistoryImporterPtr &importer) :
    _id(a_id),
    _importer(importer)
{
}


void Account::setSettings(const AccountSettings &s)
{
    _settings = s;
    _importer->setSettings(_settings.value(ACCOUNT_SETTING_PLUGIN));
}
#ifndef HKCORE_H
#define HKCORE_H

#include "../lib/plugincontact.h"
#include "accountptr.h"
#include "contact.h"
#include "db.h"
#include "hkcore_global.h"
#include "../../include/IPlugin.h"
#include "../../include/typedefs.h"

#include <QtCore/QObject>
#include <QtCore/QThread>

using std::unique_ptr;

class Account;
class QDir;
class Sync;
class QTranslator;

class HK_EXPORT HKCore : public QObject
{
    Q_OBJECT
public:
    //ctor
    explicit HKCore(QObject *parent = 0);
    //dtor
    ~HKCore() override;
    
    QString getHtml(const QVector<Message>&) const;
    QString getXML(const QVector<Message>&) const;
    QString getJSON(const QVector<Message>&) const;

    bool init();
    AccountConstPtr                     account(AccountID)      const;
    QHash<AccountID, AccountPtr>   accounts() const;
    QHash<PluginID, IFactoryPlugin*>    plugins()               const;
    QHash<MetacontactID, Metacontact>   metacontacts()          const;
    
    void addAccount(PluginID, AccountSettings);
    void deleteAccount(AccountID);
    void editAccount(AccountID, AccountSettings settings);

    void deleteAllMessages(AccountID);
    
    void syncAllAccounts();
    void syncAccount(AccountID);
    
    QHash<ContactID, Contact>   accountContacts(AccountID) const;
    QVector<Message>            messages(AccountID, ContactID) const;
    
    int                         addMetacontact(const QString &name);
    void                        metacontactAddContact(MetacontactID, AccountID, ContactID);
    void                        metacontactRemove(MetacontactID);
    QHash<AccountID, QHash<ContactID, Contact>>  metacontactContacts(MetacontactID);
    QVector<Message>            metacontactMessages(MetacontactID);
    
    QIcon* accountIcon(AccountID) const;

    /*SYNC DEVICES*/
    void syncDevices();
    void createSyncAccount(const QString &login, const QString &password);

private:
    QHash<AccountID, AccountPtr>                _accounts;
    QHash<AccountID, QThread*>                  _accountsThreads;
    QHash<IHistoryImporter*, AccountID>         _accountsImporters;
    QHash<PluginID, IFactoryPlugin*>            _plugins;
    QHash<PluginID, QTranslator*>               _pluginsTranslators;
    QHash<AccountID, QHash<ContactID, Contact>> _contacts;
    Db*                                         _db             = nullptr;
    QThread*                                    _dbThread       = nullptr;
    unique_ptr<Sync>                            _sync;
    QString                                     _currentLanguage;
    unique_ptr<QTranslator>                     _translator;
    unique_ptr<QTranslator>                     _translatorQt;

    void createAccount(PluginID, AccountID, const AccountSettings&);
    void initializePlugin(QObject *pluginObject, QString filename, const QMap<QString, Db::Plugin> &plugins);
    void loadLanguages();
    void loadLanguage(const QString& language);
    void loadPlugins();
    void loadPlugin(const QString &filename, const QDir &dir, const QMap<QString, Db::Plugin> &plugins);
    void switchTranslator(QTranslator& translator, const QString& filename);
    
    void onAccountNewContacts(QVector<PluginContact>);
    void onAccountNewMessages(QVector<PluginMessage>);
    void onAccountImportStarted();
    void onAccountImportProgress(int8_t progress);
    void onAccountImportFinished();
    void onAccountError(const QString errorString);
    
    void onSyncNewContacts(QVector<Contact>);

signals:
    void sigAccountAdd(AccountID);
    void sigAccountDeleted(AccountID);
    void sigAccountError(AccountID, const QString errorString); 
    void sigAccountImportStarted(AccountID);
    void sigAccountImportProgress(AccountID, int32_t progress); // percent
    void sigAccountImportFinished(AccountID);
};

#endif // HKCORE_H

#ifndef SKYPEHISTORYIMPORTER_H
#define SKYPEHISTORYIMPORTER_H

#include "IPlugin.h"
#include <cstdint>

#include <QtCore/QDateTime>
#include <QtCore/QLinkedList>
#include <QtSql/QSqlDatabase>
#include <sys/types.h>

class SkypeHistoryImporter : public IHistoryImporter
{
    Q_OBJECT
public:
    SkypeHistoryImporter(const IFactoryPlugin *parent);
    ~SkypeHistoryImporter();

    void init() { };
    void sync();
    virtual void setSettings(const QMap<QByteArray, QString>& settings) override;

    
private:   
    QString _path;
    
    static QVector<PluginMessage> importMessages(const QSqlDatabase &db, const QString &account);
    static QVector<PluginContact> importContacts(const QSqlDatabase &db, const QString &account);
};

#endif // SKYPEHISTORYIMPORTER_H

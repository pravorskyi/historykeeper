#include "utils.h"

#include <QtWidgets/QMessageBox>

void Utils::processError(const QString& str)
{
    QMessageBox::critical(0, "Error!", str);
}
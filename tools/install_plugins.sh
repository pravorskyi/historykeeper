#!/bin/bash
TARGET=~/.local/share/HistoryKeeper/plugins
mkdir -p $TARGET

rm $TARGET/lib*.so
cp ../build/plugins/*/lib*.so $TARGET

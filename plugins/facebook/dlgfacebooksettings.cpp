#include "dlgfacebooksettings.h"
#include "ui_dlgfacebooksettings.h"
#include "dlgfacebookauth.h"

DlgFacebookSettings::DlgFacebookSettings(QWidget *parent) :
    DlgPluginSettings(parent),
    ui(new Ui::DlgFacebookSettings)
{
    ui->setupUi(this);
    connect(ui->btnGetToken, &QPushButton::clicked, this, &DlgFacebookSettings::onTokenRequest);
}

DlgFacebookSettings::~DlgFacebookSettings()
{
    delete ui;
}


bool DlgFacebookSettings::isValid() const
{
    if(ui->lblToken->text() == "<none>" || ui->lblToken->text().isEmpty())
        return false;
    
    return true;
}


QMap<QByteArray, QString> DlgFacebookSettings::settings() const
{
    QMap<QByteArray, QString> map;
    map["Token"] = ui->lblToken->text();
    return map;
}


void DlgFacebookSettings::setSettings(const QMap<QByteArray, QString> &settings)
{
    ui->lblToken->setText(settings.value("Token", "<none>"));
}


void DlgFacebookSettings::onTokenRequest()
{
    QPointer<DlgFacebookAuth> dlg = new DlgFacebookAuth;
    dlg->exec();
    ui->lblToken->setText(dlg->accessToken());
    delete dlg;
    emit completeChanged();
}

void DlgFacebookSettings::onDataChanged()
{
    emit completeChanged();
}
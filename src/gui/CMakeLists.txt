project(historykeeper CXX)

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5WebKit REQUIRED)
find_package(Qt5WebKitWidgets REQUIRED)
find_package(Qt5LinguistTools REQUIRED)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

include_directories(
    ${HistoryKeeper_BINARY_DIR}/HistoryKeeperCore
    ${HistoryKeeper_BINARY_DIR}/HistoryKeeperLib
    ../../include/
)


set (HEADERS
    ../../include/IPlugin.h
    guisettings.h
    htmldelegate.h
    mainwindow.h
    messagemodel.h
    roster.h
    rosterprivate.h
    dlgaccountedit.h
    dlgadvancedsearch.h
    dlgsyncsettings.h
    wizardcreateaccount.h
)

set (SOURCES
    dlgaccountedit.cpp
    dlgaccounts.cpp
    dlgplugins.cpp
    dlgsyncsettings.cpp
    dlgadvancedsearch.cpp
    guisettings.cpp
    htmldelegate.cpp
    main.cpp
    mainwindow.cpp
    messagemodel.cpp
    roster.cpp
    rosterprivate.cpp
    wizardcreateaccount.cpp
    wpgcreateaccount.cpp
    wpgaccountcommonsettings.cpp
    wpgaccountpluginsettings.cpp
)


set (GUI_UI
    dlgaccountedit.ui
    dlgaccounts.ui
    dlgplugins.ui
    dlgsyncsettings.ui
    dlgadvancedsearch.ui
    mainwindow.ui
    wpgcreateaccount.ui
    wpgaccountcommonsettings.ui
)

set(GUI_RESOURCE
    ${CMAKE_CURRENT_SOURCE_DIR}/resources/res.qrc
)

qt5_add_resources(GUI_RESOURCES ${GUI_RESOURCE})

qt5_wrap_ui(GUI_H ${GUI_UI})

add_executable ( ${PROJECT_NAME} ${SOURCES} ${HEADERS} ${GUI_H} ${GUI_RESOURCES} )
target_link_libraries( ${PROJECT_NAME} HistoryKeeperCore HistoryKeeperLib QRichLogger)
qt5_use_modules(${PROJECT_NAME} Widgets Core WebKit WebKitWidgets)

if(WIN32)
    set(ENDING "dll")
endif(WIN32)

if(UNIX)
    set(ENDING "so")
endif(UNIX)

add_custom_command( TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
    "${CMAKE_CURRENT_BINARY_DIR}/../lib/libHistoryKeeperLib.${ENDING}"
    "${CMAKE_CURRENT_BINARY_DIR}/"
)

add_custom_command( TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
    "${CMAKE_CURRENT_BINARY_DIR}/../core/libHistoryKeeperCore.${ENDING}"
    "${CMAKE_CURRENT_BINARY_DIR}/"
)


add_subdirectory(localization)

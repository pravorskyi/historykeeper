#ifndef DLGPLUGINS_H
#define DLGPLUGINS_H

#include "typedefs.h"
#include <QtWidgets/QDialog>

namespace Ui
{
class DlgPlugins;
}

class IFactoryPlugin;
class QListWidgetItem;

class DlgPlugins : public QDialog
{
    Q_OBJECT
public:
    explicit DlgPlugins(QWidget *parent = 0);
    void setPlugins(const QHash<PluginID, IFactoryPlugin*> &list);

private:
    Ui::DlgPlugins* ui;
    QHash<PluginID, IFactoryPlugin*> _plugins;
};

#endif // DLGPLUGINS_H

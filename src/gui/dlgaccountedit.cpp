#include "dlgaccountedit.h"
#include "ui_dlgaccountedit.h"
#include "wpgaccountcommonsettings.h"
#include "IPlugin.h"


DlgAccountEdit::DlgAccountEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgAccountEdit)
{
    ui->setupUi(this);
    _pgCommon = new WpgAccountCommonSettings;
    _pgCommon->setParent(ui->tabCommon);
}

DlgAccountEdit::~DlgAccountEdit()
{
    delete _pgCommon;
    delete ui;
}


void DlgAccountEdit::setSettings(const AccountSettings &s)
{
    _pgCommon->setSettings(s.value(ACCOUNT_SETTING_COMMON));
    _pgPlugin->setSettings(s.value(ACCOUNT_SETTING_PLUGIN));
}


void DlgAccountEdit::setDlgPluginSettings(DlgPluginSettings* dlg)
{
    _pgPlugin = dlg;
    _pgPlugin->setParent(ui->tabPlugin);
}


AccountSettings DlgAccountEdit::settings() const
{
    AccountSettings s;
    s[ACCOUNT_SETTING_COMMON] = _pgCommon->settings();
    s[ACCOUNT_SETTING_PLUGIN] = _pgPlugin->settings();
    return s;
}
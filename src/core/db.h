#ifndef DB_H
#define DB_H

#include "accountsettings.h"
#include "typedefs.h"
#include <memory>
#include <QtCore/QObject>

class Contact;
class Message;
class Metacontact;
class PluginContact;
class PluginMessage;
class QSqlDatabase;

namespace hksync {
    class SyncContact;
}


class Db : public QObject
{
    Q_OBJECT
public:
    enum class Type {
        SQLite
    };
    
    explicit Db() = delete;
    explicit Db(const Type &);
    ~Db();
    
    void setDatabaseName(const QString &name);
    
    bool open();
    
    bool init();
    bool createTables();
    bool createPluginTables(const PluginID p_id);
    
    //PLUGINS
    struct Plugin {
        PluginID id;
        QString name;
        bool enabled;
    };
    
    QMap<QString, Plugin> plugins() const;    
    
    //ACCOUNTS
    struct Account {
        AccountID id;
        PluginID pluginID;
        QString name;
    };
    
    AccountID                           addAccount(AccountID, const AccountSettings&);
    AccountID                           account(const QString &name);
    bool                                deleteAccount(AccountID);
    void                                editAccount(AccountID, const AccountSettings&);
    QHash<AccountID, Account>           accounts() const;
    AccountSettings                     accountSettings(AccountID);
    
    // MESSAGES
    void                                addMessages(AccountID, QVector<PluginMessage> messages);
    QVector<Message>                    messages(const AccountID, const ContactID, const int limit = 0) const;
    void                                deleteAllMessages(AccountID);
    
    // CONTACTS
    void                                addContacts(AccountID, QVector<PluginContact>);
    QHash<AccountID, QSet<ContactID>>   contacts(MetacontactID metacontact_id) const;
    QHash<ContactID, Contact>           accountContacts(AccountID) const;
    
    // METACONTACTS
    void                                metacontactAddContact(MetacontactID, AccountID, ContactID);
    MetacontactID                       addMetacontact(const QString &name);
    bool                                deleteMetacontact(MetacontactID);
    QHash<MetacontactID, Metacontact>   metacontacts()                  const;
    Metacontact                         metacontact(MetacontactID)      const;
    
    // PLUGINS
    PluginID addPlugin(const QString &name, const QString &version, const bool enabled = true);
    QString plugin(PluginID p_id) { return _pluginById[p_id]->name; }
    
    // SYNC
    hksync::SyncContact syncContact(AccountID, ContactID) const;
    void addSyncContact(const hksync::SyncContact &contact);
    
    
private:
    QSqlDatabase* _db   = nullptr;
    QHash<PluginID, std::shared_ptr<Plugin>> _pluginById;
    QMap<QString, std::shared_ptr<Plugin>> _pluginByName;
    QHash<AccountID, PluginID> _account2plugin;
    
    ContactID contactID(AccountID, const QString &name);
    MessageID newMessageID(AccountID a_id, const QString& stringID); 
    
    inline QString pluginName(AccountID id) const { return _pluginById.value(_account2plugin.value(id))->name; }
};

#endif // DB_H

#include "db.h"
#include "sqlhelper.h"
#include "../lib/plugincontact.h"
#include "../lib/pluginmessage.h"
#include "contact.h"
#include "message.h"
#include "metacontact.h"
#include "sync.pb.h"

#include <QtCore/QVariantList>
#include <QtCore/QVariant>
#include <QtCore/QDateTime>
#include <QtCore/QStringBuilder>
#include <QtSql/QSqlError>
#include <QtSql/QSqlDriver>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtCore/QRegExp>
#include <QtCore/QElapsedTimer>
#include <QtCore/QVector>
#include <QtCore/QHash>
#include <QtCore/QDebug>

#include <iostream>
#include <stdexcept>
#include <memory>


enum MimeTypes {
    MIME_TEXT_PLAIN = 0
};


Db::Db(const Type &type)
{
    switch(type) {
    case Type::SQLite: _db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE")); break;
    }
}


Db::~Db()
{
    _db->close();
    delete _db;
}


void Db::setDatabaseName(const QString& name)
{
    _db->setDatabaseName(name);
}


bool Db::open()
{
    if(_db->open())
        return true;
    
    SqlHelper::dumpError(_db->lastError());
    return false;
}


/*!
 *  Create common tables
 */

bool Db::createTables()
{
    static const QString dbQueries[] = {
        
        "CREATE TABLE IF NOT EXISTS `dbversion` ("
            "`version` TEXT NOT NULL"
        ")",

        "CREATE TABLE IF NOT EXISTS `plugins` ("
            "`plugin_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "`name` TEXT NOT NULL,"
            "`version` TEXT NOT NULL,"
            "`enabled` BOOL NOT NULL DEFAULT TRUE"
        ")",
        
        "CREATE TABLE IF NOT EXISTS `accounts` ("
            "account_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "plugin_id INTEGER NOT NULL,"
            "name TEXT NOT NULL,"
            "UNIQUE(name),"
            "FOREIGN KEY(plugin_id) REFERENCES plugins(plugin_id)"
        ")",
        
        "CREATE TABLE IF NOT EXISTS `account_settings` ("
            "account_id INTEGER NOT NULL,"
            "key TEXT NOT NULL,"
            "type INTEGER NOT NULL,"
            "value TEXT,"
            "PRIMARY KEY(account_id, key, type, value),"
            "FOREIGN KEY(account_id) REFERENCES accounts(account_id)"
        ")",
        
        "CREATE TABLE IF NOT EXISTS `metacontacts` ("
            "`metacontact_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "`name` TEXT NOT NULL,"
            "`description` TEXT,"
            "UNIQUE(`name`)"
        ")",

        "CREATE TABLE IF NOT EXISTS `metacontacts_list` ("
            "`metacontact_id` INTEGER NOT NULL,"
            "`account_id` INTEGER NOT NULL,"
            "`contact_id` INTEGER NOT NULL,"
            "PRIMARY KEY(metacontact_id, account_id, contact_id),"
            "FOREIGN KEY(account_id) REFERENCES accounts(account_id),"
            "FOREIGN KEY(metacontact_id) REFERENCES metacontacts(metacontact_id)"
        ")",

    };

    SqlHelper helper(*_db);
    
    QSqlQuery q(*_db);
    for(const QString &s : dbQueries) {
        if(!q.exec(s)) {
            SqlHelper::dumpError(q);
            return false;
        }
    }
    
    helper.good();
    return true;
}


/*!
 *  Create tables for plugin
 */

bool Db::createPluginTables(const PluginID p_id)
{
    //TODO: validate exists plugins' tables' structure (need migration to DB-framework?)
    std::shared_ptr<const Plugin> plugin = _pluginById.value(p_id);
    if(plugin->name.isEmpty()) {
        qWarning() << "'pluginName' is empty. PluginID = " << p_id;
        return false;
    }
    
    const QString dbQueries[] = {

    "CREATE TABLE IF NOT EXISTS `" % plugin->name % "_messages` ("
        "`message_id` INTEGER PRIMARY KEY NOT NULL,"
        "`index` INTEGER NOT NULL,"
        "`title` TEXT,"
        "`body` TEXT,"
        "`date` INTEGER NOT NULL DEFAULT 0,"
        "`out` BOOL NOT NULL DEFAULT TRUE," 
        "`contact_id` INTEGER NOT NULL DEFAULT -1,"
        "`account_id` INTEGER NOT NULL DEFAULT -1,"
        "`conversation_id` INTEGER NOT NULL DEFAULT -1,"
        "FOREIGN KEY(account_id) REFERENCES accounts(account_id),"
        "FOREIGN KEY(contact_id) REFERENCES accounts(contact_id)"
    ")",
    
    "CREATE INDEX IF NOT EXISTS `" % plugin->name % "_messages_idx` "
        "ON `" % plugin->name % "_messages` (account_id, contact_id, date)",
    
    "CREATE TABLE IF NOT EXISTS `" % plugin->name % "_messages_str_id` ("
        "`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "`string_id` TEXT NOT NULL,"
        "UNIQUE(`string_id`)"
    ")",
       
    "CREATE TABLE IF NOT EXISTS `" % plugin->name % "_messages_metadata` ("
        "`message_metadata_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "`message_id` INTEGER NOT NULL,"
        "`type` INTEGER NOT NULL,"
        "`value`,"
        "FOREIGN KEY(message_id) REFERENCES `" % plugin->name % "_messages`(message_id)"
    ")",

    
    "CREATE TABLE IF NOT EXISTS `" % plugin->name % "_contacts` ("
        "contact_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "account_id INTEGER NOT NULL,"
        "login TEXT,"
        "name TEXT NOT NULL,"
        "is_numeric_id BOOL NOT NULL,"
        "FOREIGN KEY(account_id) REFERENCES accounts(account_id)"
    ")",
    
    "CREATE TABLE IF NOT EXISTS `" % plugin->name % "_contacts_metadata` ("
        "`contact_metadata_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "`contact_id` INTEGER NOT NULL,"
        "`type` INTEGER NOT NULL,"
        "`value`,"
        "FOREIGN KEY(contact_id) REFERENCES `" % plugin->name % "_messages`(contact_id)"
    ")",
    
    "CREATE TABLE IF NOT EXISTS `" % plugin->name % "_conversations` ("
        "`conversation_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
    ")",

    "CREATE TABLE IF NOT EXISTS `" % plugin->name % "_conversation_partipians` ("
        "`conversation_partipian_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "`conversation_id` INTEGER NOT NULL,"
        "`contact_id` INTEGER NOT NULL,"
        "UNIQUE(`conversation_id`, `contact_id`)"
    ")"

    };

    SqlHelper helper(*_db);

    QSqlQuery q(*_db);
    for(const QString &s : dbQueries) {
        if(!q.exec(s)) {
            SqlHelper::dumpError (q);
            return false;
        }
    }
    helper.good();
 
    return true;
}


bool Db::init()
{
    if(!_db->driver()->hasFeature(QSqlDriver::LastInsertId)) {
        qWarning() << "Missing feature: QSqlDriver::LastInsertId";
        return false;
    }
    
    if(!_db->driver()->hasFeature(QSqlDriver::Transactions))
    {
        qWarning() << "Missing feature: QSqlDriver::Transactions";
        return false;
    }
    
    if(!createTables()) {
        qWarning() << "Can't create common tables";
        return false;
    }
    
    QSqlQuery q(*_db);
    q.prepare("SELECT plugin_id, name FROM plugins");
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return false;
    }
    while(q.next()) {
        std::shared_ptr<Plugin> plugin(new Plugin);
        plugin->id = q.value(0).toInt();
        plugin->name = q.value(1).toString();
        _pluginById[plugin->id] = plugin;
        _pluginByName[plugin->name] = plugin;
    }
    
    for(const auto &account : accounts())
        _account2plugin[account.id] = account.pluginID;
    
    return true;
}


QMap<QString, Db::Plugin> Db::plugins() const
{
    QMap<QString, Db::Plugin> hash;
    QSqlQuery q(*_db);
    
    if(!q.exec("SELECT plugin_id, name, enabled FROM plugins")) {
        SqlHelper::dumpError(q);
        return hash;
    }
    
    while(q.next()) {
        Plugin p;
        p.id = q.value(0).toInt();
        p.enabled = q.value(2).toBool();
        QString name = q.value(1).toString();
        hash[name] = p;
    }
    
    return hash;
}


AccountID Db::addAccount(PluginID p_id, const AccountSettings &settings)
{
    QString name = settings.value(ACCOUNT_SETTING_COMMON).value("Name");
    if(name.isEmpty()) {
        throw std::runtime_error(Q_FUNC_INFO + std::string(": not contains name or name is empty"));
        return -1;
    }
    
    SqlHelper helper(*_db);    
    QSqlQuery q(*_db);
    q.prepare("INSERT INTO `accounts` (`plugin_id`, `name`) VALUES(:plugin_id, :name)");
    q.bindValue(":plugin_id", p_id);
    q.bindValue(":name", name);
    
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return -1;
    }
    int accountId = q.lastInsertId().toInt();
    
    editAccount(accountId, settings);
    
    helper.good();
    _account2plugin[accountId] = p_id;
    return accountId;
}


AccountID Db::account(const QString &name)
{
    QSqlQuery q(*_db);
    
    q.prepare("SELECT `account_id` FROM `accounts` WHERE `name` = :name");
    q.bindValue(":name", name);
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return -1;
    }
    
    if(q.next())
        return q.value(0).toInt();
    return -1;
}


bool Db::deleteAccount(AccountID a_id)
{
    SqlHelper helper(*_db);
    QSqlQuery q(*_db);
    q.prepare("DELETE FROM `accounts` WHERE `account_id` = :account_id");
    q.bindValue(":account_id", a_id);
    
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return false;
    }
    helper.good();
    return true;
}


void Db::editAccount(AccountID a_id, const AccountSettings &settings)
{
    QString name = settings.value(ACCOUNT_SETTING_COMMON).value("Name");
    if(name.isEmpty()) {
        throw std::runtime_error(Q_FUNC_INFO + std::string(": not contains name or name is empty"));
        return;
    }
    
    SqlHelper helper(*_db);
    QSqlQuery q(*_db);
        
    for(auto i = settings.constBegin(); i != settings.constEnd(); ++i) {
        AccountSettingType type = i.key();
        auto typedSettings = i.value();
        
        for(auto setting = typedSettings.constBegin(); setting != typedSettings.constEnd(); ++setting) {
            q.prepare("INSERT INTO `account_settings` (`account_id`, `type`, `key`, `value`) "
                "VALUES (:account_id, :type, :key, :value)");
            q.bindValue(":account_id", a_id);
            q.bindValue(":type", type);
            q.bindValue(":key", setting.key());
            q.bindValue(":value", setting.value());
            if(!q.exec()) {
                SqlHelper::dumpError(q);
                return;
            }
            q.finish();
        }
    }
    
    helper.good();
}


QHash<AccountID, Db::Account> Db::accounts() const
{
    QHash<AccountID, Account> map;
    QSqlQuery q(*_db);
    
    if(!q.exec("SELECT `account_id`, `plugin_id`, `name` FROM `accounts`")) {
        SqlHelper::dumpError(q);
        return map;
    }
    
    while(q.next()) {
        Account a;
        a.id = q.value(0).toInt();
        a.pluginID = q.value(1).toInt();
        a.name = q.value(2).toString();
        map[a.id] = a;
    }
    
    return map;
}


AccountSettings Db::accountSettings(AccountID a_id)
{
    AccountSettings settings;
    QSqlQuery q(*_db);
    q.prepare("SELECT `type`, `key`, `value` FROM `account_settings` WHERE account_id = :id");
    q.bindValue(":id", a_id);

    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return settings;
    }

    while(q.next()) {
        //TODO: validate AccountSettingType
        AccountSettingType type = static_cast<AccountSettingType>(q.value(0).toInt());
        QByteArray key = q.value(1).toByteArray();
        settings[type][key] = q.value(2).toString();
    }
    
    return settings;
}


QHash<MetacontactID, Metacontact> Db::metacontacts() const
{
    QHash<MetacontactID, Metacontact> map;
    QSqlQuery q(*_db);   
    
    if(!q.exec("SELECT metacontact_id, name FROM metacontacts")) {
        SqlHelper::dumpError(q);
        return map;
    }
    
    while(q.next()) {
        Metacontact m;
        m.id = q.value(0).toInt();
        m.name = q.value(1).toString();
        m.contacts = contacts(m.id);
        map.insert(m.id, m);
    }
    
    return map;
}


Metacontact Db::metacontact(MetacontactID m_id) const
{
    QSqlQuery q(*_db);
    q.prepare("SELECT name FROM metacontacts WHERE metacontact_id = :id");
    q.bindValue(":id", m_id);
    
    Metacontact m;
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return m;
    }
    
    if(!q.next()) {
        SqlHelper::dumpError(q);
        throw std::runtime_error(Q_FUNC_INFO + std::string(": empty select"));
        return m;
    }
    
    m.id = m_id;
    m.name = q.value(0).toString();
    m.contacts = contacts(m.id);
    return m;
}


QHash<AccountID, QSet<ContactID>> Db::contacts(MetacontactID metacontact_id) const
{
    QHash<AccountID, QSet<ContactID>> map;
    QSqlQuery q(*_db);
    q.prepare("SELECT contact_id, account_id FROM metacontacts_list WHERE metacontact_id = :metacontact_id");
    q.bindValue(":metacontact_id", metacontact_id);
    
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return map;
    }
    
    while(q.next()) {
        ContactID contactId = q.value(0).toLongLong();
        AccountID accountId = q.value(1).toInt();
        
        if(!map.contains(accountId))
            map.insert(accountId, QSet<ContactID>());
        
        map[accountId].insert(contactId);
    }
    return map;
}

PluginID Db::addPlugin(const QString& name, const QString &version, const bool enabled)
{
    SqlHelper helper(*_db);
    QSqlQuery q(*_db);
    q.prepare("INSERT INTO plugins (name, version, enabled) VALUES(:name, :version, :enabled)");
    q.bindValue(":name", name);
    q.bindValue(":version", version);
    q.bindValue(":enabled", enabled);
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return -1;
    }
    helper.good();
    
    std::shared_ptr<Plugin> plugin(new Plugin);
    plugin->id = q.lastInsertId().toInt();
    plugin->name = name;
    _pluginById.insert(plugin->id, plugin);
    _pluginByName[name] = plugin;
    return plugin->id;
}


MetacontactID Db::addMetacontact(const QString& name)
{
    SqlHelper helper(*_db);
    QSqlQuery q(*_db);
    q.prepare("INSERT INTO metacontacts (name) VALUES(:name)");
    q.bindValue(":name", name);
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return -1;
    }

    helper.good();
    return q.lastInsertId().toInt();
}


bool Db::deleteMetacontact(MetacontactID m_id)
{
    SqlHelper helper(*_db);
    QSqlQuery q(*_db);
    q.prepare("DELETE FROM `metacontacts` WHERE `metacontact_id` = :metacontact_id");
    q.bindValue(":metacontact_id", m_id);
    
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return false;
    }
    
    q.prepare("DELETE FROM `metacontacts_list` WHERE `metacontact_id` = :metacontact_id");
    q.bindValue(":metacontact_id", m_id);
    
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return false;
    }
    
    helper.good();
    return true;
}


void Db::metacontactAddContact(MetacontactID m_id, AccountID a_id, ContactID c_id)
{
    SqlHelper helper(*_db);
    QSqlQuery q(*_db);  
    q.prepare("INSERT INTO metacontacts_list (metacontact_id, account_id, contact_id) "
        "VALUES(:metacontact_id, :account_id, :contact_id)");
    
    q.bindValue(":metacontact_id", m_id);
    q.bindValue(":account_id", a_id);
    q.bindValue(":contact_id", QVariant::fromValue<ContactID>(c_id));
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return;
    }
    helper.good();
}


void Db::addContacts(AccountID a_id, QVector<PluginContact> contacts)
{
    SqlHelper helper(*_db);
    QSqlQuery q(*_db);
 
    for(const auto &c : contacts) {
        ContactID c_id = -1;
        
        switch(c.typeID) {
        case PluginContact::TypeID::Numeric: {
            c_id = c.id;
            q.prepare("INSERT OR REPLACE INTO`" % pluginName(a_id) % "_contacts` "
                "(`contact_id`, `account_id`, `login`, `name`, `is_numeric_id`) VALUES (:contact_id, :account_id, :login, :name, :is_numeric_id)");
            q.bindValue(":contact_id", QVariant::fromValue<ContactID>(c_id));
            q.bindValue(":is_numeric_id", true);
        }
            break;
        case PluginContact::TypeID::Login: {
            if(contactID(a_id, c.login) != -1)
                continue;
            q.prepare("INSERT OR REPLACE INTO`" % pluginName(a_id) % "_contacts` "
                "(`account_id`, `login`, `name`, `is_numeric_id`) VALUES (:account_id, :login, :name, :is_numeric_id)");
            q.bindValue(":is_numeric_id", false);
        }
            break;
        default: {
            std::cerr << Q_FUNC_INFO << ':' << __LINE__ << " WUT?! O_O" << std::endl;
        }
            break;
        }

        q.bindValue(":account_id", QVariant::fromValue<AccountID>(a_id));
        q.bindValue(":login", c.login);
        q.bindValue(":name", c.name);
        
        if(c.name.isEmpty()) {
            qDebug() << Q_FUNC_INFO << ':' << __LINE__ << " id " << c.id
                << " name " << c.name << " uniq " << c.name;
        }
        
        if(!q.exec()) {
            SqlHelper::dumpError(q);
            return;
        }
        q.finish();
    }
    
    helper.good();
    
}


ContactID Db::contactID(AccountID a_id, const QString &login)
{
    QSqlQuery q(*_db);
    q.prepare("SELECT contact_id FROM `" % pluginName(a_id) % "_contacts` WHERE `login` = :login");
    q.bindValue(":login", login);
    
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return -1;
    }
    
    while(q.next())
        return q.value(0).toLongLong();
    
    return -1;
}


void Db::addMessages(AccountID a_id, QVector<PluginMessage> messages)
{
    SqlHelper helper(*_db);
    QSqlQuery q(*_db);
    QString s = "INSERT OR REPLACE INTO `" % pluginName(a_id) % "_messages` "
        "(`message_id`, `index`, `title`, `body`, `date`, `out`, `contact_id`, `account_id`) "
        "VALUES (:message_id, :index, :title, :body, :date, :out, :contact_id, :account_id)";
    
    for(const PluginMessage &m : messages) {
        q.prepare(s);
        
        ContactID c_id = m.contact_id;
        if(c_id == -1) {
            c_id = contactID(a_id, m.login);
            std::cout << Q_FUNC_INFO << ": AccountID " << a_id << " \t, login: " << qPrintable(m.login) << " \t, c_id: " << c_id << std::endl;
        }
        
        switch(m.typeID) {
        case PluginMessage::TypeID::Numeric:
            q.bindValue(":message_id", QVariant::fromValue<MessageID>(m.id));
            break;
        case PluginMessage::TypeID::String:
            q.bindValue(":message_id", QVariant::fromValue<MessageID>(newMessageID(a_id, m.uniqueStringID)));
            break;
        default:
            qWarning() << Q_FUNC_INFO << ':' << __LINE__ << " WUT?! O_O";
            break;
        }

        q.bindValue(":index", QVariant::fromValue<int64_t>(m.index));
        q.bindValue(":title", m.title);
        q.bindValue(":body", m.body);
        q.bindValue(":date", m.date.toTime_t());
        q.bindValue(":out", m.out);
        q.bindValue(":contact_id", QVariant::fromValue<ContactID>(c_id));
        q.bindValue(":account_id", a_id);
 
        if(!q.exec()) {
            SqlHelper::dumpError(q);
            return;
        }
        q.finish();
    }
    helper.good();
}


void Db::deleteAllMessages(AccountID a_id)
{
    QSqlQuery q(*_db);
    SqlHelper helper(*_db);
    q.prepare("DELETE FROM `" % pluginName(a_id) % "_messages` WHERE account_id = :account_id ");
    q.bindValue(":account_id", a_id);

    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return;
    }
    helper.good();
}


QVector<Message> Db::messages(const AccountID a_id, const ContactID c_id, const int limit) const
{
    QVector<Message> v;
    QSqlQuery q(*_db);
    
    QString s = "SELECT `message_id`, `title`, `body`, `date`, `out` FROM `" % pluginName(a_id) % "_messages` "
        " WHERE contact_id = :contact_id AND account_id = :account_id ORDER BY date";
    if(limit > 0)
        s.append(" LIMIT :limit");

    if(!q.prepare(s)) {
        SqlHelper::dumpError(q);
        return v;
    }
    q.bindValue(":account_id", a_id);
    q.bindValue(":contact_id", QVariant::fromValue<ContactID>(c_id));
    q.bindValue(":limit", limit);
    
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return v;
    }

    while(q.next()) {
        Message m;
        m.id = q.value(0).toLongLong();
        m.title = q.value(1).toString();
        m.body = q.value(2).toString();
        m.date = QDateTime::fromTime_t(q.value(3).toInt());
        m.out = q.value(4).toBool();
        m.contact_id = c_id;
        m.account_id = a_id;
        v.append(m);
    }
 
    
    return v;
}


QHash<ContactID, Contact> Db::accountContacts(AccountID a_id) const
{
    QHash<ContactID, Contact> map;
    
    QSqlQuery q(*_db);
    q.prepare("SELECT contact_id, login, name FROM `" % pluginName(a_id) % "_contacts` WHERE account_id = :account_id");
    q.bindValue(":account_id", a_id);

    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return map;
    }
    
    while(q.next()) {
        Contact contact;
        contact.id = q.value(0).toLongLong();
        contact.login = q.value(1).toString();
        contact.name = q.value(2).toString();
        map.insert(contact.id, contact);
    }
    return map;
}



MessageID Db::newMessageID(AccountID a_id, const QString &stringID)
{
    QSqlQuery q(*_db);
    q.prepare("SELECT id FROM `" % pluginName(a_id) % "_messages_str_id` WHERE `string_id` = :string_id");
    q.bindValue(":string_id", stringID);
    
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return -1;
    }
    
    while(q.next())
        return q.value(0).toLongLong();
    
    q.finish();
    
    SqlHelper helper(*_db);
    q.prepare("INSERT INTO " % pluginName(a_id) % "_messages_str_id (`string_id`) VALUES(:string_id)");
    q.bindValue(":string_id", stringID);
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return -1;
    }
 
    helper.good();
    return q.lastInsertId().toLongLong();
}


hksync::SyncContact Db::syncContact(AccountID a_id, ContactID c_id) const
{
    hksync::SyncContact c;
    QSqlQuery q(*_db);
    q.prepare("SELECT login, name, is_numeric_id FROM `" % pluginName(a_id) % "_contacts` WHERE account_id = :a_id AND contact_id = :c_id");
    q.bindValue(":a_id", a_id);
    q.bindValue(":c_id", QVariant::fromValue<ContactID>(c_id));
    if(!q.exec()) {
        SqlHelper::dumpError(q);
        return c;
    }
    q.next();
    QString login = q.value(0).toString();
    QString name = q.value(1).toString();
    c.set_login(login.toUtf8().toStdString());
    c.set_name(name.toUtf8().toStdString());
    
    bool is_numeric_id = q.value(2).toBool();
    c.set_numeric(is_numeric_id);
    
    if(is_numeric_id)
        c.set_id(QString::number(c_id).toStdString());
    else
        c.set_id(login.toUtf8().toStdString());
    
    return c;
}


void Db::addSyncContact(const hksync::SyncContact &contact)
{
    QString pluginName = QString::fromStdString(contact.plugin());
    if(!_pluginByName.contains(pluginName))
        return;
    
    QString accountName = QString::fromStdString(contact.account());
}
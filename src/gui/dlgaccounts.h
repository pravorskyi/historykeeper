#ifndef DLGACCOUNTS_H
#define DLGACCOUNTS_H

#include "../core/accountptr.h"
#include "typedefs.h"
#include "IPlugin.h"
#include <QtWidgets/QDialog>

namespace Ui {
class DlgAccounts;
}

class DlgAccounts : public QDialog
{
    Q_OBJECT
    
public:
    // ctor/dtor
    explicit DlgAccounts(QWidget *parent = 0);
    ~DlgAccounts();
    
    void setAccounts(const QHash<AccountID, AccountPtr> &accounts);
    void appendAccount(const AccountPtr&);
    void setPlugins(const QHash<PluginID, IFactoryPlugin*> &plugins);
    
private:
    void addAccount();
    void onDeleteAccount();
    void onEditAccount();
    
    Ui::DlgAccounts *ui;
    QHash<AccountID, AccountPtr> _accounts;
    QHash<PluginID, IFactoryPlugin*> _plugins;

signals:
    void createAccount(PluginID pluginId, AccountSettings settings);
    void deleteAccount(AccountID);
    void editAccount(AccountID, AccountSettings settings);
};

#endif // DLGACCOUNTS_H

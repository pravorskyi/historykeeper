#ifndef FACEBOOKHISTORYIMPORTER_H
#define FACEBOOKHISTORYIMPORTER_H

#include "IPlugin.h"

class QNetworkAccessManager;

class FacebookHistoryImporter : public IHistoryImporter
{
public:
    //ctor
    explicit FacebookHistoryImporter(const IFactoryPlugin *parent);
    
    void init() override;
    void sync() override;
    
    void setSettings(const QMap<QByteArray, QString> &) override;
    
private:
    ContactID _userID;
    QByteArray  _token;
    
    int _importProgress = 0;
    QNetworkAccessManager* _nam = nullptr;
    
    void getUserID();
    void getMessages();
    void onRequestFinished();
    void onRequestError();
    
    void onApiMe(const QJsonObject &);
    void onApiInbox(const QJsonObject &);
    
};

#endif // FACEBOOKHISTORYIMPORTER_H
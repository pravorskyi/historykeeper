#include "networksheduler.h"

#include <QtCore/QJsonArray>
#include <QtCore/QJsonParseError>
#include <QtCore/QJsonObject>
#include <QtNetwork/QNetworkReply>
#include <iostream>

NetworkSheduler::NetworkSheduler(QObject* parent):
    QObject(parent),
    _nam(this)
{
    _networkSheduleTimer = new QTimer(this);
    _networkSheduleTimer->setInterval(REQUESTS_QUOTE_TIMEOUT_MS);
    connect(_networkSheduleTimer, &QTimer::timeout, this, &NetworkSheduler::onQuoteTimeout);
}

void NetworkSheduler::addGetRequest(const QNetworkRequest& request)
{
    Request r;
    r.qrequest = request;
    r.type = Request::RequestType::Get;
    addRequest(r);
}


void NetworkSheduler::addRequest(const Request &request)
{
    if(_useQuote) {
        _queueRequests.enqueue(request);
        return;
    }
    
    dequeue(request);
}

void NetworkSheduler::dequeue()
{
    if(_queueRequests.isEmpty())
        return;
    
    const Request r = _queueRequests.dequeue();
    dequeue(r);
}

void NetworkSheduler::dequeue(const NetworkSheduler::Request& request)
{
    _useQuote = true;
    QNetworkReply *r = nullptr;
    switch(request.type) {
        case Request::RequestType::Get:
            r = _nam.get(request.qrequest);
            break;
        default:
            std::cout << Q_FUNC_INFO << ':' << __LINE__ << " ?!" << std::endl;
            return;
    }
    
    connect(r, &QNetworkReply::finished, this, &NetworkSheduler::onReplyApiFinished);
    connect(r, static_cast<void (QNetworkReply:: *)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
        this, &NetworkSheduler::onReplyApiError);
    
    _networkSheduleTimer->start();
}


void NetworkSheduler::onQuoteTimeout()
{
    _useQuote = false;
    dequeue();
}

void NetworkSheduler::onReplyApiFinished()
{
    QNetworkReply *r = static_cast<QNetworkReply*>(sender());

    emit reply(r, userID);
}


void NetworkSheduler::onReplyApiError()
{
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    qDebug("QUERY %s", qPrintable(reply->request().url().toString()));
    qDebug("ERROR CODE %s", qPrintable(reply->errorString()));
    reply->deleteLater();
}

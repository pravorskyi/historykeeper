#ifndef PSIHISTORYIMPORTER_H
#define PSIHISTORYIMPORTER_H

#include "IPlugin.h"

#include <QtCore/QDateTime>
#include <QtCore/QVector>

class PsiHistoryImporter : public IHistoryImporter
{
    Q_OBJECT
public:
    PsiHistoryImporter(const IFactoryPlugin *parent);
    ~PsiHistoryImporter();
    
    void init();
    void sync();

private:
    enum MessageAttributes {
        MAttrType = 1,
        MAttrUrl = 2,
        MAttrUrlDesc = 3
    };
    
    QString _configFilename;
    QString _path;
 
    static Message parseLine(const QByteArray &line);
   // static QVector<Message> parseMessages(const QString &fullpath, int32_t jid);
};

#endif // PSIHISTORYIMPORTER_H
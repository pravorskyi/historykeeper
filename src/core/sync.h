#ifndef SYNC_H
#define SYNC_H

#include <QtCore/QObject>
#include <QtNetwork/QAbstractSocket>
#include "db.h"

class QWebSocket;

namespace google {
    namespace protobuf {
        class Message;
    }
}

namespace hksync {
    class ResponseSyncPull;
}


class Sync : public QObject
{
    Q_OBJECT
public:
    explicit Sync(QObject *parent = 0);
    ~Sync();

    void init();
    Db* _db = nullptr; //TODO: remove
    
    void syncPull();
    void syncPush();
    
    void createAccount(const QString &login, const QString &password);

private:
    QWebSocket* _socket = nullptr;
    bool _createlogin = false;

    void onConnected();
    void onError(QAbstractSocket::SocketError socketError);
    void onBinaryMessage(const QByteArray & message);

    void handshake();
    
    void send(uint8_t type, const ::google::protobuf::Message &message);
    
    void onSyncPull(const hksync::ResponseSyncPull &r);
signals:
    void accountUpdated(QString accountName);
};

#endif // SYNC_H

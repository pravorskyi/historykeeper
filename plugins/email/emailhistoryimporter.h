#ifndef EMAILHISTORYIMPORTER_H
#define EMAILHISTORYIMPORTER_H

#include "IPlugin.h"
#include <vmime/vmime.hpp>

class EmailHistoryImporter : public IHistoryImporter
{
public:
    //ctor
    explicit EmailHistoryImporter(const IFactoryPlugin *parent);
    ~EmailHistoryImporter();
    
    void init() override;
    void sync() override;
    
    void setSettings(const QMap<QByteArray, QString> &) override;
private:
    std::shared_ptr<vmime::net::session> _session;
    std::shared_ptr<vmime::net::store> st;
    uint16_t _port;
    
    QDateTime convertDatetime(const std::shared_ptr<const vmime::datetime>&);
};

#endif // EMAILHISTORYIMPORTER_H

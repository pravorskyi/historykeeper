#include "wpgcreateaccount.h"
#include "ui_wpgcreateaccount.h"

WpgCreateAccount::WpgCreateAccount(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::WpgCreateAccount)
{
    ui->setupUi(this);
    connect(ui->comboBox, static_cast<void (QComboBox:: *)(int)>(&QComboBox::currentIndexChanged), this, &WpgCreateAccount::onPluginChanged);
}

WpgCreateAccount::~WpgCreateAccount()
{
    delete ui;
}


bool WpgCreateAccount::isComplete() const
{    
    return true;
}


void WpgCreateAccount::setPlugins(const QHash<PluginID, IFactoryPlugin*> plugins)
{
    _plugins = plugins;
    for(auto i = plugins.constBegin(); i != plugins.constEnd(); ++i)
        ui->comboBox->addItem(i.value()->prettyName(), i.key());
}


PluginID WpgCreateAccount::plugin() const
{
    return *static_cast<const PluginID*>(ui->comboBox->currentData().constData());
}


void WpgCreateAccount::onPluginChanged(int index)
{
    IFactoryPlugin* plugin = _plugins.value(pluginID(index));
    ui->textBrowser->setText(plugin->description());
}


PluginID WpgCreateAccount::pluginID(int index) const
{
    return *static_cast<const PluginID*>(ui->comboBox->itemData(index).constData());
}
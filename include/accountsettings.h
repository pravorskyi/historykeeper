#ifndef ACCOUNTSETTINGS_H
#define ACCOUNTSETTINGS_H

#include <QtCore/QHash>
#include <QtCore/QMap>
#include <QtCore/QString>

enum AccountSettingType {
    ACCOUNT_SETTING_COMMON = 0,
    ACCOUNT_SETTING_PLUGIN = 1
};

typedef QHash<AccountSettingType, QMap<QByteArray, QString>> AccountSettings;

#endif // ACCOUNTSETTINGS_H
#ifndef ACCOUNTPTR_H
#define ACCOUNTPTR_H
#include <memory>

class Account;

typedef std::shared_ptr<const Account> AccountConstPtr;
typedef std::shared_ptr<Account> AccountPtr;

#endif // ACCOUNTPTR_H
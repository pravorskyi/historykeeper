#include "dlgfacebookauth.h"
#include "ui_dlgfacebookauth.h"
#include "globals.h"
#include <QtWebKitWidgets>

DlgFacebookAuth::DlgFacebookAuth(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgFacebookAuth)
{
    ui->setupUi(this);
    _webView = new QWebView(this);
    ui->verticalLayout->insertWidget(0, _webView);
    connect(_webView, &QWebView::urlChanged, this, &DlgFacebookAuth::onUrlChanged);
}

DlgFacebookAuth::~DlgFacebookAuth()
{
    delete ui;
}


int DlgFacebookAuth::exec()
{
    loadAuthPage();
    return QDialog::exec();
}


void DlgFacebookAuth::loadAuthPage()
{
    const QString query = QString("https://www.facebook.com/dialog/oauth?"
        "client_id=%1"
        "&display=popup"
        "&scope=read_mailbox"
        "&response_type=token"
        "&redirect_uri=%2")
        .arg(FACEBOOK_APP_ID, FACEBOOK_REDIRECT_SUCCESS);
        
    _webView->load(QUrl(query));
    qDebug("DEBUG loading auth page %s", qPrintable(query));
}


void DlgFacebookAuth::onUrlChanged()
{
    const QUrl url = _webView->url();
    qDebug("%s", qPrintable(url.toString()));
    qDebug("HOST %s", qPrintable(url.host()));
    qDebug("PATH %s", qPrintable(url.path()));
    if(url.host() == "www.facebook.com" && url.path() == "/connect/login_success.html") {
        qDebug("PARSE PARAMS");
        qDebug("FRAGMENT %s", qPrintable(url.fragment()));
        QUrl urlFragment("/?"+url.fragment());
        QUrlQuery urlQuery(urlFragment.query());

        if(urlQuery.hasQueryItem("error")) {
            m_ErrorDescription = urlQuery.queryItemValue("error_description");
            reject();
        } else {
            m_AccessToken = urlQuery.queryItemValue("access_token");
            accept();
        }
       //qDebug("EXPIRES_IN %s", qPrintable(urlFragment.queryItemValue("expires_in")))
    }
}
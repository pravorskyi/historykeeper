#ifndef HKCORE_GLOBAL_H
#define HKCORE_GLOBAL_H

//TODO: странный workaround для Windows
// http://stackoverflow.com/questions/21295115/qt-signals-missing-in-external-dll

#if defined (_WIN32)
  // cmake makes this variable if your a shared library (projectname_EXPORTS)
  #if defined(HistoryKeeperCore_EXPORTS)
    #define HK_EXPORT Q_DECL_EXPORT
  #else
    #define HK_EXPORT Q_DECL_IMPORT
  #endif
#else /* defined (_WIN32) */
 #define HK_EXPORT
#endif

#endif // HKCORE_GLOBAL_H

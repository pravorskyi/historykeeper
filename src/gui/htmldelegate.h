#ifndef HTMLDELEGATE_H
#define HTMLDELEGATE_H

#include <QtWidgets/QStyledItemDelegate>

class HTMLDelegate : public QStyledItemDelegate
{
public:
    explicit HTMLDelegate(QObject *parent = 0);
protected:
    void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const override;
    QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const override;
};


#endif
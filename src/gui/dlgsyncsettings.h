#ifndef DLGSYNCSETTINGS_H
#define DLGSYNCSETTINGS_H

#include <QDialog>

namespace Ui {
class DlgSyncSettings;
}

class DlgSyncSettings : public QDialog
{
    Q_OBJECT

public:    
    explicit DlgSyncSettings(QWidget *parent = 0);
    ~DlgSyncSettings();
    bool init();


private:
    Ui::DlgSyncSettings *ui;
    void onAccept();
    void onCreateAccount();
    
signals:
    void createAccount(QString login, QString password);
};

#endif // DLGSYNCSETTINGS_H

#include "facebookfactory.h"
#include "facebookhistoryimporter.h"
#include "dlgfacebooksettings.h"
#include <QtGui/QIcon>

IHistoryImporterPtr FacebookFactory::create()
{
    return std::shared_ptr<IHistoryImporter>(new FacebookHistoryImporter(this));
}

bool FacebookFactory::hasFeature(HistoryImporter::Features) const
{
    return false;
}


const QByteArray FacebookFactory::systemName() const
{
    return "facebook";
}


const QString FacebookFactory::prettyName() const
{
    return "Facebook";
}


const QString FacebookFactory::description() const
{
    return tr("Biggest social network service");
}


const QString FacebookFactory::version() const
{
    return "0.1";
}


DlgPluginSettings* FacebookFactory::dlgSettings() const
{
    return new DlgFacebookSettings();
}


QIcon* FacebookFactory::icon() const
{
    QPixmap *pixmap = new QPixmap(":/icons/facebook_icon.png");
    pixmap->scaled(32, 32, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    QIcon* icon = new QIcon(*pixmap);
    return icon;
}
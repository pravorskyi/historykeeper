<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DlgAccountEdit</name>
    <message>
        <location filename="../dlgaccountedit.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlgaccountedit.ui" line="24"/>
        <source>Account</source>
        <translation>Аккаунт</translation>
    </message>
    <message>
        <location filename="../dlgaccountedit.ui" line="29"/>
        <source>Common</source>
        <translation>Загальні</translation>
    </message>
    <message>
        <location filename="../dlgaccountedit.ui" line="34"/>
        <source>Plugin spec</source>
        <translation>Плагін</translation>
    </message>
</context>
<context>
    <name>DlgAccounts</name>
    <message>
        <location filename="../dlgaccounts.ui" line="14"/>
        <source>Accounts</source>
        <translation>Аккаунти</translation>
    </message>
    <message>
        <location filename="../dlgaccounts.ui" line="23"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../dlgaccounts.ui" line="28"/>
        <source>Plugin</source>
        <translation>Плагін</translation>
    </message>
    <message>
        <location filename="../dlgaccounts.ui" line="33"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../dlgaccounts.ui" line="43"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../dlgaccounts.ui" line="50"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../dlgaccounts.ui" line="57"/>
        <source>Edit</source>
        <translation>Змінити</translation>
    </message>
</context>
<context>
    <name>DlgAdvancedSearch</name>
    <message>
        <location filename="../dlgadvancedsearch.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlgadvancedsearch.ui" line="25"/>
        <source>Пошук</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlgadvancedsearch.ui" line="37"/>
        <source>Типи пошуку</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlgadvancedsearch.ui" line="43"/>
        <source>По співпадінню тексту</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlgadvancedsearch.ui" line="53"/>
        <source>Регулярний вираз</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlgadvancedsearch.ui" line="60"/>
        <source>По масці</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgConfig</name>
    <message>
        <location filename="../../../plugins/smaper/dlgconfig.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/smaper/dlgconfig.ui" line="22"/>
        <source>Path:</source>
        <translation>Шлях:</translation>
    </message>
    <message>
        <location filename="../../../plugins/smaper/dlgconfig.ui" line="32"/>
        <source>Browse...</source>
        <translation>Огляд...</translation>
    </message>
</context>
<context>
    <name>DlgEmailSettings</name>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="22"/>
        <source>Type:</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="30"/>
        <source>IMAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="57"/>
        <source>Host:</source>
        <translation>Хост</translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="67"/>
        <source>Encryption:</source>
        <translation>Шифрування</translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="77"/>
        <source>TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="82"/>
        <source>SSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="105"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="136"/>
        <source>Login:</source>
        <translation>Логін:</translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="146"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.ui" line="168"/>
        <location filename="../../../plugins/email/dlgemailsettings.cpp" line="93"/>
        <source>Show</source>
        <translation>Показати</translation>
    </message>
    <message>
        <location filename="../../../plugins/email/dlgemailsettings.cpp" line="90"/>
        <source>Hide</source>
        <translation>Сховати</translation>
    </message>
</context>
<context>
    <name>DlgFacebookAuth</name>
    <message>
        <location filename="../../../plugins/facebook/dlgfacebookauth.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgFacebookSettings</name>
    <message>
        <location filename="../../../plugins/facebook/dlgfacebooksettings.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/facebook/dlgfacebooksettings.ui" line="20"/>
        <source>Get token</source>
        <translation>Отримати токен</translation>
    </message>
    <message>
        <location filename="../../../plugins/facebook/dlgfacebooksettings.ui" line="27"/>
        <source>Token:</source>
        <translation>Токен:</translation>
    </message>
    <message>
        <location filename="../../../plugins/facebook/dlgfacebooksettings.ui" line="34"/>
        <source>&lt;none&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgPlugins</name>
    <message>
        <location filename="../dlgplugins.ui" line="14"/>
        <source>Плагіни</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlgplugins.ui" line="33"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../dlgplugins.ui" line="38"/>
        <source>Description</source>
        <translation>Опис</translation>
    </message>
</context>
<context>
    <name>DlgPsiSettings</name>
    <message>
        <location filename="../../../plugins/psi/dlgpsisettings.ui" line="14"/>
        <source>Configuration</source>
        <translation>Конфігурація</translation>
    </message>
    <message>
        <location filename="../../../plugins/psi/dlgpsisettings.ui" line="20"/>
        <source>Default path</source>
        <translation>Шлях по-замовчуванню</translation>
    </message>
    <message>
        <location filename="../../../plugins/psi/dlgpsisettings.ui" line="26"/>
        <source>Automatically found accounts:</source>
        <translation>Автоматично знайдені аккаунти</translation>
    </message>
</context>
<context>
    <name>DlgSettings</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Налаштування</translation>
    </message>
    <message>
        <source>Sync</source>
        <translation type="obsolete">Синхронізація</translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="obsolete">Хост</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="obsolete">Порт</translation>
    </message>
    <message>
        <source>Account</source>
        <translation type="obsolete">Аккаунт</translation>
    </message>
    <message>
        <source>Passphrase</source>
        <translation type="obsolete">Пароль</translation>
    </message>
</context>
<context>
    <name>DlgSkypeSettings</name>
    <message>
        <location filename="../../../plugins/skype/dlgskypesettings.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/skype/dlgskypesettings.ui" line="20"/>
        <source>Skype account&apos;s directory:</source>
        <translation>Каталог аккаунта Skype:</translation>
    </message>
    <message>
        <location filename="../../../plugins/skype/dlgskypesettings.ui" line="32"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgSyncSettings</name>
    <message>
        <location filename="../dlgsyncsettings.ui" line="14"/>
        <source>Synchronization Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlgsyncsettings.ui" line="24"/>
        <source>Sync</source>
        <translation>Синхронізація</translation>
    </message>
    <message>
        <location filename="../dlgsyncsettings.ui" line="30"/>
        <source>Host</source>
        <translation>Хост</translation>
    </message>
    <message>
        <location filename="../dlgsyncsettings.ui" line="42"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../dlgsyncsettings.ui" line="64"/>
        <source>Login</source>
        <translation>Логін</translation>
    </message>
    <message>
        <location filename="../dlgsyncsettings.ui" line="74"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../dlgsyncsettings.ui" line="84"/>
        <source>Device name</source>
        <translation>Ім’я пристрою</translation>
    </message>
    <message>
        <location filename="../dlgsyncsettings.ui" line="94"/>
        <source>Create account</source>
        <translation>Створити аккаунт</translation>
    </message>
</context>
<context>
    <name>DlgUser</name>
    <message>
        <location filename="../dlguser.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlguser.ui" line="26"/>
        <source>lblFirstName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlguser.ui" line="39"/>
        <source>lblLastName</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgVkAuth</name>
    <message>
        <location filename="../../../plugins/vk.com/dlgvkauth.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvkauth.ui" line="21"/>
        <source>about:blank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgVkSettings</name>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="24"/>
        <source>Authentication</source>
        <translation>Аутентифікація</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="32"/>
        <source>Use autologin</source>
        <translation>Використовувати автологін</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="51"/>
        <source>Login:</source>
        <translation>Логін:</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="61"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="78"/>
        <source>Use token</source>
        <translation>Використовувати токен</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="94"/>
        <source>Get token</source>
        <translation>Отримати токен</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="101"/>
        <source>Token:</source>
        <translation>Токен</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="108"/>
        <source>&lt;none&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="134"/>
        <source>Advanced</source>
        <translation>Розширені налаштування</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="140"/>
        <source>Custom APP ID</source>
        <translation>Власний APP ID</translation>
    </message>
    <message>
        <location filename="../../../plugins/vk.com/dlgvksettings.ui" line="152"/>
        <source>APP ID:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmailFactory</name>
    <message>
        <location filename="../../../plugins/email/emailfactory.cpp" line="37"/>
        <source>Electronic mail.</source>
        <translation>Електронна пошта.</translation>
    </message>
</context>
<context>
    <name>FacebookFactory</name>
    <message>
        <location filename="../../../plugins/facebook/facebookfactory.cpp" line="31"/>
        <source>Biggest social network service</source>
        <translation>Найбільша соціальна мережа.</translation>
    </message>
</context>
<context>
    <name>HKCore</name>
    <message>
        <location filename="../../core/hkcore.cpp" line="284"/>
        <source>Messages not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>HistoryKeeper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="28"/>
        <source>&amp;About</source>
        <translation>&amp;Про програму</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="36"/>
        <source>&amp;Settings</source>
        <translation>&amp;Налаштування</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="42"/>
        <source>&amp;Tools</source>
        <translation>&amp;Інструменти</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <source>&amp;Export...</source>
        <translation>&amp;Експорт</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <source>Synchroni&amp;zation</source>
        <translation>&amp;Синхронізація</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="78"/>
        <source>&amp;Plugins...</source>
        <translation>&amp;Плагіни</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="83"/>
        <source>&amp;Sync all</source>
        <translation>&amp;Оновити все</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <source>&amp;Add metacontact</source>
        <translation>&amp;Додати метаконтакт</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <source>&amp;Delete metacontact</source>
        <translation>&amp;Видалити метаконтакт</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="98"/>
        <source>&amp;About...</source>
        <translation>&amp;Про программу...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="103"/>
        <source>About &amp;Qt...</source>
        <translation>Про &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="108"/>
        <source>Export to HTML...</source>
        <translation>Експортувати в HTML...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <source>Export to JSON...</source>
        <translation>Експортувати в JSON...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="118"/>
        <source>Export to XML...</source>
        <translation>Експортувати в XML...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="123"/>
        <source>&amp;Accounts...</source>
        <translation>&amp;Аккаунти...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="128"/>
        <source>&amp;Settings...</source>
        <translation>&amp;Налаштування...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="133"/>
        <source>S&amp;ync devices</source>
        <translation>С&amp;инхронізувати пристрої</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="138"/>
        <source>&amp;Syncronization settings...</source>
        <translation>&amp;Налаштування синхронізації...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="240"/>
        <source>You</source>
        <translation>Ви</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="415"/>
        <source>Add metacontact</source>
        <translation>Додати метаконтакт</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="433"/>
        <source>Account error</source>
        <translation>Помилка (аккаунт)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="477"/>
        <source>About Qt</source>
        <translation>Про Qt</translation>
    </message>
</context>
<context>
    <name>MessageModel</name>
    <message>
        <location filename="../messagemodel.cpp" line="27"/>
        <source>You</source>
        <translation>Ви</translation>
    </message>
</context>
<context>
    <name>PsiFactory</name>
    <message>
        <location filename="../../../plugins/psi/psifactory.cpp" line="49"/>
        <source>Cross-platform XMPP/Jabber client</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../../plugins/psi/psihistoryimporter.cpp" line="148"/>
        <source>&lt;big&gt;[System Message]&lt;/big&gt;&lt;br&gt;You are now authorized.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/psi/psihistoryimporter.cpp" line="150"/>
        <source>&lt;big&gt;[System Message]&lt;/big&gt;&lt;br&gt;Your authorization has been removed!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Roster</name>
    <message>
        <location filename="../roster.cpp" line="16"/>
        <source>Filter contacts...</source>
        <translation>Фільтрація контактів...</translation>
    </message>
    <message>
        <location filename="../roster.cpp" line="23"/>
        <source>Metacontacts</source>
        <translation>Метаконтакти</translation>
    </message>
    <message>
        <location filename="../roster.cpp" line="169"/>
        <source>Sync</source>
        <translation>Отримати нові повідомлення</translation>
    </message>
    <message>
        <location filename="../roster.cpp" line="174"/>
        <source>Delete all messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../roster.cpp" line="181"/>
        <source>Remove metacontact</source>
        <translation>Видалити метаконтакт</translation>
    </message>
    <message>
        <location filename="../roster.cpp" line="188"/>
        <source>Add metacontact</source>
        <translation>Створити метаконтакт</translation>
    </message>
</context>
<context>
    <name>SkypeFactory</name>
    <message>
        <location filename="../../../plugins/skype/skypefactory.cpp" line="39"/>
        <source>Proprietary Voice over IP service and software application.</source>
        <translation>Закрите ПЗ та сервіс для VoIP.</translation>
    </message>
</context>
<context>
    <name>VkComFactory</name>
    <message>
        <location filename="../../../plugins/vk.com/vkcomfactory.cpp" line="38"/>
        <source>Biggest international social network service in Europe</source>
        <translation>Найбільша міжнародна соціальна мережа в Європі</translation>
    </message>
</context>
<context>
    <name>VkComHistoryImporter</name>
    <message>
        <location filename="../../../plugins/vk.com/vkcomhistoryimporter.cpp" line="225"/>
        <source>Authorization error on vk.com. Please, check your settings</source>
        <translation>Помилка авторизації. Будь-ласка, перевірте ваші налаштування</translation>
    </message>
</context>
<context>
    <name>WizardCreateAccount</name>
    <message>
        <location filename="../wizardcreateaccount.cpp" line="20"/>
        <source>Create account wizard</source>
        <translation>Майстер створення аккаунта</translation>
    </message>
</context>
<context>
    <name>WpgAccountCommonSettings</name>
    <message>
        <location filename="../wpgaccountcommonsettings.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wpgaccountcommonsettings.ui" line="22"/>
        <source>Account name</source>
        <translation>Ім’я аккаунта</translation>
    </message>
    <message>
        <location filename="../wpgaccountcommonsettings.ui" line="37"/>
        <source>Autosync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wpgaccountcommonsettings.ui" line="46"/>
        <source>Every hh:mm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WpgCreateAccount</name>
    <message>
        <location filename="../wpgcreateaccount.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wpgcreateaccount.ui" line="22"/>
        <source>Resource (plugin):</source>
        <translation>Ресурс (плагін):</translation>
    </message>
</context>
</TS>

#include "wpgaccountpluginsettings.h"

WpgAccountPluginSettings::WpgAccountPluginSettings(QWidget *parent) :
    QWizardPage(parent)
{
    
}

WpgAccountPluginSettings::~WpgAccountPluginSettings()
{
    
}


bool WpgAccountPluginSettings::isComplete() const
{
    return _widget->isValid();
}


QMap<QByteArray, QString> WpgAccountPluginSettings::settings() const
{
    return _widget->settings();
}


void WpgAccountPluginSettings::setWidget(DlgPluginSettings* widget)
{
    if(_widget != nullptr)
        delete _widget;
        
    _widget = widget;
    _widget->setParent(this);
    connect(_widget, &DlgPluginSettings::completeChanged, this, &QWizardPage::completeChanged);
}

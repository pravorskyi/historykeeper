#include "messagemodel.h"
#include <QtCore/QStringBuilder>

#include <QDebug>

MessageModel::MessageModel(QObject* parent): QAbstractListModel(parent)
{
}


QVariant MessageModel::data(const QModelIndex& index, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();
    
    const Message &m = _messages.at(index.row());
    QString body = m.body;
    body.replace('\n', QLatin1String("<br>"));
    
    QString prepend;
    if(index.row() == 0 ||  m.date.date() > _messages.at(index.row() - 1).date.date())
        prepend = "<b><font color=darkgreen>" % m.date.date().toString() % "</font></b><br>";
    
    QLatin1String color = m.out ? QLatin1String("blue") : QLatin1String("red");
    QString sender;
    if(m.out) {
        sender = tr("You");
    } else {
        sender = _contacts.value(m.account_id).value(m.contact_id).name;
    }
    
    QString s = prepend % "<font color=" % color % ">[" % m.date.toString("hh:mm:ss") % "] " % sender % ":</font> "
        % body;
    
    return s;
}


int MessageModel::rowCount(const QModelIndex&) const
{
    return _messages.count();
}


void MessageModel::setMessages(const QVector<Message> &messages)
{
    beginResetModel();
    _messages = messages;
    endResetModel();
}


void MessageModel::setContacts(const QHash<AccountID, QHash<ContactID, Contact>> &contacts)
{
    qDebug() << Q_FUNC_INFO << ":" << __LINE__;
    qDebug() << contacts.keys();
    _contacts = contacts;
    qDebug() << Q_FUNC_INFO << ":" << __LINE__;
}

#include "dlgskypesettings.h"
#include "ui_dlgskypesettings.h"

#include <QtWidgets/QFileDialog>

DlgSkypeSettings::DlgSkypeSettings(QWidget *parent) :
    DlgPluginSettings(parent),
    ui(new Ui::DlgSkypeSettings)
{
    ui->setupUi(this);
    connect(ui->pushButton, &QPushButton::clicked, this, &DlgSkypeSettings::onBrowse);
    connect(ui->lineEdit, &QLineEdit::textChanged, this, &DlgSkypeSettings::onDataChanged);
}


DlgSkypeSettings::~DlgSkypeSettings()
{
    delete ui;
}


bool DlgSkypeSettings::isValid() const
{
    if(ui->lineEdit->text().isEmpty())
        return false;
    
    return true;
}


QMap<QByteArray, QString> DlgSkypeSettings::settings() const
{
    QMap<QByteArray, QString> map;
    
    map["Path"] = ui->lineEdit->text();    
    return map;
}


void DlgSkypeSettings::setSettings(const QMap<QByteArray, QString> &settings)
{
    ui->lineEdit->setText(settings["Path"]);
}


void DlgSkypeSettings::onBrowse()
{
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    if (dialog.exec()) {
        ui->lineEdit->setText(dialog.selectedFiles().first());
    }
}


void DlgSkypeSettings::onDataChanged()
{
    emit completeChanged();
}

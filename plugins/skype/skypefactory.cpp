#include "skypefactory.h"
#include "skypehistoryimporter.h"
#include "dlgskypesettings.h"
#include <QtGui/QIcon>


SkypeFactory::~SkypeFactory()
{
}


bool SkypeFactory::hasFeature(HistoryImporter::Features feature) const
{
    using namespace HistoryImporter;
    switch(feature) {
        case Features::AsyncSynchronization:
            return true;
        case Features::ConfigDialog:
            return true;
    }
    return false;
}


IHistoryImporterPtr SkypeFactory::create()
{
    return std::shared_ptr<IHistoryImporter>(new SkypeHistoryImporter(this));
}


const QString SkypeFactory::prettyName() const
{
    return "Skype";
}


const QString SkypeFactory::description() const
{
    return tr("Proprietary Voice over IP service and software application.");
}


const QByteArray SkypeFactory::systemName() const
{
    return "skype";
}

const QString SkypeFactory::version() const
{
    return "0.1";
}


DlgPluginSettings* SkypeFactory::dlgSettings() const
{
    return new DlgSkypeSettings();
}


QIcon* SkypeFactory::icon() const
{
    QPixmap *pixmap = new QPixmap(":/icons/skype_icon.png");
    pixmap->scaled(32, 32, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    QIcon* icon = new QIcon(*pixmap);
    return icon;
}
#ifndef PLUGININTERFACE_H
#define PLUGININTERFACE_H

#include "accountsettings.h"
#include "typedefs.h"
#include "../src/lib/plugincontact.h"
#include "../src/lib/pluginmessage.h"

#include <QtCore/QObject>
#include <QtCore/QtPlugin>
#include <QtCore/QList>
#include <QtCore/QVariant>
#include <QtWidgets/QWidget>
#include <memory>

class QDialog;
class QObject;
class QIcon;

class IFactoryPlugin;
class IHistoryImporter;
typedef std::shared_ptr<IHistoryImporter> IHistoryImporterPtr;  

namespace HistoryImporter {
    
    enum class Features {
        AsyncSynchronization,
        ConfigDialog,
        Conversations,
        Icon,
        SyncSynchronization
    };
}


class IHistoryImporter : public QObject
{
    Q_OBJECT
public:
    //ctor
    explicit IHistoryImporter(const IFactoryPlugin *parent) { _parent = parent; }
    //dtor
    virtual ~IHistoryImporter() {}
   

    virtual void init() = 0;
    virtual void setSettings(const QMap<QByteArray, QString> &s) { __settings = s; }
    //
    void setName(const QString &name) { _name = name; }
    
    const IFactoryPlugin* plugin() const { return _parent; }
    QString name() const { return _name; }
    
    QMap<QByteArray, QString> __settings; //FIXME: remove this
private:
    const IFactoryPlugin* _parent;
    QString _name;
    
public slots:
    virtual void sync() = 0;
    
signals:
    void sigError(const QString &errorString);
    void importStarted();
    void importProgress(int32_t progress);
    void importFinished();
    void newContacts(QVector<PluginContact> contacts);
    void newMessages(QVector<PluginMessage> messages);
};


class DlgPluginSettings: public QWidget
{
    Q_OBJECT
public:
    explicit DlgPluginSettings(QWidget *parent = 0) : QWidget(parent) { };
    virtual ~DlgPluginSettings() {}
    
    virtual bool isValid() const = 0;
    virtual QMap<QByteArray, QString> settings() const = 0;
    
    virtual void setSettings(const QMap<QByteArray, QString> &settings) = 0;

signals:
    void completeChanged();
};


class IFactoryPlugin
{
public:
    //dtor
     virtual ~IFactoryPlugin() {}
     
    virtual IHistoryImporterPtr create() = 0;
    //info
    virtual bool hasFeature(const HistoryImporter::Features) const = 0;
    virtual const QString       prettyName()    const = 0;
    virtual const QString       description()   const = 0;
    // MUST contains only alphanumeric symbols and underscore (a-z, A-Z, 0-9, "_")
    virtual const QByteArray    systemName()    const = 0; //MUST be unique for each plugin
    
    virtual const QString       version()       const = 0;
    
    virtual DlgPluginSettings*  dlgSettings()   const = 0;
    virtual QIcon*              icon()          const = 0;
private:
    friend class HKCore;
    PluginID pluginId = -1;
};

Q_DECLARE_INTERFACE(IFactoryPlugin, "com.HistoryKeeper.FactoryPlugin/1.0")


#endif // PLUGININTERFACE_H

#ifndef NETWORKSHEDULER_H
#define NETWORKSHEDULER_H

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QQueue>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

class NetworkSheduler : public QObject
{
    Q_OBJECT
public:
    NetworkSheduler(QObject *parent = 0);
    void addGetRequest(const QNetworkRequest &request);
    int userID;
private:
    struct Request {
        enum class RequestType {
            Get,
            Post
        };
        QNetworkRequest qrequest;
        RequestType type;
        QByteArray postData;
    };
    
    void addRequest(const Request &request);
    void dequeue();
    void dequeue(const Request &request);
    void onQuoteTimeout();
    void onReplyApiFinished();
    void onReplyApiError();
    
    QNetworkAccessManager _nam;
    QQueue<Request> _queueRequests;
    QTimer* _networkSheduleTimer = nullptr;
    bool _useQuote = false;
    static const int REQUESTS_QUOTE_TIMEOUT_MS = 340;  // ~3 request per second

signals:
    void reply(QNetworkReply* reply, int userID);
    void sigError(const QString &errorString);
};

#endif // NETWORKSHEDULER_H

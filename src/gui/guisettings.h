#ifndef GUISETTINGS_H
#define GUISETTINGS_H

extern const char* SETTING_GROUP_MAINWINDOW;
extern const char* SETTING_MAINWINDOW_GEOMETRY;
extern const char* SETTING_MAINWINDOW_MAXIMIZED;

extern const char* SETTING_GROUP_SYNC;
extern const char* SETTING_SYNC_HOST;
extern const char* SETTING_SYNC_PORT;
extern const char* SETTING_SYNC_ACCOUNT;
extern const char* SETTING_SYNC_PASSPHRASE;


#endif // GUISETTINGS_H

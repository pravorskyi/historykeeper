#include "dlgplugins.h"
#include "ui_dlgplugins.h"

#include "IPlugin.h"


DlgPlugins::DlgPlugins(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::DlgPlugins)
{
    ui->setupUi(this);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void DlgPlugins::setPlugins(const QHash<int32_t, IFactoryPlugin*> &list)
{
    ui->tableWidget->setRowCount(list.count());
    ui->tableWidget->setColumnCount(2);

    int i = 0;
    for(auto plugin : list) {
        QTableWidgetItem* itemName = new QTableWidgetItem(plugin->prettyName());
        itemName->setIcon(*plugin->icon());
        ui->tableWidget->setItem(i, 0, itemName);
        
        QTableWidgetItem* itemDescr = new QTableWidgetItem(plugin->description());
        ui->tableWidget->setItem(i, 1, itemDescr);
        
        ++i;
    }
}

#include "vkcomhistoryimporter.h"

#include "dlgvkauth.h"

#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlError>
#include <QtCore/QVariant>
#include <QtCore/QSettings>
#include <QtCore/QVector>
#include <QtCore/QUrl>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonParseError>
#include <QtCore/QJsonObject>
#include <QtCore/QCoreApplication>
#include <QtCore/QFile>
#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QStringBuilder>

#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <iostream>

enum RequestAttributeFields {
    RequestAttrType = 1000,
    RequestAttrOut = 1001,
    RequestAttrCount = 1002,
    RequestAttrOffset = 1003
};

VkComHistoryImporter::VkComHistoryImporter(const IFactoryPlugin *parent) :
    IHistoryImporter(parent),
    _networkSheduler(this)
{
    connect(&_networkSheduler, &NetworkSheduler::reply, this, &VkComHistoryImporter::onReply);
}

VkComHistoryImporter::~VkComHistoryImporter()
{
}


void VkComHistoryImporter::setSettings(const QMap<QByteArray, QString> &settings)
{
    _token = settings.value("Token", "").toLatin1();
}


void VkComHistoryImporter::sync()
{
    _importProgress = 0;
    _downloadedCount = 0;
    _contactsToDownload.clear();
    emit importStarted();
    downloadMessages(0, 200, 0);
}


void VkComHistoryImporter::downloadMessages(qint8 out, int count, int offset)
{
    QUrl url(QString("https://api.vk.com/method/messages.get?offset=%1&time_offset=0&count=%2&out=%3&access_token=%4")
             .arg(offset).arg(count).arg(out).arg(QString(_token)));
    QNetworkRequest request(url);
    request.setAttribute((QNetworkRequest::Attribute)RequestAttrType, QString("messages.get"));
    request.setAttribute((QNetworkRequest::Attribute)RequestAttrOut, out);
    request.setAttribute((QNetworkRequest::Attribute)RequestAttrOffset, offset);
    request.setAttribute((QNetworkRequest::Attribute)RequestAttrCount, count);
    
    _networkSheduler.addGetRequest(request);
}

void VkComHistoryImporter::downloadContacts(const QSet<int> &uids)
{
    if(uids.isEmpty()) {
        emit importFinished();
        std::cout << Q_FUNC_INFO << " list empty..." << std::endl;
        return;
    }

    QByteArray uids_str;
    for(int uid : uids) {
        uids_str.append(QString::number(uid).toLatin1()); //TODO: replace with boost::lexical_cast
        uids_str.append(",");
    }
    uids_str.chop(1);

    QString fields("uid,first_name,last_name");

    /* relation
        1 - не женат/не замужем
        2 - есть друг/есть подруга
        3 - помолвлен/помолвлена
        4 - женат/замужем
        5 - всё сложно
        6 - в активном поиске
        7 - влюблён/влюблена
    */

    QUrl url("https://api.vk.com/method/users.get"
        "?uids="+uids_str+
        "&access_token=" + _token +
        "&fields=" + fields);

    QNetworkRequest request(url);
    request.setAttribute((QNetworkRequest::Attribute)RequestAttrType, QString("users.get"));
    _networkSheduler.addGetRequest(request);
}


void VkComHistoryImporter::processMessages(const QJsonArray &data)
{
    QVector<PluginMessage> messages;
    
    int count = data.count();
    for(int i = 1; i < count; ++i) {
        PluginMessage message;

        const QJsonObject map = data.at(i).toObject();
        bool ok = true;
        int mid = map.value("mid").toVariant().toInt(&ok);
        if(!ok) {
            qWarning("Error: \"mid\" is not valid int");
            continue;
        }
        int32_t uid = map.value("uid").toVariant().toInt(&ok);
        if(!ok) {
            qWarning("Error: \"uid\" is not valid int");
            continue;
        }
        int date = map.value("date").toVariant().toUInt(&ok);
        if(!ok) {
            qWarning("Error: \"date\" is not valid int");
            continue;
        }

        //bool readState = map["read_state"].toVariant().toInt();
        //int chatId = map.value("chat_id").toVariant().toInt();
        //int deleted = map.value("deleted").toVariant().toInt();
        //int emoji = map.value("emoji").toVariant().toInt();

        message.id = mid;
        message.index = mid;
        message.title = map["title"].toString();
        message.body = map["body"].toString();
        message.date = QDateTime::fromTime_t(date);
        message.contact_id = uid;
        message.out = map["out"].toVariant().toInt();

        //TODO: add emoji
        //if(emoji != 0)
        //    message.metadata.insert(MAttrEmoji, QVector<QVariant>({ emoji }));
        _contactsToDownload.insert(uid);
        messages.append(message);
    }
    emit newMessages(messages);
}


void VkComHistoryImporter::processUsers(const QJsonArray &data)
{
    QVector<PluginContact> contacts;
    
    for(const QJsonValue &item : data) {
        QJsonObject jsonUser = item.toObject();
        PluginContact contact;
        contact.typeID = PluginContact::TypeID::Numeric;
        contact.id  = jsonUser.take("uid").toVariant().toLongLong();

        QString first_name  = jsonUser.take("first_name").toString();
        QString last_name   = jsonUser.take("last_name").toString();

        contact.name = last_name % ' ' % first_name;
        
        std::cout << Q_FUNC_INFO << ':' << __LINE__  << " uniq " << contact.login.toStdString() <<  std::endl;

        if(jsonUser.contains("deactivated")) {
            contact.attributes.insert(CAttrDeactivated, jsonUser.take("deactivated").toString());
        }

        contact.attributes.insert(CAttrFirstName, first_name);
        contact.attributes.insert(CAttrLastName, last_name);
        
        if(!jsonUser.isEmpty()) {
            std::cout << Q_FUNC_INFO << ':' << __LINE__ << " " << QJsonDocument(jsonUser).toJson(QJsonDocument::Indented).data() << std::endl;
        }
        
        contacts.append(contact);
    }
    emit newContacts(contacts);
    emit importFinished();
    _contactsToDownload.clear();
    _contactsToDownload.squeeze();
}


void VkComHistoryImporter::onNetworkShedule()
{
    
}

void VkComHistoryImporter::onReply(QNetworkReply* reply)
{
    qDebug("QUERY %s", qPrintable(reply->request().url().toString()));
    const QByteArray data = reply->readAll();
    qDebug("FINISHED %s", data.constData());

        
    QJsonParseError jsonParseError;
    const QJsonDocument jsonDoc = QJsonDocument::fromJson(data, &jsonParseError);
    if(jsonParseError.error != QJsonParseError::NoError) {
        qDebug("Error! Parse JSON fail — %s. Data:\n\n%s", qPrintable(jsonParseError.errorString()), data.constData());
        return;
    }
       
    const QJsonObject jsonObj = jsonDoc.object();
        
    if( jsonObj.contains("error") ) {
        //TODO: error code
        const QJsonObject error = jsonObj.value("error").toObject();
        int errorCode = error.value("error_code").toVariant().toInt();
        switch(errorCode) {
        case 5:
            emit sigError(tr("Authorization error on vk.com. Please, check your settings"));
            std::cout << Q_FUNC_INFO << " error emited" << std::endl;
            break;
        case 6: //Too many requests per second
            _networkSheduler.addGetRequest(reply->request());
            break;
        default:
            std::cout << Q_FUNC_INFO << ':' << __LINE__ << " ERROR "
                      << QJsonDocument(error).toJson(QJsonDocument::Indented).constData() << std::endl;
        }

        reply->deleteLater();
        return;
    }
    
    if(reply->request().attribute((QNetworkRequest::Attribute)RequestAttrType).toString() == "messages.get") {
        int m_count = reply->request().attribute((QNetworkRequest::Attribute)RequestAttrCount).toInt();
        int offset = reply->request().attribute((QNetworkRequest::Attribute)RequestAttrOffset).toInt();
        int out = reply->request().attribute((QNetworkRequest::Attribute)RequestAttrOut).toInt();
        

        const QJsonArray messages = jsonObj.value("response").toArray();
        if(messages.isEmpty()) {
            emit sigError("Messages is empty: " + QJsonDocument(messages).toJson());
            reply->deleteLater();
            return;
        }
        
        int count = messages.at(0).toVariant().toInt();

        if(count == 0) {
            std::cout << "Messages count 0: " << QJsonDocument(messages).toJson().data() << std::endl;
            reply->deleteLater();
            return;
        }

        int progress = offset * 100 / count / 2 + 50*out;
        std::cout << "COUNT " << count << " \tPROGRESS " << progress << std::endl;
        emit importProgress(progress);

        emit processMessages(messages);
        if(offset <= count) {
            downloadMessages(out, m_count, offset + m_count);
        } else if(out == 0) {
             downloadMessages(1, 200, 0);
        } else if(out == 1) {
            downloadContacts(_contactsToDownload);
        }


    } else if(reply->request().attribute((QNetworkRequest::Attribute)RequestAttrType).toString() == "users.get") {
        const QJsonArray users = jsonObj.value("response").toArray();
        processUsers(users);
    }
    reply->deleteLater();
}

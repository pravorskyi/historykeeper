#include "dlgemailsettings.h"
#include "ui_dlgemailsettings.h"

DlgEmailSettings::DlgEmailSettings(QWidget *parent) :
    DlgPluginSettings(parent),
    ui(new Ui::DlgEmailSettings)
{
    ui->setupUi(this);
    connect(ui->edtHost, &QLineEdit::textEdited, this, &DlgEmailSettings::onDataChanged);
    connect(ui->edtLogin, &QLineEdit::textEdited, this, &DlgEmailSettings::onDataChanged);
    connect(ui->edtPassword, &QLineEdit::textEdited, this, &DlgEmailSettings::onDataChanged);
    connect(ui->btnShowPassword, &QPushButton::clicked, this, &DlgEmailSettings::onShowPassword);
}


DlgEmailSettings::~DlgEmailSettings()
{
    delete ui;
}


bool DlgEmailSettings::isValid() const
{
    if(ui->edtHost->text().isEmpty() ||
        ui->edtLogin->text().isEmpty()
    )
        return false;
    
    return true;
}


QMap<QByteArray, QString> DlgEmailSettings::settings() const
{
    QMap<QByteArray, QString> m;
    
    m["host"] = ui->edtHost->text();
    m["encryption"] = ui->cbxEncryption->currentText();
    if(ui->sbPort->value() != 0)
        m["port"] = QString::number(ui->sbPort->value());
    
    m["login"] = ui->edtLogin->text();
    m["password"] = ui->edtPassword->text();
    
    return m;
}


void DlgEmailSettings::setSettings(const QMap<QByteArray, QString> &settings)
{
    ui->edtHost->setText(settings.value("host", ""));
    
    int index = ui->cbxEncryption->findText(settings.value("encryption", ""));
    if(index != -1)
        ui->cbxEncryption->setCurrentIndex(index);
    
    ui->sbPort->setValue(settings.value("port", "0").toInt());
    ui->edtLogin->setText(settings.value("login", ""));
    ui->edtPassword->setText(settings.value("password", ""));
}


void DlgEmailSettings::onDataChanged()
{
    emit completeChanged();
}


void DlgEmailSettings::onEncryptionChanged()
{
    if(ui->sbPort->value() == 0)
        return;
    
    if(ui->cbxEncryption->currentText() == "SSL") {
        ui->sbPort->setValue(993);
        return;
    }
    
    if(ui->cbxEncryption->currentText() == "TLS") {
        ui->sbPort->setValue(143);
        return;
    }
}


void DlgEmailSettings::onShowPassword()
{
    if(ui->edtPassword->echoMode() == QLineEdit::Password) {
        ui->edtPassword->setEchoMode(QLineEdit::Normal);
        ui->btnShowPassword->setText(tr("Hide"));
    } else {
        ui->edtPassword->setEchoMode(QLineEdit::Password);
        ui->btnShowPassword->setText(tr("Show"));
    }
}
#include "wpgaccountcommonsettings.h"
#include "ui_wpgaccountcommonsettings.h"

WpgAccountCommonSettings::WpgAccountCommonSettings(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::WpgAccountCommonSettings)
{
    ui->setupUi(this);
}

WpgAccountCommonSettings::~WpgAccountCommonSettings()
{
    delete ui;
}


QMap<QByteArray, QString> WpgAccountCommonSettings::settings() const
{
    QMap<QByteArray, QString> map;
    map["Name"] = ui->edtAccountName->text();
    return map;
}


void WpgAccountCommonSettings::setSettings(const QMap<QByteArray, QString> &settings)
{
    ui->edtAccountName->setText(settings.value("Name"));
}
#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <stdint.h>

typedef int32_t AccountID;
typedef int64_t ContactID;
typedef int64_t MessageID;
typedef int32_t MetacontactID;
typedef int32_t PluginID;

#endif // TYPEDEFS_H
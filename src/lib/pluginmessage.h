#ifndef PLUGINMESSAGE_H
#define PLUGINMESSAGE_H

#include <QtCore/QDateTime>
#include <QtCore/QString>
#include <QtCore/QVector>

#include "typedefs.h"
#include <string>

class PluginMessage {
public:    
    // Тип id сообщения
    enum class TypeID {
        Numeric,        // числовой тип aka int64_t
        String           // в качестве id выступает уникальная строка
    } typeID    = TypeID::Numeric;
    
    MessageID id    = -1; // уникальный id сообщения
    QString uniqueStringID;
    
    
    
    QString title;
    QString body;
    QDateTime date;
    bool out            = false;
    ContactID contact_id;
    QString login;

    int64_t index;      //номер п/п
};

#endif // PLUGINMESSAGE_H

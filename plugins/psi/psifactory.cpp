#include "psifactory.h"
#include "psihistoryimporter.h"
#include "dlgpsisettings.h"

PsiFactory::~PsiFactory()
{

}


bool PsiFactory::hasFeature(HistoryImporter::Features feature) const
{
    using namespace HistoryImporter;
    switch(feature) {
        case Features::AsyncSynchronization:
            return false;
            break;
        case Features::ConfigDialog:
            return true;
            break;
        case Features::Conversations:
            return true;
            break;
        case Features::Icon:
            return false;
            break;
        case Features::SyncSynchronization:
            return true;
            break;
    }
    return false;
}


IHistoryImporter* PsiFactory::create()
{
    return new PsiHistoryImporter(this);
}


const QString PsiFactory::prettyName() const
{
    return "Psi/Psi+";
}


const QString PsiFactory::description() const
{
    return tr("Cross-platform XMPP/Jabber client");
}


const QByteArray PsiFactory::systemName() const
{
    return "psi";
}


const QString PsiFactory::version() const
{
    return "0.1";
}


DlgPluginSettings* PsiFactory::dlgSettings() const
{
    DlgPsiSettings* dlg = new DlgPsiSettings();
    return dlg;
}


QIcon* PsiFactory::icon() const
{
    return nullptr;
}
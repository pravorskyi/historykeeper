#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../core/accountptr.h"

#include <QtWidgets/QMainWindow>
#include "typedefs.h"

class Roster;
class QTextBrowser;
class QThread;
class QTreeWidgetItem;
class QLabel;
class QProgressBar;
class QIcon;
class QSplitter;
class MessageModel;

class HKCore;
class Message;
class Metacontact;
class Contact;
enum class SearchType;
class IHistoryImporter;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool init();

private:
    Ui::MainWindow *ui          = nullptr;
    HKCore *_hkcore             = nullptr;
    QThread *_hkcoreThread      = nullptr;
    Roster* _roster             = nullptr;
    MessageModel* _messageModel = nullptr;
    QTextBrowser* _browser      = nullptr;

    QLabel *_progressWidget     = nullptr;
    QProgressBar* _progressBar  = nullptr;
    QSplitter* _splitter        = nullptr;

    void onAboutToQuit();
    
    void initRoster();
    
    void setMessages(const QHash<AccountID, QHash<ContactID, Contact>> &contacts, const QVector<Message>&);
    void onRosterContactSelected(AccountID, ContactID);
    void onRosterMetacontactSelected(MetacontactID metacontact_id);
    bool searchFilter(const Message &m, const SearchType &searchType, const QString &pattern);
    void onLiveSearchChanged(int state);
    void onSearchReturnPressed();
    void search(const QString &searchText);
    void advancedSearch(const SearchType &searchType, const QString &pattern);
    QVector<Message> searchForContact(AccountID, ContactID, const SearchType &searchType, const QString &text);
    QVector<Message> searchForAccount(AccountID, const SearchType &searchType, const QString &text);
    QVector<Message> searchForMetacontact(MetacontactID, const SearchType &searchType, const QString& text);
    void showDlgPlugins();
    void onAccountAdd(AccountID);
    void addMetacontact();
    void removeMetacontact();
    void onDeleteAllMessages();
    void onAccountError(AccountID, const QString);
    void onImportStarted(AccountID);
    void onImportProgress(AccountID, int32_t progress);
    void onImportFinished(AccountID);
    void showDlgAbout();
    void showDlgAboutQt(); //TODO: change to lambda-function
    void showDlgAccounts();
    void showDlgAdvancedSearch();
    void showDlgSettings();
    void showDlgSyncSettings();
    void showError(const QString &errorString);
    void exportMessagesToHTML();
    void exportMessagesToXML();
    void exportMessagesToJSON();
    
    void createRichTextWidget();

signals:
    void sigNewAccount(AccountPtr);

};

#endif // MAINWINDOW_H

#ifndef DLGFACEBOOKSETTINGS_H
#define DLGFACEBOOKSETTINGS_H

#include "IPlugin.h"

namespace Ui {
class DlgFacebookSettings;
}

class DlgFacebookSettings : public DlgPluginSettings
{
    Q_OBJECT

public:
    explicit DlgFacebookSettings(QWidget *parent = 0);
    ~DlgFacebookSettings();
    
    bool isValid() const override;
    QMap<QByteArray, QString> settings() const override;
    
    void setSettings(const QMap<QByteArray, QString> &settings) override;

private:
    Ui::DlgFacebookSettings *ui;
    
    void onTokenRequest();
    void onDataChanged();
};

#endif // DLGFACEBOOKSETTINGS_H

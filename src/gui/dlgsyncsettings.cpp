#include "dlgsyncsettings.h"
#include "ui_dlgsyncsettings.h"
#include "guisettings.h"
#include <QtCore/QDebug>
#include <QtCore/QSettings>


DlgSyncSettings::DlgSyncSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgSyncSettings)
{
    ui->setupUi(this);
    connect(this, &QDialog::accepted, this, &DlgSyncSettings::onAccept);
    connect(ui->pushButton, &QPushButton::clicked, this, &DlgSyncSettings::onCreateAccount);
}


DlgSyncSettings::~DlgSyncSettings()
{
    delete ui;
}


bool DlgSyncSettings::init()
{
    QSettings s;
    s.beginGroup("Sync");
    ui->edtHost->setText(s.value("Host", "").toString());
    ui->sbxPort->setValue(s.value("Port", 8081).toInt());
    ui->edtLogin->setText(s.value("Login", "").toString());
    ui->edtPassword->setText(s.value("Password", "").toString());
    ui->edtDeviceName->setText(s.value("DeviceName", "").toString());
    return true;
}


void DlgSyncSettings::onAccept()
{
    QSettings s;
    s.beginGroup("Sync");
    s.setValue("Host", ui->edtHost->text());
    s.setValue("Port", ui->sbxPort->value());
    s.setValue("Login", ui->edtLogin->text());
    s.setValue("Password", ui->edtPassword->text());
    s.setValue("DeviceName", ui->edtDeviceName->text());
    s.endGroup();
    s.sync();
}


void DlgSyncSettings::onCreateAccount()
{
    onAccept();
    emit createAccount(ui->edtLogin->text(), ui->edtPassword->text());
}

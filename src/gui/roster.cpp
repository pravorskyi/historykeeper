#include "roster.h"
#include "../core/contact.h"
#include "../core/metacontact.h"

#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMenu>
#include <QtWidgets/QVBoxLayout>

#include <iostream>
#include "rosterprivate.h"
#include <QDebug>

Roster::Roster(QWidget* parent): QWidget(parent)
{
    _rosterPrivate = new RosterPrivate(this);
    _edtFilter = new QLineEdit(this);
    _edtFilter->setPlaceholderText(tr("Filter contacts..."));
    
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(_edtFilter);
    mainLayout->addWidget(_rosterPrivate);
    
    _metacontactsRootItem = new RosterMetacontactsRootItem(_rosterPrivate->invisibleRootItem());
    _metacontactsRootItem->setText(0, tr("Metacontacts"));
    _metacontactsRootItem->setFlags(_metacontactsRootItem->flags()  &= ~(Qt::ItemIsDragEnabled|Qt::ItemIsDropEnabled));
        //FIXME: memory leak;
    //FIXME: icons load
    QPixmap *pix = new QPixmap(":/icons/metacontact.png");
    pix->scaled(32, 32, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    _metacontactsIcon = new QIcon(*pix);
    _metacontactsRootItem->setIcon(0, *_metacontactsIcon);
    
    connect(_edtFilter, &QLineEdit::textChanged, this, &Roster::filter); 
    connect(_rosterPrivate, &QTreeWidget::customContextMenuRequested, this, &Roster::onCustomContextMenuRequested);
    connect(_rosterPrivate, &QTreeWidget::itemClicked, this, &Roster::onItemClicked);
    connect(_rosterPrivate, &RosterPrivate::droppedItem, this, &Roster::onItemDropped);
}


void Roster::filter(const QString& text)
{
    qDebug("FILTER");
    QList<QTreeWidgetItem*> items = _rosterPrivate->findItems("", Qt::MatchContains | Qt::MatchRecursive);
    for(QTreeWidgetItem* item : items)
        if(dynamic_cast<RosterAccountItem*>(item) == NULL &&
           dynamic_cast<RosterMetacontactsRootItem*>(item) == NULL &&
           dynamic_cast<RosterMetacontactItem*>(item) == NULL)
            item->setHidden(true);
    
    items = _rosterPrivate->findItems(text, Qt::MatchContains | Qt::MatchRecursive);
    for(QTreeWidgetItem* item : items)
        item->setHidden(false);
}


void Roster::addAccount(AccountID id, const QString &name, QIcon* icon)
{
    RosterAccountItem* item = new RosterAccountItem(_rosterPrivate->invisibleRootItem());
    item->setText(0, name);
    item->setFlags(item->flags()  &= ~(Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled));
    item->accountId = id;
    if(icon != nullptr)
        item->setIcon(0, *icon);
    _mapAccounts.insert(id, item);
}


void Roster::removeAccount(AccountID id)
{
    RosterAccountItem* item = _mapAccounts.value(id);
    delete item;
    _mapAccounts.remove(id);
}


void Roster::addMetacontact(const Metacontact &metacontact)
{
    qDebug("LOAD METACONTACT");
    qDebug() << _mapAccounts;
    RosterMetacontactItem* item = new RosterMetacontactItem(_metacontactsRootItem);
    item->setFlags(item->flags()  &= ~Qt::ItemIsDragEnabled);
    item->setText(0, metacontact.name);
    item->id = metacontact.id;
    item->setIcon(0, *_metacontactsIcon);
     
     for(auto it = metacontact.contacts.constBegin(); it != metacontact.contacts.constEnd(); ++it) {
         AccountID accountID = it.key();
         
         for(auto set_it = it.value().constBegin(); set_it != it.value().constEnd(); ++set_it) {
            std::cout << Q_FUNC_INFO << ": " << it.key() << "\t" << *set_it << std::endl;
            RosterContactItem* contactItem = contact(accountID, *set_it);
            std::cout << "ContactItem " << contactItem << std::endl;
            item->addChild(contactItem);
            
            QTreeWidgetItem* parent = contactItem->parent();
            int index = parent->indexOfChild(contactItem);
            item->addChild(parent->takeChild(index));
         }
     }
     _metacontacts[item->id] = item;
}


void Roster::addContact(AccountID accountId, const Contact& contact)
{
    RosterAccountItem* account = _mapAccounts.value(accountId);

    if(account->hashContactItems.contains(contact.id)) {
        RosterContactItem *item = account->hashContactItems.value(contact.id);
        item->setText(0, contact.name);
        return;
    }

    RosterContactItem *item = new RosterContactItem(account);
    
    item->setFlags(item->flags()  &= ~Qt::ItemIsDropEnabled);
    item->contact_id = contact.id;
    item->setText(0, contact.name);
    item->accountId = accountId;
    item->setIcon(0, account->icon(0));
    account->hashContactItems[item->contact_id] = item;
}


RosterContactItem* Roster::contact(AccountID accountId, ContactID contact_id)
{
    RosterAccountItem *account = accountItem(accountId);
    if(account == nullptr)
        return nullptr;
     
    return account->hashContactItems.value(contact_id, nullptr);
}


RosterAccountItem* Roster::accountItem(AccountID accountId) const
{
    return _mapAccounts.value(accountId, nullptr);
}


void Roster::onAccountSync()
{
    QAction* action = static_cast<QAction*>(sender());
    emit sigAccountSync(action->data().toInt());
}

void Roster::onAccountDeleteAllMessages()
{
    QAction* action = static_cast<QAction*>(sender());
    emit sigAccountDeleteAllMessages(action->data().toInt());
}


void Roster::onMetacontactRemove()
{
    QAction* action = static_cast<QAction*>(sender());
    removeMetacontactByItem(_metacontacts[action->data().toInt()]);
}


void Roster::onCustomContextMenuRequested(const QPoint& pos)
{
    QTreeWidgetItem* item = _rosterPrivate->itemAt(pos);
 
    if (item) {
    // Note: We must map the point to global from the viewport to
    // account for the header.
        QMenu menu;
        //TODO: check if not memory leak with QAction
        switch (item->type()) {
        case AccountType: {
            QAction* action = menu.addAction(tr("Sync"));
            RosterAccountItem *accountItem = static_cast<RosterAccountItem*>(item);
            action->setData(accountItem->accountId);
            connect(action, &QAction::triggered, this, &Roster::onAccountSync);

            QAction* actionDeleteAllMessages = menu.addAction(tr("Delete all messages"));
            actionDeleteAllMessages->setData(accountItem->accountId);
            connect(actionDeleteAllMessages, &QAction::triggered, this, &Roster::onAccountDeleteAllMessages);

            break;
        }
        case MetacontactType: {
            QAction* actionMetacontactRemove = menu.addAction(tr("Remove metacontact"));
            RosterMetacontactItem *metacontactItem = static_cast<RosterMetacontactItem*>(item);
            actionMetacontactRemove->setData(metacontactItem->id);
            connect(actionMetacontactRemove, &QAction::triggered, this, &Roster::onMetacontactRemove);
            break;
        }
        case MetacontactsRootType: {
            QAction* action = menu.addAction(tr("Add metacontact")); //TODO: merge with main window action
            connect(action, &QAction::triggered, [=]() { emit sigMetacontactAdd(); } );
            break;
        }
        }
 
        menu.exec(_rosterPrivate->viewport()->mapToGlobal(pos));
    }
}


void Roster::onItemClicked(QTreeWidgetItem* item)
{
    switch(item->type()) {
    case ContactType: {
        RosterContactItem* contactItem = static_cast<RosterContactItem*>(item);
        emit contactSelected(contactItem->accountId, contactItem->contact_id);
        break;
    }
    case MetacontactType: {
        RosterMetacontactItem* metacontactItem = static_cast<RosterMetacontactItem*>(item);
        emit metacontactSelected(metacontactItem->id);
        break;
    }
    }
}


void Roster::removeMetacontactByItem(RosterMetacontactItem* item)
{
    while(item->childCount() > 0) {
        RosterContactItem* child = static_cast<RosterContactItem*>(item->child(0));
        RosterAccountItem* account = accountItem(child->accountId);
        
        account->addChild(item->takeChild(0));
    }
    
    _metacontactsRootItem->removeChild(item);
    _metacontacts.remove(item->id);
    emit sigMetacontactRemoved(item->id);
    delete item;
}


void Roster::removeMetacontact()
{
    QTreeWidgetItem* selectedItem = _rosterPrivate->selectedItems().first();
    RosterMetacontactItem* metacontactItem = dynamic_cast<RosterMetacontactItem*>(selectedItem);
    if(metacontactItem == NULL)
        return;
    
    removeMetacontactByItem(metacontactItem);
}



void Roster::onItemDropped(RosterContactItem* item)
{
    std::cout << Q_FUNC_INFO << std::endl;
    RosterMetacontactItem* parent = static_cast<RosterMetacontactItem*>(item->parent());
    emit sigMetacontactAddContact(parent->id, item->accountId, item->contact_id);
}


Roster::Item Roster::currentItemType() const
{
    QTreeWidgetItem* item = _rosterPrivate->selectedItems().first();
    switch (item->type()) {
    case AccountType:
        return Item::Account;
    case MetacontactsRootType:
        return Item::MetacontactRoot;
    case MetacontactType:
        return Item::Metacontact;
    case ContactType:
        return Item::Contact;
    }
    return Item::None;
}


AccountID Roster::currentAccount() const
{
    QTreeWidgetItem* item = _rosterPrivate->selectedItems().first();
    switch (item->type()) {
    case AccountType: {
            RosterAccountItem *accountItem = static_cast<RosterAccountItem*>(item);
            return accountItem->accountId;
        }
        break;
    case MetacontactsRootType:
        return -1;
    case MetacontactType:
        return -1;
    case ContactType: {
            RosterContactItem *contactItem = static_cast<RosterContactItem*>(item);
            return contactItem->accountId;
        }
        break;
    }
    return -1;
}


ContactID Roster::currentContact() const
{
    QTreeWidgetItem* item = _rosterPrivate->selectedItems().first();
    switch (item->type()) {
    case AccountType:
        return -1;
    case MetacontactsRootType:
        return -1;
    case MetacontactType:
        return -1;
    case ContactType: {
            RosterContactItem *contactItem = static_cast<RosterContactItem*>(item);
            return contactItem->contact_id;
        }
        break;
    }
    return -1;
}


MetacontactID Roster::currentMetacontact() const
{
    QTreeWidgetItem* item = _rosterPrivate->selectedItems().first();

    switch (item->type()) {
    case AccountType:
        return -1;
        break;
    case MetacontactsRootType:
        return -1;
    case MetacontactType: {
        RosterMetacontactItem *metacontactItem = static_cast<RosterMetacontactItem*>(item);
        return metacontactItem->id;
    }
    case ContactType:
        return -1;
        break;
    }
    return -1;
}

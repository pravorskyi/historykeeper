#ifndef WPGCREATEACCOUNT_H
#define WPGCREATEACCOUNT_H

#include "typedefs.h"
#include "IPlugin.h"
#include <QtWidgets/QWizardPage>

namespace Ui {
class WpgCreateAccount;
}

class WpgCreateAccount : public QWizardPage
{
    Q_OBJECT
public:
    explicit WpgCreateAccount(QWidget *parent = 0);
    ~WpgCreateAccount();
    
    bool isComplete() const override;
    
    void setPlugins(const QHash<PluginID, IFactoryPlugin*> plugins);
    PluginID plugin() const;
    
private:
    Ui::WpgCreateAccount *ui;
    QHash<PluginID, IFactoryPlugin*> _plugins;

    void onPluginChanged(int index);
    PluginID pluginID(int index) const;
};

#endif // WPGCREATEACCOUNT_H

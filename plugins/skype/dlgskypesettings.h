#ifndef DLGSKYPESETTINGS_H
#define DLGSKYPESETTINGS_H

#include "IPlugin.h"

namespace Ui {
class DlgSkypeSettings;
}

class DlgSkypeSettings : public DlgPluginSettings
{
    Q_OBJECT
    
public:
    explicit DlgSkypeSettings(QWidget *parent = 0);
    ~DlgSkypeSettings();
    
    bool isValid() const override;
    QMap<QByteArray, QString> settings() const override;
    
    void setSettings(const QMap<QByteArray, QString> &settings) override;
    
private:
    Ui::DlgSkypeSettings *ui;

    void onBrowse();
    void onDataChanged();
};

#endif // DLGSKYPESETTINGS_H

#include "dlguser.h"
#include "ui_dlguser.h"

DlgUser::DlgUser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgUser)
{
    ui->setupUi(this);
}

DlgUser::~DlgUser()
{
    delete ui;
}

void DlgUser::setFirstName(const QString &firstName)
{
    ui->lblFirstName->setText(firstName);
}

void DlgUser::setLastName(const QString &lastName)
{
    ui->lblLastName->setText(lastName);
}

#include "guisettings.h"

const char* SETTING_GROUP_MAINWINDOW        = "MainWindow";
const char* SETTING_MAINWINDOW_GEOMETRY     = "Geometry";
const char* SETTING_MAINWINDOW_MAXIMIZED    = "Maximized";

const char* SETTING_GROUP_SYNC              = "Sync";
const char* SETTING_SYNC_HOST               = "Host";
const char* SETTING_SYNC_PORT               = "Port";
const char* SETTING_SYNC_ACCOUNT            = "Account";
const char* SETTING_SYNC_PASSPHRASE         = "Passphrase";

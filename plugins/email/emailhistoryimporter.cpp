#include "emailhistoryimporter.h"
#include <vmime/vmime.hpp>
#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <iostream>
#include <sstream>

#include <vmime/contentTypeField.hpp>
#include <vmime/header.hpp>
#include <vmime/headerField.hpp>
#include <vmime/headerFieldFactory.hpp>
#include <vmime/headerFieldValue.hpp>

#ifdef Q_OS_WIN
    #include <vmime/platforms/windows/windowsHandler.hpp>
#else
    #include <vmime/platforms/posix/posixHandler.hpp>
#endif

//FIXME: create normal validating
class BlindCertificateAccepter : public vmime::security::cert::certificateVerifier {
public:
    void verify(std::shared_ptr<vmime::security::cert::certificateChain>, const vmime::string&) override {}
};


class myTracer : public vmime::net::tracer
{
public:

    myTracer(const vmime::string& proto, const int connectionId)
        : m_proto(proto), m_connectionId(connectionId)
    {
    }

    // Called by VMime to trace what is sent on the socket
    void traceSend(const vmime::string& line)
    {
        std::cout << "[" << m_proto << ":" << m_connectionId
                  << "] C: " << line << std::endl;
    }

    // Called by VMime to trace what is received from the socket
    void traceReceive(const vmime::string& line)
    {
        std::cout << "[" << m_proto << ":" << m_connectionId
                  << "] S: " << line << std::endl;
    }

private:

    const vmime::string m_proto;
    const int m_connectionId;
};


class myTracerFactory : public vmime::net::tracerFactory
{
public:

    std::shared_ptr <vmime::net::tracer> create
        (std::shared_ptr <vmime::net::service> serv,
         const int connectionId)
    {
        return vmime::make_shared <myTracer>
               (serv->getProtocolName(), connectionId);
    }
};




static const vmime::string getFolderPathString(std::shared_ptr <vmime::net::folder> f)
{
    const vmime::string n = f->getName().getBuffer();

    if (n.empty()) {// root folder
        return "/";
    } else {
        std::shared_ptr <vmime::net::folder> p = f->getParent();
        return getFolderPathString(p) + n + "/";
    }
}


static void printFolders(std::shared_ptr <vmime::net::folder> folder, const int level = 0)
{
    for (int j = 0 ; j < level * 2 ; ++j)
        std::cout << " ";

    std::cout << getFolderPathString(folder) << std::endl;

    std::vector <std::shared_ptr <vmime::net::folder> > subFolders = folder->getFolders(false);
    for (unsigned int i = 0 ; i < subFolders.size() ; ++i)
        printFolders(subFolders[i], level + 1);
}


EmailHistoryImporter::EmailHistoryImporter(const IFactoryPlugin *parent) :
    IHistoryImporter(parent)
{
}


EmailHistoryImporter::~EmailHistoryImporter()
{
    st->disconnect();
}


void EmailHistoryImporter::init()
{

}


void EmailHistoryImporter::sync()
{
    QVector<PluginContact> contacts;
    QVector<PluginMessage> messages;
    
    try {
#ifdef Q_OS_WIN
        vmime::platform::setHandler<vmime::platforms::windows::windowsHandler>();
#else
        vmime::platform::setHandler<vmime::platforms::posix::posixHandler>();
#endif

        QString encryptionMethod = __settings.value("encryption");
        
        _session = vmime::net::session::create();
        QString login = __settings.value("login", "");
        QString password = __settings.value("password", "");
        _session->getProperties()["store.imap.auth.username"] = qPrintable(login);
        _session->getProperties()["store.imap.auth.password"] = qPrintable(password);

        
        QString proto = "imap://";
        if(encryptionMethod == "SSL") {
            proto = QString("imaps://%1:%2@").arg(login, password);
        }
  
            
        
        QString urlStr = QString("%1%2").arg(proto, __settings.value("host", ""));
        if(_port != 0)
            urlStr.append(QString(":%1").arg(_port));
        
        vmime::utility::url url(qPrintable(urlStr));
    
        st = _session->getStore(url);
        st->setProperty("options.need-authentication", true);
        
        //if(encryptionMethod == "TLS")
        st->setProperty("connection.tls", true);
        // Set the object responsible for verifying certificates, in the
        // case a secured connection is used (TLS/SSL)
        st->setCertificateVerifier(vmime::make_shared<BlindCertificateAccepter>());
        st->setTracerFactory(vmime::make_shared <myTracerFactory>());
        
        st->connect();

        // Display some information about the connection
        std::shared_ptr <vmime::net::connectionInfos> ci = st->getConnectionInfos();

        std::cout << std::endl;
        std::cout << "Connected to '" << ci->getHost() << "' (port " << ci->getPort() << ")" << std::endl;
        std::cout << "Connection is " << (st->isSecuredConnection() ? "" : "NOT ") << "secured." << std::endl;
        
        std::shared_ptr <vmime::net::folder> f = st->getDefaultFolder();
        f->open(vmime::net::folder::MODE_READ_ONLY);

        int count = f->getMessageCount();

        std::cout << std::endl;
        std::cout << count << " message(s) in your inbox" << std::endl;
        
// List folders
        //vmime::shared_ptr <vmime::net::folder> root = st->getRootFolder();
        //printFolders(root);
        //

        int fCapabilities = f->getFetchCapabilities();
        if (fCapabilities & vmime::net::fetchAttributes::ENVELOPE)
            std::cout << "FLAG_ENVELOPE" << std::endl;
        
        
        auto allMessages = f->getMessages(vmime::net::messageSet::byNumber(1, -1));
        std::cout << "Fetched: " << allMessages.size() << std::endl;
        f->fetchMessages(allMessages, vmime::net::fetchAttributes::FLAGS
	  | vmime::net::fetchAttributes::ENVELOPE
	  | vmime::net::fetchAttributes::UID);
        
        std::cout << "Fetched: " << allMessages.size() << std::endl;
        
        for(uint i = 0; i < allMessages.size(); ++i) {
            auto mgs = allMessages[i];
            std::cout << "Message " << i << " : " << std::endl;

        }

        emit importStarted();
        const vmime::charset ch_utf8(vmime::charsets::UTF_8);
        for(int i = 1; i <= count; ++i) {    
            std::shared_ptr<vmime::net::message> msg = f->getMessage(i);
            f->fetchMessage(msg, vmime::net::fetchAttributes::FLAGS | vmime::net::fetchAttributes::ENVELOPE | vmime::net::fetchAttributes::UID);
            std::shared_ptr<const vmime::header> hdr = msg->getHeader();
            std::cout << " - sent on " << hdr->Date()->generate() << std::endl;
            //std::cout << " - sent by " << hdr->From()->getValue<vmime::mailbox>()->getEmail().toString() << std::endl;

            vmime::charset ch(vmime::charsets::UTF_8);
            //get charset
            try {
                //TODO: report error with "const_cast<vmime::header*>(hdr.get())" and non-const method getField
                std::shared_ptr<vmime::contentTypeField> fld = const_cast<vmime::header*>(hdr.get())->getField<vmime::contentTypeField>(vmime::fields::CONTENT_TYPE);
                ch = fld->getCharset();
            } catch (vmime::exceptions::no_such_field) {
            }
            
            PluginContact contact;
            contact.login = QString::fromStdString( hdr->From()->getValue<vmime::mailbox>()->getEmail().toString() );
            contact.name = QString::fromStdString( hdr->From()->getValue<vmime::mailbox>()->getName().getConvertedText(ch_utf8) );
            if(contact.name.isEmpty())
                contact.name = contact.login;
            contact.typeID = PluginContact::TypeID::Login;
            
            contacts.append(contact);      
            
            PluginMessage message;
            message.contact_id = -1;
            message.login = contact.login;
            //message.typeID = Message::TypeID::Numeric;
	    //message.id = i;
	    message.typeID = PluginMessage::TypeID::String;
            message.uniqueStringID = login + "_" +QString::fromStdString(msg->getUID());
            message.date = convertDatetime(hdr->Date()->getValue<vmime::datetime>());
            
            
            std::shared_ptr<vmime::message> parsedMsg = msg->getParsedMessage();
            //vmime::utility::outputStreamAdapter out(std::cout);
            //msg->extract(out);
            vmime::messageParser mp(parsedMsg);
            for(uint j = 0; j < mp.getTextPartCount(); ++j) {
                QCoreApplication::processEvents();
                auto tp = mp.getTextPartAt(j);

                std::string unconverted;
                vmime::charset ch_local(vmime::charsets::UTF_8);
                
                // text/html
                if(tp->getType().getSubType() == vmime::mediaTypes::TEXT_PLAIN) {
                    ch_local = tp->getCharset();
                    vmime::utility::outputStreamStringAdapter textOut(unconverted);
                    tp->getText()->extract(textOut);
                    textOut.flush();
                }
                else if(tp->getType().getSubType() == vmime::mediaTypes::TEXT_HTML) {
                    auto htp = vmime::dynamicCast<const vmime::htmlTextPart>(tp);
                    // HTML text i s in tp− >getText ()
                    // Plain text i s in tp− >getPlainText ()
                    ch_local = htp->getCharset();
                    vmime::utility::outputStreamStringAdapter htmlOut(unconverted);
                    htp->getPlainText()->extract(htmlOut);
                    htmlOut.flush();
                                        // Enumerate embedded objects
          //          for(uint i1 = 0; i1 < htp->getObjectCount(); ++i1) {
          //              auto obj = htp->getObjectAt(i1);
                        // Identifier (Content−Id or Content−Location) i s obj− >getId ()
                        // Object data i s in obj− >getData()
            //        }
                }
                
                std::string converted;
		
		try {
		    vmime::charset::convert(unconverted, converted, ch_local, ch_utf8);
		} catch (vmime::exceptions::no_encoder_available &e) {
		    std::cerr << "vmime::exceptions::no_encoder_available" << std::endl;
		    std::cerr << e.what() << std::endl;
		    emit sigError(QString("VMIME: No encoder available: %1").arg(e.what()));
		    std::cerr << qPrintable(contact.login) << std::endl;
		    std::cerr << unconverted << std::endl;
		}
		
		message.body.append(converted.c_str());
            }

            messages.append(message);
            emit importProgress(i/(count/100.0));
            QCoreApplication::processEvents();
        }
    } catch(vmime::exceptions::authentication_error &e) {
        emit sigError(e.what());
        qWarning() << "vmime::exceptions::authentication_error: " << e.what();
        emit importFinished();
    } catch (vmime::exceptions::malformed_url &e) {
        emit sigError(e.what());
        qWarning() << "vmime::exceptions::malformed_url: " << e.what();
        emit importFinished();
    } catch (vmime::exceptions::no_encoder_available &e) {
        std::cerr << "vmime::exceptions::no_encoder_available" << std::endl;
        std::cerr << e.what() << std::endl;
        //emit sigError(QString("VMIME: No encoder available: %1").arg(e.what()));
    } catch (vmime::exceptions::operation_timed_out& e) {
        std::cerr << "vmime::exceptions::operation_timed_out" << std::endl;
        std::cerr << e.what() << std::endl;
        emit sigError("Server time-out. Please, try again later");
        emit importFinished();
        return;
    } catch (vmime::exceptions::socket_exception& e) {
        std::cerr << "vmime::exceptions::socket_exception" << std::endl;
        std::cerr << e.what() << std::endl;
        emit sigError(QString("Socket exception: %1").arg(e.what()));
        emit importFinished();
        return;
    } catch (vmime::exception& e) {
        std::cerr << std::endl;
        std::cerr << e.what() << std::endl;
        throw;
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        std::cerr << "std::exception: " << e.what() << std::endl;        
        throw;
    }
    
    emit newContacts(contacts);
    emit newMessages(messages);
    emit importFinished();
}
    

void EmailHistoryImporter::setSettings(const QMap<QByteArray, QString> &settings)
{
    __settings = settings;
    _port = __settings.value("port", "0").toInt();
}


QDateTime EmailHistoryImporter::convertDatetime(const std::shared_ptr<const vmime::datetime> &vdate)
{
    QDateTime qdate;
    qdate.setDate(QDate(vdate->getYear(), vdate->getMonth(), vdate->getDay()));
    qdate.setTime(QTime(vdate->getHour(), vdate->getMinute(), vdate->getSecond()));
    //TODO: add timezone 
    return qdate;
}

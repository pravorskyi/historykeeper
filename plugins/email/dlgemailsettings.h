#ifndef DLGEMAILSETTINGS_H
#define DLGEMAILSETTINGS_H

#include "IPlugin.h"

namespace Ui {
class DlgEmailSettings;
}

class DlgEmailSettings : public DlgPluginSettings
{
    Q_OBJECT

public:
    explicit DlgEmailSettings(QWidget *parent = 0);
    ~DlgEmailSettings();
    
    bool isValid() const override;
    QMap<QByteArray, QString> settings() const override;
    
    void setSettings(const QMap<QByteArray, QString> &settings) override;

private:
    Ui::DlgEmailSettings *ui;
    void onDataChanged();
    void onEncryptionChanged();
    void onShowPassword();
};

#endif // DLGEMAILSETTINGS_H

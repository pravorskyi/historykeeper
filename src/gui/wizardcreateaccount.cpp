#include "wizardcreateaccount.h"
#include "wpgaccountcommonsettings.h"
#include "wpgaccountpluginsettings.h"
#include "wpgcreateaccount.h"

WizardCreateAccount::WizardCreateAccount(const QHash<PluginID, IFactoryPlugin*>& plugins, QWidget* parent) :
    QWizard(parent),
    _plugins(plugins)
{
    _wpgCreateAccount = new WpgCreateAccount(this);
    _wpgCreateAccount->setPlugins(_plugins);
    _wpgCommonSettings = new WpgAccountCommonSettings(this);
    _wpgPluginSettings = new WpgAccountPluginSettings(this);
    
    setPage(PG_CREATE, _wpgCreateAccount);
    setPage(PG_COMMON_SETTINGS, _wpgCommonSettings);
    setPage(PG_PLUGIN_SETTINGS, _wpgPluginSettings);
       
    setModal(true);
    setWindowTitle(tr("Create account wizard"));
}


void WizardCreateAccount::initializePage(int id)
{
    switch(id) {
    case PG_CREATE:
        break;
    case PG_COMMON_SETTINGS:
        break;
    case PG_PLUGIN_SETTINGS:
        PluginID pluginId = _wpgCreateAccount->plugin();
        IFactoryPlugin* plugin = _plugins[pluginId];
        _wpgPluginSettings->setWidget(plugin->dlgSettings());
        break;
    }       
}


PluginID WizardCreateAccount::pluginId()
{
    return _wpgCreateAccount->plugin();
}


QMap<QByteArray, QString> WizardCreateAccount::commonSettings() const
{
    auto map = _wpgCommonSettings->settings();
    return map;
}


QMap<QByteArray, QString> WizardCreateAccount::pluginSettings() const
{
    return _wpgPluginSettings->settings();
}
#ifndef DLGFACEBOOKAUTH_H
#define DLGFACEBOOKAUTH_H

#include <QDialog>

class QWebView;

namespace Ui {
class DlgFacebookAuth;
}

class DlgFacebookAuth : public QDialog
{
    Q_OBJECT

public:
    explicit DlgFacebookAuth(QWidget *parent = 0);
    ~DlgFacebookAuth();
    
    inline QString accessToken() const { return m_AccessToken; }
    
    int exec() override;

private:
    Ui::DlgFacebookAuth *ui;
    QWebView *_webView = nullptr;
    
    QString m_AccessToken;
    QString m_ErrorDescription;
    
    void loadAuthPage();
    void onUrlChanged();
};

#endif // DLGFACEBOOKAUTH_H

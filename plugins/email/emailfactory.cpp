#include "emailfactory.h"
#include "emailhistoryimporter.h"
#include "dlgemailsettings.h"

#include <QtGui/QIcon>

EmailFactory::~EmailFactory()
{
}


bool EmailFactory::hasFeature(HistoryImporter::Features feature) const
{
    using namespace HistoryImporter;
    switch(feature) {
        case Features::ConfigDialog:
            return true;
    }
    return false;
}


IHistoryImporterPtr EmailFactory::create()
{
    return std::shared_ptr<IHistoryImporter>(new EmailHistoryImporter(this));
}


const QString EmailFactory::prettyName() const
{
    return "e-mail";
}


const QString EmailFactory::description() const
{
    return tr("Electronic mail.");
}


const QByteArray EmailFactory::systemName() const
{
    return "email";
}

const QString EmailFactory::version() const
{
    return "0.1";
}


DlgPluginSettings* EmailFactory::dlgSettings() const
{
    return new DlgEmailSettings();
}


QIcon* EmailFactory::icon() const
{
    QPixmap *pixmap = new QPixmap(":/icons/email_icon.png");
    pixmap->scaled(32, 32, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    QIcon* icon = new QIcon(*pixmap);
    return icon;
}
#ifndef WPGACCOUNTCOMMONSETTINGS_H
#define WPGACCOUNTCOMMONSETTINGS_H

#include <QWizardPage>

namespace Ui {
class WpgAccountCommonSettings;
}

class WpgAccountCommonSettings : public QWizardPage
{
    Q_OBJECT
    
public:
    explicit WpgAccountCommonSettings(QWidget *parent = 0);
    ~WpgAccountCommonSettings();
    
    QMap<QByteArray, QString> settings() const;
    void setSettings(const QMap<QByteArray, QString>&);
    
private:
    Ui::WpgAccountCommonSettings *ui;
};

#endif // WPGACCOUNTCOMMONSETTINGS_H

#include "psihistoryimporter.h"

#include <iostream>

#include <QtCore/QDir>
#include <QtCore/QRegularExpression>
#include <QtCore/QUrl>
#include <QtCore/QVector>
#include <QtCore/QSettings>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlDatabase>
#include <QtCore/QLinkedList>



const QString SQL_PREFIX = "psi";
const char NAME[] = "psi";

PsiHistoryImporter::PsiHistoryImporter(const IFactoryPlugin *parent) :
    IHistoryImporter(parent)
{
}

PsiHistoryImporter::~PsiHistoryImporter()
{

}

void PsiHistoryImporter::init()
{
    QSettings s(_configFilename, QSettings::IniFormat);
    s.beginGroup("main");
    _path = s.value("path", QDir::homePath()).toString();
}


QString logdecode(const QString &str)
{
    QString ret;

    for(int n = 0; n < str.length(); ++n) {
        if(str.at(n) == '\\') {
            ++n;
            if(n >= str.length()) {
                break;
            }
            if(str.at(n) == 'n') {
                ret.append('\n');
            }
            if(str.at(n) == 'p') {
                ret.append('|');
            }
            if(str.at(n) == '\\') {
                ret.append('\\');
            }
        } else {
            ret.append(str.at(n));
        }
    }

    return ret;
}

Message PsiHistoryImporter::parseLine(const QByteArray &line)
{
    QString sTime, sType, sOrigin, sFlags, sText, sSubj, sUrl, sUrlDesc;
    int x1, x2;
    x1 = line.indexOf('|') + 1;

    x2 = line.indexOf('|', x1);
    sTime = line.mid(x1, x2-x1);
    x1 = x2 + 1;

    x2 = line.indexOf('|', x1);
    sType = line.mid(x1, x2-x1);
    x1 = x2 + 1;

    x2 = line.indexOf('|', x1);
    sOrigin = line.mid(x1, x2-x1);
    x1 = x2 + 1;

    x2 = line.indexOf('|', x1);
    sFlags = line.mid(x1, x2-x1);
    x1 = x2 + 1;

    // check for extra fields
    if(sFlags[1] != '-') {
        int subflags = QString(sFlags[1]).toInt(NULL,16);

        // have subject?
        if(subflags & 1) {
            x2 = line.indexOf('|', x1);
            sSubj = line.mid(x1, x2-x1);
            x1 = x2 + 1;
        }
        // have url?
        if(subflags & 2) {
            x2 = line.indexOf('|', x1);
            sUrl = line.mid(x1, x2-x1);
            x1 = x2 + 1;
            x2 = line.indexOf('|', x1);
            sUrlDesc = line.mid(x1, x2-x1);
            x1 = x2 + 1;
        }
    }

    // body text is last
    sText = line.mid(x1);

    // -- read end --

    Message m;
    int type = sType.toInt();
    if(type == 0 || type == 1 || type == 4 || type == 5) {   
        m.date = QDateTime::fromString(sTime, Qt::ISODate);
        //if(type == 1)
        //m.setType("chat");
        //else if(type == 4)
        //m.setType("error");
        //else if(type == 5)
        //m.setType("headline");
        //else
        //m.setType("");
        m.metadata[MAttrType] << type;

        //bool originLocal = (sOrigin == "to") ? true: false;
        m.out = (sOrigin == "to") ? true : false;
        if(sFlags[0] == 'N')
            m.body = logdecode(sText);
        else
            m.body = logdecode(QString::fromUtf8(sText.toLatin1()));
        m.title = logdecode(sSubj);

        QString url = logdecode(sUrl);
        if(!url.isEmpty()) {
            m.metadata[MAttrUrl] << url;
            m.metadata[MAttrUrlDesc] << logdecode(sUrlDesc);
        }
        m.body.chop(1);
        return m;
    }
    else if(type == 2 || type == 3 || type == 6 || type == 7 || type == 8) {
        QString subType = "subscribe";
        if(type == 2) {
            // stupid "system message" from Psi <= 0.8.6
            // try to figure out what kind it REALLY is based on the text
            if(sText == QObject::tr("<big>[System Message]</big><br>You are now authorized."))
                subType = "subscribed";
            else if(sText == QObject::tr("<big>[System Message]</big><br>Your authorization has been removed!"))
                subType = "unsubscribed";
         }
         else if(type == 3)
            subType = "subscribe";
         else if(type == 6)
            subType = "subscribed";
        else if(type == 7)
            subType = "unsubscribe";
        else if(type == 8)
            subType = "unsubscribed";

        m.date = QDateTime::fromString(sTime, Qt::ISODate);
        
        m.metadata[MAttrType] << type;
        m.body = sText;
        return m;
    }

    return m;
}

/*QVector<Message> PsiHistoryImporter::parseMessages(const QString &fullpath)
{
    QVector<Message> list;
    
    QFile file(fullpath);
    if(!file.open(QIODevice::ReadOnly)) {
        std::cerr << Q_FUNC_INFO << ':' << __LINE__ << " can't open file: " << fullpath.toStdString() << std::endl;
    }
    

    
    while(!file.atEnd()) {
        Message message = parseLine(file.readLine()); 
        message.contact_id = jid;
        //std::cout << message->body.toStdString() << std::endl;
        list.append(message);
    }    
    file.close();
    
    return list;
}
*/

void PsiHistoryImporter::sync()
{
    std::cout << "psi sync" << std::endl;
    emit importStarted();
    
    QDir dir(_path);
    QStringList filters;
    filters << "*.history";
    QStringList entries = dir.entryList(filters, QDir::Files);
    int totalEntries = entries.count();
    int currentEntry = 0;
    for(QString filename : entries) {
        QString contact(filename);
        contact.replace(QString("_at_"), QString("@"));
        contact = contact.replace(QRegularExpression("\\.history$"), "");
        contact = QUrl::fromEncoded(contact.toUtf8()).toString();
        qDebug("%s \t -> \t %s", qPrintable(filename), qPrintable(contact));
        QString fullpath = dir.path() + "/" + filename;
        std::cout << "Full path: " << fullpath.toStdString() << std::endl;
        
        //int32_t jid = fillDb(contact);
       // QVector<Message>> messages = parseMessages(fullpath, jid);
        
        
       // fillDb(messages);
        ++currentEntry;
        emit importProgress( currentEntry * 100 / totalEntries );
    }
    emit importFinished();
}

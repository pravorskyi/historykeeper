#ifndef DLGUSER_H
#define DLGUSER_H

#include <QDialog>

namespace Ui {
class DlgUser;
}

class DlgUser : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgUser(QWidget *parent = 0);
    ~DlgUser();
    void setFirstName(const QString &firstName);
    void setLastName(const QString &lastName);
    
private:
    Ui::DlgUser *ui;
};

#endif // DLGUSER_H

#ifndef ROSTERPRIVATE_H
#define ROSTERPRIVATE_H

#include "typedefs.h"
#include <QtWidgets/QTreeWidget>

enum RosterItemType {
    AccountType                 = QTreeWidgetItem::UserType,
    ContactType                 = QTreeWidgetItem::UserType + 1,
    MetacontactsRootType        = QTreeWidgetItem::UserType + 2,
    MetacontactType             = QTreeWidgetItem::UserType + 3
};


class RosterContactItem : public QTreeWidgetItem {
public:
    explicit RosterContactItem(QTreeWidgetItem *parent) :
        QTreeWidgetItem(parent, ContactType)
        { }
    AccountID accountId = -1;
    ContactID contact_id = -1;
};

class RosterAccountItem : public QTreeWidgetItem {
public:
    explicit RosterAccountItem(QTreeWidgetItem *parent) :
        QTreeWidgetItem(parent, AccountType)
        { }
    AccountID accountId;
    QHash<ContactID, RosterContactItem*> hashContactItems;
};



class RosterMetacontactsRootItem : public QTreeWidgetItem {
public:
    explicit RosterMetacontactsRootItem(QTreeWidgetItem *parent) :
        QTreeWidgetItem(parent, MetacontactsRootType)
        {   }
};

class RosterMetacontactItem : public QTreeWidgetItem {
public:
    explicit RosterMetacontactItem(QTreeWidgetItem *parent) :
        QTreeWidgetItem(parent, MetacontactType)
        {   }
    MetacontactID id = 0;
};

class RosterPrivate: public QTreeWidget
{
    Q_OBJECT
public:
    explicit RosterPrivate(QWidget* parent = 0);    
    
protected:
    void dropEvent(QDropEvent* event) override;
    void startDrag(Qt::DropActions supportedActions) override;
    
private:
    QTreeWidgetItem* _currentItemForDrop;
    
signals:
    void droppedItem(RosterContactItem* item);
};

#endif
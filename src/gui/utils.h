#ifndef UTILS_H
#define UTILS_H

#include <QtCore/QString>

class Utils
{
public:
    static void processError(const QString &str);
};

#endif // UTILS_H

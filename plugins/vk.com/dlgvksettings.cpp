#include "dlgvksettings.h"
#include "ui_dlgvksettings.h"
#include "dlgvkauth.h"

#include "globals.h"

DlgVkSettings::DlgVkSettings(QWidget *parent) :
    DlgPluginSettings(parent),
    ui(new Ui::DlgVkSettings)
{
    ui->setupUi(this);
    
    ui->edtAppID->setPlaceholderText(QString::number(VKCOM_DEFAULT_APP_ID));
    
    //FIXME: add auth login/password if possible
    ui->rbtnToken->setChecked(true);
    ui->rbtnAutologin->setEnabled(false);
    //END
    
    connect(ui->pushButton, &QPushButton::clicked, this, &DlgVkSettings::onTokenRequest);
    connect(ui->rbtnAutologin, &QRadioButton::toggled, this, &DlgVkSettings::onAuthTypeChanged);
    //input changed
    connect(ui->rbtnAutologin, &QRadioButton::toggled, this, &DlgVkSettings::onDataChanged);
    connect(ui->edtLogin, &QLineEdit::textEdited, this, &DlgVkSettings::onDataChanged);
    connect(ui->edtPassword, &QLineEdit::textEdited, this, &DlgVkSettings::onDataChanged);
    connect(ui->edtAppID, &QLineEdit::textEdited, this, &DlgVkSettings::onDataChanged);
    
    onAuthTypeChanged();
}

DlgVkSettings::~DlgVkSettings()
{
    delete ui;
}


bool DlgVkSettings::isValid() const
{
    if(ui->rbtnAutologin->isChecked()) {
        if(ui->edtLogin->text().isEmpty() || ui->edtPassword->text().isEmpty())
            return false;
    }
    
    if(ui->rbtnToken->isChecked()) {
        if(ui->lblToken->text() == "<none>")
            return false;
    }
    
    return true;
}


QMap<QByteArray, QString> DlgVkSettings::settings() const
{
    QMap<QByteArray, QString> map;
    
    if(ui->rbtnAutologin->isChecked()) {
        map["AuthType"] = "Autologin";
        map["Login"] = ui->edtLogin->text();
        map["Password"] = ui->edtPassword->text();
    }
    
    if(ui->rbtnToken->isChecked()) {
        map["Token"] = ui->lblToken->text();
    }
    
    if(ui->boxCustomAppID->isChecked())
        map["APP_ID"] = ui->edtAppID->text();
    
    return map;
}


void DlgVkSettings::setSettings(const QMap<QByteArray, QString> &settings)
{
    ui->lblToken->setText(settings.value("Token", "<none>"));
}


void DlgVkSettings::onAuthTypeChanged()
{
    ui->frameAuthAutologin->setEnabled(ui->rbtnAutologin->isChecked());
    ui->frameAuthToken->setEnabled(ui->rbtnToken->isChecked());
}


void DlgVkSettings::onDataChanged()
{
    emit completeChanged();
}


void DlgVkSettings::onTokenRequest()
{
    QPointer<DlgVkAuth> dlg = new DlgVkAuth;
    dlg->exec();
    ui->lblToken->setText(dlg->accessToken());
    delete dlg;
    emit completeChanged();
}


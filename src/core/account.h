#ifndef ACCOUNT_H
#define ACCOUNT_H

#include "IPlugin.h"

class Account
{
public:
    Account(AccountID, const IHistoryImporterPtr&);

    inline AccountID                                id() const { return _id; }
    inline IHistoryImporterPtr                      importer() { return _importer; }
    inline std::shared_ptr<const IHistoryImporter>  importer() const { return _importer; }
    inline AccountSettings                          settings() const { return _settings; }
    
    void setSettings(const AccountSettings&);

private:
    AccountID _id;
    IHistoryImporterPtr _importer;   
    AccountSettings _settings;
};


#endif // ACCOUNT_H
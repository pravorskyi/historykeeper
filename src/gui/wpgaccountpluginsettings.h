#ifndef WPGACCOUNTPLUGINSETTINGST_H
#define WPGACCOUNTPLUGINSETTINGS_H

#include "typedefs.h"
#include "IPlugin.h"
#include <QtWidgets/QWizardPage>

class WpgAccountPluginSettings : public QWizardPage
{
    Q_OBJECT
    
public:
    explicit WpgAccountPluginSettings(QWidget *parent = 0);
    virtual ~WpgAccountPluginSettings();
    
    bool isComplete() const override;
    
    QMap<QByteArray, QString> settings() const;
    void setWidget(DlgPluginSettings* widget);

private:
    DlgPluginSettings* _widget  = nullptr;
    
};

#endif // WPGACCOUNTPLUGINSETTINGS_H
#ifndef VKCOMHISTORYIMPORTER_H
#define VKCOMHISTORYIMPORTER_H

#include "IPlugin.h"
#include "networksheduler.h"

#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QTimer>


class VkComHistoryImporter : public IHistoryImporter
{
    Q_OBJECT
public:
    explicit VkComHistoryImporter(const IFactoryPlugin *parent);
    ~VkComHistoryImporter();
    bool hasFeature(HistoryImporter::Features feature) const;
    
    void sync();
    void init() { };
    virtual void setSettings(const QMap<QByteArray, QString>& settings) override;
    
private:
    enum ContactAttributes {
        CAttrDeactivated        = 1, // string
        CAttrFirstName          = 2, // string
        CAttrLastName           = 3  // string
    };
    
    enum MessageAttributes {
        MAttrEmoji              = 1
    };
    
    
    QSet<int> _contactsToDownload;
    void downloadMessages(qint8 out, int count, int offset);
    void downloadContacts(const QSet<int> &uids);
    void onReply(QNetworkReply* reply);

    void onNetworkShedule();
    void processMessages(const QJsonArray &data);
    void processUsers(const QJsonArray &data);
    
    int _importProgress = 0;
    int _downloadedCount = 0;
    NetworkSheduler _networkSheduler;
    
    //INTERNAL SETTINGS
    QByteArray  _token;
    int _userID = 0;
};

#endif // VKCOMHISTORYIMPORTER_H

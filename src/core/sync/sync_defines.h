#ifndef SYNC_DEFINES_H
#define SYNC_DEFINES_H

#include <unistd.h>
#include "../message.h"
#include "../account.h"
#include <QtCore/QDataStream>


enum SyncFrameType : unsigned char
{
    EActionCreateLogin      = 1,
    EResponseCreateLogin    = 2,
    EActionSyncPull         = 3,
    EResponseSyncPull       = 4,
    EActionSyncPush         = 5,
    EResponseSyncPush       = 6,
    EActionAuthentication   = 7,
    EResponseAuthentication = 8
};


QDataStream& operator <<(QDataStream &out, const Message &m)
{
    out << m.account_id
        << m.body
        << static_cast<qint64>(m.contact_id)
        << m.date
        << static_cast<qint64>(m.id)
        << static_cast<qint64>(m.index)
        << m.out
        << m.title;
    return out;
}

QDataStream& operator >>(QDataStream &out, Message &m)
{
    qint64 contactID, id, index; //TODO: refactoring
    out >> m.account_id
        >> m.body
        >> contactID
        >> m.date
        >> id
        >> index
        >> m.out
        >> m.title;
    m.contact_id = contactID;                                                                                                                                                                                     
    m.id = id;                                                                                                                                                                                                    
    m.index = index;                                                                                                                                                                                              
    return out;                                                                                                                                                                                                   
}


#endif // SYNC_DEFINES_H
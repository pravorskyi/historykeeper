#ifndef DLGVKAUTH_H
#define DLGVKAUTH_H

#include <QtWidgets/QDialog>

namespace Ui {
class DlgVkAuth;
}

class DlgVkAuth : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgVkAuth(QWidget *parent = 0);
    ~DlgVkAuth();
    void loadAuthPage();
    inline QString accessToken() const { return m_AccessToken; }
    inline QString errorDescription() const { return m_ErrorDescription; }
    inline QString userID() const { return m_UserID; }

    virtual int exec() override;
private:
    Ui::DlgVkAuth *ui;
    QString m_AccessToken;
    QString m_ErrorDescription;
    QString m_UserID;

    void onUrlChanged();
};

#endif // DLGVKAUTH_H

#ifndef MESSAGE_H
#define MESSAGE_H

#include <QtCore/QDateTime>
#include <QtCore/QString>
#include <QtCore/QVector>

#include "typedefs.h"
#include <string>

class Message {
public:
    explicit Message() { };
    MessageID id    = -1; // уникальный id сообщения
    void init();
    QString title;
    QString body;
    QDateTime date;
    bool out            = false;
    ContactID contact_id;
    
    int64_t index;      //номер п/п
    AccountID account_id = -1;
};

#endif // MESSAGE_H

#ifndef DLGADVANCEDSEARCH_H
#define DLGADVANCEDSEARCH_H

#include <QDialog>

enum class SearchType {
    Contain,
    Regexp,
    SimpleMask
};

namespace Ui {
class DlgAdvancedSearch;
}

class DlgAdvancedSearch : public QDialog
{
    Q_OBJECT
public:
    explicit DlgAdvancedSearch(QWidget *parent = 0);
    ~DlgAdvancedSearch();

private:
    Ui::DlgAdvancedSearch* ui;
    void runSearch();

    
signals:
    void search(const SearchType &searchType, const QString &pattern);
};

#endif // DLGADVANCEDSEARCH_H

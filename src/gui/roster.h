#ifndef ROSTER_H
#define ROSTER_H

#include "typedefs.h"
#include <QtWidgets/QWidget>

class Contact;
class Metacontact;

class RosterAccountItem;
class RosterContactItem;
class RosterMetacontactItem;
class RosterMetacontactsRootItem;
class RosterPrivate;
class QTreeWidgetItem;
class QLineEdit;

class Roster : public QWidget {
    Q_OBJECT
public:
    enum class Item{
        Metacontact,
        MetacontactRoot,
        Contact,
        Account,
        None
    };

    explicit Roster(QWidget* parent = 0);
    
    void addAccount(AccountID, const QString &name, QIcon* icon = 0);
    void removeAccount(AccountID);   
    void addMetacontact(const Metacontact &);
    void addContact(AccountID, const Contact &);
    void removeMetacontact();

    Item currentItemType() const;
    AccountID currentAccount() const;
    ContactID currentContact() const;
    MetacontactID currentMetacontact() const;

    
private:
    RosterContactItem* contact(AccountID accountId, ContactID contact_id);
    RosterMetacontactsRootItem *_metacontactsRootItem;
    QIcon *_metacontactsIcon;
    QHash<AccountID, RosterAccountItem*> _mapAccounts; 
    QHash<MetacontactID, RosterMetacontactItem*> _metacontacts;
    
    RosterPrivate* _rosterPrivate       = nullptr;
    QLineEdit* _edtFilter               = nullptr;
    
    void filter(const QString &text);
    void removeMetacontactByItem(RosterMetacontactItem*);
    void onCustomContextMenuRequested(const QPoint&);
    void onItemClicked(QTreeWidgetItem* item);
    
    RosterAccountItem* accountItem(AccountID accountId) const;
    void onAccountSync();
    void onAccountDeleteAllMessages();
    void onMetacontactRemove();
    void onItemDropped(RosterContactItem* item);
    
signals:

    void contactSelected(AccountID, ContactID);
    void metacontactSelected(MetacontactID);
    void sigMetacontactRemoved(MetacontactID);
    void sigAccountSync(AccountID);
    void sigAccountDeleteAllMessages(AccountID);
    void sigMetacontactAdd();
    void sigMetacontactAddContact(MetacontactID, AccountID, ContactID);
};



#endif // ROSTER_H

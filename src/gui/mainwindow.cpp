#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../core/account.h"
#include "../core/hkcore_global.h"
#include "../core/hkcore.h"
#include "../core/metacontact.h"
#include "dlguser.h"
#include "dlgplugins.h"
#include "dlgsyncsettings.h"
#include "utils.h"
#include "roster.h"
#include "dlgaccounts.h"
#include "dlgadvancedsearch.h"
#include "guisettings.h"
#include "messagemodel.h"


#include <QtCore/QDir>
#include <QtCore/QUrl>
#include <QtCore/QString>
#include <QtCore/QPointer>
#include <QtCore/QSettings>
#include <QtCore/QVariant>
#include <QtCore/QRegExp>
#include <QtCore/QElapsedTimer>

#include <QtCore/QPluginLoader>
#include <QtCore/QRegularExpression>
#include <QtCore/QThread>
#include <QtCore/QDebug>

#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <QtWidgets/QAction>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTreeWidgetItem>
#include <QtWidgets/QInputDialog>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QStringBuilder>

#include <QtSql/QSqlDriver>
#include <QtSql/QSqlError>

#include <QPixmap>
#include <QIcon>

#include <iostream>


//TODO:
// http://stackoverflow.com/questions/161738/what-is-the-best-regular-expression-to-check-if-a-string-is-a-valid-url

//TODO: remove
#include <QtGui/QStandardItemModel>
#include <QtCore/QAbstractItemModel>
#include "htmldelegate.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qRegisterMetaType<int32_t>("int32_t");
    qRegisterMetaType<QVector<Contact>>("QVector<Contact>");
    qRegisterMetaType<QVector<Message>>("QVector<Message>");

    ui->setupUi(this);
    QVBoxLayout* layout = new QVBoxLayout(ui->centralWidget);

    _splitter = new QSplitter(ui->centralWidget);
    _roster = new Roster(this);
    _splitter->addWidget(_roster);
    layout->addWidget(_splitter);

    _progressWidget = new QLabel(this);
    ui->statusBar->addWidget(_progressWidget);

    _progressBar = new QProgressBar(ui->statusBar);
    ui->statusBar->addWidget(_progressBar);
    _progressBar->hide();

    _hkcore = new HKCore;
    _hkcoreThread = new QThread(this);
    _hkcoreThread->setObjectName("core");
    _hkcore->moveToThread(_hkcoreThread);

    _messageModel = new MessageModel();//FIXME:
    //FIXME: ui->viewMessages->setModel(_messageModel);
    //FIXME: HTMLDelegate* delegate = new HTMLDelegate(this);
    //FIXME: ui->viewMessages->setItemDelegate(delegate);

    _roster->setFocus();
    _roster->setBaseSize(200, _roster->height());

    connect(qApp, &QCoreApplication::aboutToQuit, this, &MainWindow::onAboutToQuit);
    connect(ui->actionAccounts, &QAction::triggered, this, &MainWindow::showDlgAccounts);
    connect(ui->actionPlugins, &QAction::triggered, this, &MainWindow::showDlgPlugins);
    connect(ui->actionSync_all, &QAction::triggered, _hkcore, &HKCore::syncAllAccounts);
    connect(ui->actionSync_devices, &QAction::triggered, _hkcore, &HKCore::syncDevices);
    connect(ui->actionAdd_metacontact, &QAction::triggered, this, &MainWindow::addMetacontact);
    connect(ui->actionRemove_metacontact, &QAction::triggered, _roster, &Roster::removeMetacontact);
    connect(ui->actionAbout_program, &QAction::triggered, this, &MainWindow::showDlgAbout);
    connect(ui->actionAbout_Qt, &QAction::triggered, this, &MainWindow::showDlgAboutQt);
    connect(ui->actionSettings, &QAction::triggered, this, &MainWindow::showDlgSettings);
    connect(ui->actionSyncronization_settings, &QAction::triggered, this, &MainWindow::showDlgSyncSettings);
    //FIXME: connect(ui->btnAdvancedSearch, &QPushButton::clicked, this, &MainWindow::showDlgAdvancedSearch);
    //FIXME: connect(ui->edtSearch, &QLineEdit::returnPressed, this, &MainWindow::onSearchReturnPressed);
    //FIXME: connect(ui->chkbxLiveSearch, &QCheckBox::stateChanged, this, &MainWindow::onLiveSearchChanged);
    connect(_hkcore, &HKCore::sigAccountAdd, this, &MainWindow::onAccountAdd);
    connect(_hkcore, &HKCore::sigAccountDeleted, _roster, &Roster::removeAccount);
    connect(_hkcore, &HKCore::sigAccountError, this, &MainWindow::onAccountError);
    connect(_hkcore, &HKCore::sigAccountImportStarted, this, &MainWindow::onImportStarted);
    connect(_hkcore, &HKCore::sigAccountImportProgress, this, &MainWindow::onImportProgress);
    connect(_hkcore, &HKCore::sigAccountImportFinished, this, &MainWindow::onImportFinished);
    connect(_roster, &Roster::contactSelected, this, &MainWindow::onRosterContactSelected);
    connect(_roster, &Roster::metacontactSelected, this, &MainWindow::onRosterMetacontactSelected);
    connect(_roster, &Roster::sigAccountDeleteAllMessages, _hkcore, &HKCore::deleteAllMessages);
    connect(_roster, &Roster::sigAccountSync, _hkcore, &HKCore::syncAccount);
    connect(_roster, &Roster::sigMetacontactAdd, this, &MainWindow::addMetacontact);
    connect(_roster, &Roster::sigMetacontactAddContact, _hkcore, &HKCore::metacontactAddContact);
    connect(_roster, &Roster::sigMetacontactRemoved, _hkcore, &HKCore::metacontactRemove);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::onAboutToQuit()
{
    QSettings s;
    s.beginGroup(SETTING_GROUP_MAINWINDOW);

    bool isMaximized = this->isMaximized();
    s.setValue(SETTING_MAINWINDOW_MAXIMIZED, isMaximized);
    if(!isMaximized) {
        s.setValue(SETTING_MAINWINDOW_GEOMETRY, this->saveGeometry());
    }
}


bool MainWindow::init()
{
    _hkcoreThread->start();
    QSettings s;
    //FIXME: ui->chkbxLiveSearch->setCheckState(static_cast<Qt::CheckState>(s.value("live_search", Qt::Unchecked).toInt()));

    s.beginGroup(SETTING_GROUP_MAINWINDOW);
    bool isMaximized = s.value(SETTING_MAINWINDOW_MAXIMIZED, false).toBool();

    if(isMaximized)
        this->setWindowState(Qt::WindowMaximized);
    else
        this->restoreGeometry(s.value(SETTING_MAINWINDOW_GEOMETRY).toByteArray());

    if(!_hkcore->init())
        return false;
    qApp->processEvents();
    qWarning() << "Core initialized.";

    initRoster();

    createRichTextWidget();


    //FIXME possible memory leak

    //FIXME:
    /*
    QMenu* nodeMenu = new QMenu(_roster);
    nodeMenu->addAction(ui->actionAdd_metacontact);
    _roster->addAction(ui->actionAdd_metacontact);

    ui->menuExport->addAction(ui->action_HTML);
    ui->menuExport->addAction(ui->action_JSON);
    ui->menuExport->addAction(ui->action_XML);

    QMenu* exportMenu = new QMenu(ui->btnExport);
    exportMenu->addAction(ui->action_HTML);
    exportMenu->addAction(ui->action_JSON);
    exportMenu->addAction(ui->action_XML);
    connect(ui->btnExport, &QPushButton::clicked, [=]() {
        exportMenu->exec(QCursor::pos());
    } );
    connect(ui->action_HTML, &QAction::triggered, this, &MainWindow::exportMessagesToHTML);
    connect(ui->action_JSON, &QAction::triggered, this, &MainWindow::exportMessagesToJSON);
    connect(ui->action_XML, &QAction::triggered, this, &MainWindow::exportMessagesToXML);

    */
    return true;
}


void MainWindow::initRoster()
{
    auto metacontacts = _hkcore->metacontacts();
    for(const auto &m : metacontacts)
        _roster->addMetacontact(m);
}


void MainWindow::createRichTextWidget()
{
    _browser = new QTextBrowser();
    _splitter->insertWidget(0, _browser);
}


void MainWindow::setMessages(const QHash<AccountID, QHash<ContactID, Contact>> &contacts, const QVector<Message> &messages)
{
    _messageModel->setContacts(contacts);
    _messageModel->setMessages(messages);
}

void MainWindow::onRosterContactSelected(AccountID accountId, ContactID contactId)
{
    QHash<AccountID, QHash<ContactID, Contact>> contacts;
    contacts[accountId] = _hkcore->accountContacts(accountId);
    auto messages = _hkcore->messages(accountId, contactId);
// setMessages(contacts, messages);


    QString html;
    QElapsedTimer t;
    t.start();
    for(int i = 0; i < messages.count(); ++i)
    {
            const Message &m = messages.at(i);
    QString body = m.body;
    body.replace('\n', QLatin1String("<br>"));

    QString prepend;
    if(i == 0 ||  m.date.date() > messages.at(i - 1).date.date())
        prepend = "<b><font color=darkgreen>" % m.date.date().toString() % "</font></b><br>";

    QLatin1String color = m.out ? QLatin1String("blue") : QLatin1String("red");
    QString sender;
    if(m.out) {
        sender = tr("You");
    } else {
        sender = contacts.value(m.account_id).value(m.contact_id).name;
    }

    QString s = prepend % "<font color=" % color % ">[" % m.date.toString("hh:mm:ss") % "] " % sender % ":</font> "
        % body % "<br>";
        html += s;

    }
    int64_t elapsed = t.elapsed();
    _browser->setHtml(html);
    qDebug("RENDER TIME: %llims", t.elapsed() - elapsed);
}


void MainWindow::onRosterMetacontactSelected(MetacontactID metacontact_id)
{
    QHash<AccountID, QHash<ContactID, Contact>> contacts = _hkcore->metacontactContacts(metacontact_id);
    auto messages = _hkcore->metacontactMessages(metacontact_id);
//    setMessages(contacts, messages);


// setMessages(contacts, messages);


    QString html;
    QElapsedTimer t;
    t.start();
    for(int i = 0; i < messages.count(); ++i)
    {
            const Message &m = messages.at(i);
    QString body = m.body;
    body.replace('\n', QLatin1String("<br>"));

    QString prepend;
    if(i == 0 ||  m.date.date() > messages.at(i - 1).date.date())
        prepend = "<b><font color=darkgreen>" % m.date.date().toString() % "</font></b><br>";

    QLatin1String color = m.out ? QLatin1String("blue") : QLatin1String("red");
    QString sender;
    if(m.out) {
        sender = tr("You");
    } else {
        sender = contacts.value(m.account_id).value(m.contact_id).name;
    }

    QString s = prepend % "<font color=" % color % ">[" % m.date.toString("hh:mm:ss") % "] " % sender % ":</font> "
        % body % "<br>";
        html += s;

    }
    int64_t elapsed = t.elapsed();
    _browser->setHtml(html);
    qDebug("RENDER TIME: %llims", t.elapsed() - elapsed);
}


void MainWindow::onLiveSearchChanged(int /*state*/)
{
    //FIXME:
    /*if(state == Qt::Checked)
        connect(ui->edtSearch, &QLineEdit::textEdited, this, &MainWindow::search);
    else
        disconnect(ui->edtSearch, &QLineEdit::textEdited, this, &MainWindow::search);

    QSettings s;
    s.setValue("live_search", state);
    */
}

bool MainWindow::searchFilter(const Message &m, const SearchType &searchType, const QString &pattern)
{
    switch(searchType) {
    case SearchType::Contain: {
            if(m.body.contains(pattern))
                return true;
             break;
        }
    case SearchType::Regexp: {
            if(m.body.contains(QRegularExpression(pattern)))
                return true;
             break;
        }
    case SearchType::SimpleMask: {
            if(m.body.contains(QRegExp(pattern, Qt::CaseInsensitive, QRegExp::Wildcard)))
                return true;
            break;
        }
    }

    return false;
}

void MainWindow::onSearchReturnPressed()
{
    //FIXME: search(ui->edtSearch->text());
}

void MainWindow::search(const QString &searchText)
{
    if(searchText.length() < 3) //TODO
        return;

    advancedSearch(SearchType::Contain, searchText);
}

void MainWindow::advancedSearch(const SearchType &searchType, const QString &pattern)
{
    switch(_roster->currentItemType()) {
    case Roster::Item::Contact: {
        AccountID a_id = _roster->currentAccount();
        ContactID c_id = _roster->currentContact();

        const QVector<Message> foundList =  searchForContact(a_id, c_id, searchType, pattern);
        QHash<AccountID, QHash<ContactID, Contact>> contacts;
        contacts[a_id] = _hkcore->accountContacts(a_id);
        setMessages(contacts, foundList);
        }
        break;
    case Roster::Item::Account: {
        AccountID a_id = _roster->currentAccount();
        const QVector<Message> foundList =  searchForAccount(a_id, searchType, pattern);
        QHash<AccountID, QHash<ContactID, Contact>> contacts;
        contacts[a_id] = _hkcore->accountContacts(a_id);
        setMessages(contacts, foundList);
        }
        break;
    case Roster::Item::Metacontact: {
        MetacontactID m_id = _roster->currentMetacontact();
        std::cout << Q_FUNC_INFO << ": metacontact id " << m_id << std::endl;
        QVector<Message> foundList = searchForMetacontact(m_id,  searchType, pattern);
        QHash<AccountID, QHash<ContactID, Contact>> contacts = _hkcore->metacontactContacts(m_id);
        setMessages(contacts, foundList);
        }
        break;
    case Roster::Item::MetacontactRoot:
        break;
    case Roster::Item::None:
        break;
    }
}



QVector<Message> MainWindow::searchForContact(AccountID a_id, ContactID c_id, const SearchType &searchType, const QString &text)
{
    auto messages = _hkcore->messages(a_id, c_id);
    QVector<Message> result;
    for(const Message &m : messages) {
        if(searchFilter(m, searchType, text))
            result.append(m);
    }
    return result;

}


QVector<Message> MainWindow::searchForAccount(AccountID a_id, const SearchType &searchType, const QString &text)
{
    QVector<Message> messages;
    auto contacts = _hkcore->accountContacts(a_id);
    for(ContactID c_id : contacts.keys())
        messages <<  _hkcore->messages(a_id, c_id);


    QVector<Message> result;
    for(const Message &m : messages) {
        if(searchFilter(m, searchType, text))
            result.append(m);
    }
    return result;
}

QVector<Message> MainWindow::searchForMetacontact(MetacontactID m_id, const SearchType &searchType, const QString& text)
{

    QVector<Message> result;
    QVector<Message> messages = _hkcore->metacontactMessages(m_id);
    for(const Message &m : messages)
        if(searchFilter(m, searchType, text))
            result.append(m);

    return result;
}


void MainWindow::showDlgPlugins()
{
    QPointer<DlgPlugins> dlg = new DlgPlugins();
    dlg->setPlugins(_hkcore->plugins());
    dlg->exec();
    delete dlg;
}


void MainWindow::onAccountAdd(AccountID id)
{
    _roster->addAccount(id, _hkcore->account(id)->settings().value(ACCOUNT_SETTING_COMMON).value("Name"), _hkcore->accountIcon(id));

    for(const auto &c: _hkcore->accountContacts(id))
        _roster->addContact(id, c);

    emit sigNewAccount(_hkcore->accounts()[id]);
}


void MainWindow::addMetacontact()
{
    QString name = QInputDialog::getText(this, tr("Add metacontact"), "Enter name:");

    if(name.isEmpty())
        return;

    int id = _hkcore->addMetacontact(name);
    if(id == -1) {
        std::cerr << Q_FUNC_INFO << " can't add metacontact" << std::endl;
    }

    Metacontact m;
    m.id = id;
    m.name = name;
    _roster->addMetacontact(m);
}

void MainWindow::onAccountError(AccountID a_id, const QString errorString)
{
    QMessageBox::critical(this, tr("Account error"),
                          QString("%1: %2").arg(_hkcore->account(a_id)->settings().value(ACCOUNT_SETTING_COMMON).value("Name"), errorString));
}


void MainWindow::onDeleteAllMessages()
{

}


void MainWindow::onImportStarted(AccountID)
{
    if(!_progressBar) {
        _progressBar = new QProgressBar(this);
        ui->statusBar->addWidget(_progressBar);
    }
    _progressBar->show();

}


void MainWindow::onImportProgress(AccountID, int32_t progress)
{
    _progressBar->setValue(progress);
}


void MainWindow::onImportFinished(AccountID id)
{
    _progressBar->hide();
    _progressBar->setValue(0);
    for(const auto &c: _hkcore->accountContacts(id))
        _roster->addContact(id, c);
}


void MainWindow::showDlgAbout()
{
    QMessageBox::about(this, "About...", "HistoryKeeper\n Version 0.1 beta\n\n(c) Pravorskyi Andrii, 2013");
}

void MainWindow::showDlgAboutQt()
{
    QMessageBox::aboutQt(this, tr("About Qt"));
}


void MainWindow::showDlgAccounts()
{
    QPointer<DlgAccounts> dlg = new DlgAccounts();
    connect(dlg.data(), &DlgAccounts::createAccount, _hkcore, &HKCore::addAccount);
    connect(dlg.data(), &DlgAccounts::deleteAccount, _hkcore, &HKCore::deleteAccount);
    connect(dlg.data(), &DlgAccounts::editAccount, _hkcore, &HKCore::editAccount);
    connect(this, &MainWindow::sigNewAccount, dlg.data(), &DlgAccounts::appendAccount);

    dlg->setAccounts(_hkcore->accounts());
    dlg->setPlugins(_hkcore->plugins());
    dlg->exec();
    delete dlg;
}


void MainWindow::showDlgAdvancedSearch()
{
    DlgAdvancedSearch *dlg = new DlgAdvancedSearch();
    dlg->setModal(false);
    connect(dlg, &DlgAdvancedSearch::search, this, &MainWindow::advancedSearch);
    connect(dlg, &QDialog::accepted, dlg, &QObject::deleteLater);
    dlg->open();
}


void MainWindow::showDlgSettings()
{

}


void MainWindow::showDlgSyncSettings()
{
    QPointer<DlgSyncSettings> dlg = new DlgSyncSettings;
    connect(dlg.data(), &DlgSyncSettings::createAccount, _hkcore, &HKCore::createSyncAccount);
    if(!dlg->init()) {
        qWarning() << "Can't init DlgSyncSettings";
        return;
    }

    dlg->exec();
}


void MainWindow::showError(const QString &errorString)
{
    QMessageBox::critical(this, "Error", errorString);
}

void MainWindow::exportMessagesToXML()
{
    QFileDialog dialog;
    dialog.setNameFilters( { "XML (*.xml)"});
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    dialog.setDefaultSuffix(".xml");
    if (!dialog.exec()) {
        return;
    }
    QString filename = dialog.selectedFiles().first();

    QFile f(filename);
    f.open(QIODevice::WriteOnly | QIODevice::Truncate  );
    f.write(_hkcore->getXML(_messageModel->messages()).toUtf8());
    f.flush();
    f.close();
}

void MainWindow::exportMessagesToJSON()
{
    QFileDialog dialog;
    dialog.setNameFilters( { "JSON (*.json)"});
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    dialog.setDefaultSuffix(".json");
    if (!dialog.exec()) {
        return;
    }
    QString filename = dialog.selectedFiles().first();

    QFile f(filename);
    f.open(QIODevice::WriteOnly | QIODevice::Truncate  );
    f.write(_hkcore->getJSON(_messageModel->messages()).toUtf8());
    f.flush();
    f.close();
}

void MainWindow::exportMessagesToHTML()
{
    QFileDialog dialog;
    dialog.setNameFilters( { "HTML (*.html)"});
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    dialog.setDefaultSuffix(".html");
    if (!dialog.exec()) {
        return;
    }
    QString filename = dialog.selectedFiles().first();

    QFile f(filename);
    f.open(QIODevice::WriteOnly | QIODevice::Truncate  );
    f.write("<!DOCTYPE html>"
            "<head>"
            "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
            "<title>Messages</title>"
            "</head>"
            "<body>"
            );
    f.write(_hkcore->getHtml(_messageModel->messages()).toUtf8());
    f.write("</body></html>");
    f.flush();
    f.close();
}



#include "dlgaccounts.h"
#include "ui_dlgaccounts.h"
#include "dlgaccountedit.h"
#include "wizardcreateaccount.h"
#include "../core/account.h"

#include <QtWidgets/QWizard>

DlgAccounts::DlgAccounts(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgAccounts)
{
    ui->setupUi(this);
    
    connect(ui->buttonBox, &QDialogButtonBox::clicked, this, &QDialog::accept);
    connect(ui->btnAdd, &QPushButton::clicked, this, &DlgAccounts::addAccount);
    connect(ui->btnDelete, &QPushButton::clicked, this, &DlgAccounts::onDeleteAccount);
    connect(ui->btnEdit, &QPushButton::clicked, this, &DlgAccounts::onEditAccount);
}


DlgAccounts::~DlgAccounts()
{
    delete ui;
}


void DlgAccounts::setAccounts(const QHash<AccountID, AccountPtr> &accounts)
{
    _accounts = accounts;
    ui->tableWidget->setRowCount(0);
    
    for(const auto &account : _accounts)
       appendAccount(account);
}


void DlgAccounts::appendAccount(const AccountPtr &account)
{
    int row = ui->tableWidget->rowCount();
    ui->tableWidget->setRowCount(row + 1);

    QTableWidgetItem* itemName = new QTableWidgetItem(account->settings().value(ACCOUNT_SETTING_COMMON).value("Name"));
    ui->tableWidget->setItem(row, 0, itemName);
    itemName->setData(Qt::UserRole, account->id());

    QTableWidgetItem* itemPlugin = new QTableWidgetItem(account->importer()->plugin()->prettyName());
    ui->tableWidget->setItem(row, 1, itemPlugin);
}


void DlgAccounts::setPlugins(const QHash<PluginID, IFactoryPlugin*>& plugins)
{
    _plugins = plugins;
}


void DlgAccounts::addAccount()
{    
    QPointer<WizardCreateAccount> wizard = new WizardCreateAccount(_plugins, this);
 
    if(wizard->exec() == QDialog::Accepted) {
        PluginID pluginId = wizard->pluginId();
        AccountSettings settings;
        settings[ACCOUNT_SETTING_COMMON] = wizard->commonSettings();
        settings[ACCOUNT_SETTING_PLUGIN] = wizard->pluginSettings();
        
        emit createAccount(pluginId, settings);
    }
    delete wizard;
}


void DlgAccounts::onDeleteAccount()
{
    const int row = ui->tableWidget->currentRow();
    QTableWidgetItem* item = ui->tableWidget->item(row, 0);
    emit deleteAccount(item->data(Qt::UserRole).toInt());
    ui->tableWidget->removeRow(row);
}


void DlgAccounts::onEditAccount()
{
    const int row = ui->tableWidget->currentRow();
    QTableWidgetItem* item = ui->tableWidget->item(row, 0);
    AccountID a_id = item->data(Qt::UserRole).toInt();
    
    QPointer<DlgAccountEdit> dlg = new DlgAccountEdit;    
    QPointer<DlgPluginSettings> dlgPluginSettings = _accounts[a_id]->importer()->plugin()->dlgSettings();
    dlg->setDlgPluginSettings(dlgPluginSettings);
    dlg->setSettings(_accounts[a_id]->settings());
    
    if(dlg->exec() == QDialog::Accepted)       
        emit editAccount(a_id, dlg->settings());
}

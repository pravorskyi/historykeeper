#ifndef PLUGINCONTACT_H
#define PLUGINCONTACT_H

#include <QtCore/QHash>
#include <QtCore/QString>
#include <QtCore/QVariant>

#include "typedefs.h"

// используется только для работы с плагинами (IHistoryImporter)
class PluginContact
{    
public:
    // Тип id контакта
    enum class TypeID {
        Numeric,        // числовой тип aka int64_t
        Login           // в качестве id выступает уникальный логин
    } typeID = TypeID::Numeric;
    
    ContactID id;
    QString login;
    QString name;
    QHash<int32_t, QVariant> attributes;
};

#endif // PLUGINCONTACT_H
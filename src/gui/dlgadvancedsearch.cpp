#include "dlgadvancedsearch.h"
#include "ui_dlgadvancedsearch.h"

DlgAdvancedSearch::DlgAdvancedSearch(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgAdvancedSearch)
{
    ui->setupUi(this);
    connect(ui->btnSearch, &QPushButton::clicked, this, &DlgAdvancedSearch::runSearch);
}

DlgAdvancedSearch::~DlgAdvancedSearch()
{
    
}

void DlgAdvancedSearch::runSearch()
{
    if(ui->rbtnSearchContain->isChecked())
        emit search(SearchType::Contain, ui->edtSearchText->text());
    else if(ui->rbtnSearchRegexp->isChecked())
        emit search(SearchType::Regexp, ui->edtSearchText->text());
    else if(ui->rbtnSearchSimpleMask->isChecked())
        emit search(SearchType::SimpleMask, ui->edtSearchText->text());
}


#include "sync.h"
#include <QtCore/QDebug>
#include <QtWebSockets/QWebSocket>
#include <QtNetwork/QHostAddress>
#include "sync/sync_defines.h"
#include "contact.h"
#include "message.h"
#include "sync.pb.h"
#include <QSettings>

using namespace hksync;

const char SYNC_VERSION[] = "0";

Sync::Sync(QObject *parent) : QObject(parent)
{
    _socket = new QWebSocket();
    _socket->setParent(this);
    connect(_socket, &QWebSocket::connected, this, &Sync::onConnected);
    connect(_socket, &QWebSocket::binaryMessageReceived, this, &Sync::onBinaryMessage);
    connect(_socket, static_cast<void (QWebSocket:: *)(QAbstractSocket::SocketError)>(&QWebSocket::error), this, &Sync::onError);
}


Sync::~Sync()
{
}



void Sync::init()
{
    QSettings s;
    s.beginGroup("Sync");
    qDebug("SYNC: Connecting...");
    _socket->open(QString("ws://%1:%2/sync/%3").arg(s.value("Host", "localhost").toString()).arg(s.value("Port", 8081).toInt()).arg(SYNC_VERSION));
}


void Sync::onConnected()
{
    qDebug() << "SYNC: Connected";
    if(_createlogin) {
        QSettings s;
        s.beginGroup("Sync");
        createAccount(s.value("Login", "").toString(), s.value("Password", "").toString());
        _createlogin = false;
    }
    else
        handshake();
}


void Sync::onError(QAbstractSocket::SocketError socketError)
{
    qDebug() << "SYNC: SOCKET ERROR [" << socketError << "]: " << _socket->errorString();
}


void Sync::createAccount(const QString &login, const QString &password)
{
    _createlogin = true;
    ActionCreateLogin x;
    x.set_login(login.toStdString());
    x.set_password(password.toStdString());
    send(EActionCreateLogin,x);
}


void Sync::handshake()
{
    QSettings s;
    s.beginGroup("Sync");
   
    ActionAuthentication x;
    x.set_login(s.value("Login", "").toString().toUtf8().toStdString());
    x.set_password(s.value("Password", "").toString().toUtf8().toStdString());
    x.set_device(s.value("DeviceName", "").toString().toUtf8().toStdString());
    send(EActionAuthentication, x);
}


void Sync::onBinaryMessage(const QByteArray & message)
{
    qDebug("BINARY MESSAGE");
    
    switch(message[0]) {
    case EResponseCreateLogin: {
        qDebug("RECEIVE ResponseCreateLogin");
        ResponseCreateLogin pb;
        pb.ParseFromArray(message.constData()+1, message.length());
        if(pb.success()) {
            qDebug("SYNC: CreateLogin success");
        } else {
            qDebug("SYNC: CreateLogin failed");
            _socket->close();
        }
        break;
    }
    
    case EResponseAuthentication: {
        qDebug("RECEIVE ResponseAuthentication");
        ResponseAuthentication pb;
        pb.ParseFromArray(message.constData()+1, message.length());
        if(pb.success()) {
            qDebug("SYNC: authentication success");
            syncPull();
            syncPush();
        } else {
            qDebug("SYNC: authentication failed");
            _socket->close();
        }
        break;
    }
    
    
    case EResponseSyncPull: {
        qDebug("RECEIVE ResponseSyncPull");
        ResponseSyncPull r;
        r.ParseFromArray(message.constData()+1, message.length());
        onSyncPull(r);
        break;
    }
    
    case EResponseSyncPush: {
        qDebug("RECEIVE ResponseSyncPush");
        ResponseSyncPush r;
        r.ParseFromArray(message.constData()+1, message.length());
        qDebug() << "SYNC push " << (r.success() ? "success" : "failed");
        break;
    }
            
    }
}


void Sync::syncPull()
{
    ActionSyncPull action;
    action.set_force(false);
    send(EActionSyncPull, action);
}


void Sync::syncPush()
{
    ActionSyncPush action;
    
    QHash<ContactID, SyncContact> contacts;
    
    QMap<QString, Db::Plugin> plugins = _db->plugins();
    for(const Db::Account &account : _db->accounts()) {
        for(const Contact &contact : _db->accountContacts(account.id)) {
            for(const Message &message : _db->messages(account.id, contact.id)) {
                if(!contacts.contains(contact.id)) {
                    contacts[contact.id] = _db->syncContact(account.id, contact.id);
                    contacts[contact.id].set_plugin(_db->plugin(account.pluginID).toUtf8().toStdString());
                    contacts[contact.id].set_account(account.name.toUtf8().toStdString());
                }
                
                QByteArray buf;
                QDataStream stream(&buf, QIODevice::WriteOnly);
                stream << message;
                
                SyncMessage *sm = action.add_message();
                sm->set_data(buf.constData(), buf.length());
                sm->set_id("s");
                sm->set_plugin(_db->plugin(account.pluginID).toUtf8().toStdString());
                sm->set_account(account.name.toUtf8().toStdString());
                sm->set_contact(contacts[contact.id].id());
            }
        }
    }
    
    for(const SyncContact c : contacts) {
        int size = c.ByteSize();
        char* buf = new char[size];
        c.SerializeToArray(buf, size);
        action.add_contact(buf, size);
        delete buf;
    }
    
    send(EActionSyncPush, action);
    qDebug("SYNC: send %d messages", action.message_size());
    qDebug("SYNC: send %d contacts", action.contact_size());
}


void Sync::send(uint8_t type, const ::google::protobuf::Message& message)
{
    if(_socket->state() != QAbstractSocket::ConnectedState) {
        init();
        return;
    }
    
    int size = message.ByteSize();
    qDebug("MESSAGE SIZE %d", size);
    char* buf = new char[size + 1];
    buf[0] = type;
    message.SerializeToArray(buf+1, size);
    
    qint64 send_size = _socket->sendBinaryMessage(QByteArray::fromRawData(buf, size + 1));
    delete buf;
}


void Sync::onSyncPull(const ResponseSyncPull &r)
{
    int size = r.contact_size();
    qDebug("SYNC PULL %d contacts", size);
    for(int i = 0; i < size; ++i) {
        SyncContact c;
        c.ParseFromArray(r.contact(i).c_str(), r.contact(i).size());
        _db->addSyncContact(c);        
    }
   
    
    size = r.message_size();
    qDebug("SYNC PULL %d messages", size);
    for(int i = 0; i < size; ++i) {
        const SyncMessage &sm = r.message(i);
        QByteArray buf = QByteArray::fromRawData(sm.data().c_str(), sm.data().size());
        QDataStream stream(&buf, QIODevice::ReadOnly);
        Message m;
        stream >> m;
    }
    
}


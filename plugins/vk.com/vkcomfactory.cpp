#include "vkcomfactory.h"
#include "dlgvksettings.h"
#include "vkcomhistoryimporter.h"
#include <QtGui/QIcon>

VkComFactory::~VkComFactory()
{
}


bool VkComFactory::hasFeature(HistoryImporter::Features feature) const
{
    using namespace HistoryImporter;
    switch(feature) {
        case Features::AsyncSynchronization:
            return true;
        case Features::ConfigDialog:
            return true;
    }
    return false;
}


IHistoryImporterPtr VkComFactory::create()
{
    return std::shared_ptr<IHistoryImporter>(new VkComHistoryImporter(this));
}


const QString VkComFactory::prettyName() const
{
    return "vk.com";
}


const QString VkComFactory::description() const
{
    return tr("Biggest international social network service in Europe");
}


const QByteArray VkComFactory::systemName() const
{
    return "vk_com";
}

const QString VkComFactory::version() const
{
    return "0.1";
}


DlgPluginSettings* VkComFactory::dlgSettings() const
{
    return new DlgVkSettings();
}


QIcon* VkComFactory::icon() const
{
    QPixmap *pixmap = new QPixmap(":/icons/vkcom_icon.png");
    pixmap->scaled(32, 32, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
    QIcon* icon = new QIcon(*pixmap);
    return icon;
}
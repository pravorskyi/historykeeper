#include "rosterprivate.h"

RosterPrivate::RosterPrivate(QWidget* parent): QTreeWidget(parent)
{
    setContextMenuPolicy(Qt::CustomContextMenu);
    setHeaderHidden(true);
    setDragDropMode(InternalMove);
    setDefaultDropAction(Qt::MoveAction);
    invisibleRootItem()->setFlags(invisibleRootItem()->flags() &= ~Qt::ItemIsDropEnabled);
}


void RosterPrivate::dropEvent(QDropEvent* event)
{
    QTreeWidget::dropEvent(event);
    emit droppedItem(static_cast<RosterContactItem*>(_currentItemForDrop));
}

void RosterPrivate::startDrag(Qt::DropActions supportedActions)
{
    _currentItemForDrop = selectedItems().first();
    QAbstractItemView::startDrag(supportedActions);
}

#include "hkcore.h"
#include "account.h"
#include "message.h"
#include "metacontact.h"
#include "sync.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QPluginLoader>
#include <QtCore/QTranslator>
#include <QRegularExpression>
#include <QtCore/QXmlStreamWriter>
#include <QtCore/QStandardPaths>

#include <iostream>
#include <stdexcept>


HKCore::HKCore(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<QVector<PluginContact>>("QVector<PluginContact>");
    qRegisterMetaType<QVector<PluginMessage>>("QVector<PluginMessage>");

    //Localization
    _translator = unique_ptr<QTranslator>(new QTranslator);
    _translatorQt = unique_ptr<QTranslator>(new QTranslator);

    //Database
    _db = new Db(Db::Type::SQLite);
    _dbThread = new QThread(this);
    _dbThread->setObjectName("database");
    _db->moveToThread(_dbThread);
}

HKCore::~HKCore()
{
    // stop threads
    for(const auto &account : _accounts)
        account->importer()->thread()->quit();

    _dbThread->quit();

    // wait threads
    for(auto &account : _accounts)
        account->importer()->thread()->wait();

    _dbThread->wait();
}

bool HKCore::init()
{
    QString appDataLocation = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    qDebug() << QStandardPaths::displayName(QStandardPaths::AppDataLocation);
    qDebug() << appDataLocation;

    QDir dir(appDataLocation);
    if(!dir.exists()) {
        bool ok = dir.mkpath(appDataLocation);
        if(!ok) {
            qDebug() << " can't create path: " << qPrintable(appDataLocation);
            return false;
        }
    }

    loadLanguages();

    _dbThread->start();
    _db->setDatabaseName(appDataLocation + "/db.sqlite3");

    if(!_db->open()) {
        return false;
    }

    if(!_db->init()) {

        return false;
    }

    if(!_sync) {
        _sync = std::unique_ptr<Sync>(new Sync);
        _sync->_db = _db;
    }


    loadPlugins();

    for(const auto &a : _db->accounts()) {
        if(!_plugins.contains(a.pluginID)) {
            qWarning("%s:%d plugin %s [%d] is not exists", Q_FUNC_INFO, __LINE__, qPrintable(a.name), a.pluginID);
            continue;
        }
        createAccount(a.pluginID, a.id, _db->accountSettings(a.id));
    }
    return true;
}


AccountConstPtr HKCore::account(AccountID id) const
{
    return _accounts.value(id, nullptr);
}


QHash<AccountID, AccountPtr> HKCore::accounts() const
{
    return _accounts;
}


QHash<PluginID, IFactoryPlugin*> HKCore::plugins() const
{
    return _plugins;
}


void HKCore::metacontactRemove(MetacontactID m_id)
{
    _db->deleteMetacontact(m_id);
}


void HKCore::loadLanguages()
{
    // QActionGroup* langGroup = new QActionGroup(ui.menuLanguage);
    //langGroup->setExclusive(true);

    //connect(langGroup, SIGNAL (triggered(QAction *)), this, SLOT (slotLanguageChanged(QAction *)));

    // format systems language
    QString defaultLocale = QLocale::system().name(); // e.g. "uk_UA"
    defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // e.g. "uk"

    QString langPath = QCoreApplication::applicationDirPath() + "/languages";
    QDir dir(langPath);
    QStringList fileNames = dir.entryList(QStringList(QCoreApplication::applicationName() + "_*.qm"));

    for (QString locale : fileNames) {
        // get locale extracted by filename
        // "TranslationExample_de.qm"
        qDebug() << "Load locale: " << qPrintable(locale);
        locale.truncate(locale.lastIndexOf('.')); // "TranslationExample_de"
        locale.remove(0, locale.indexOf('_') + 1); // "de"

        QString lang = QLocale::languageToString(QLocale(locale).language());
        qDebug() << lang;
 //QIcon ico(QString("%1/%2.png").arg(m_langPath).arg(locale));

 //QAction *action = new QAction(ico, lang, this);
 //action->setCheckable(true);
 //action->setData(locale);

 //ui.menuLanguage->addAction(action);
 //langGroup->addAction(action);

 // set default translators and language checked
 //if (defaultLocale == locale)
 //{
 //action->setChecked(true);
 //}
    }
}


void HKCore::loadLanguage(const QString& language)
{
    if(_currentLanguage == language)
        return;

    _currentLanguage = language;
    QLocale locale = QLocale(_currentLanguage);
    QLocale::setDefault(locale);
    QString languageName = QLocale::languageToString(locale.language());
    switchTranslator(*_translator, QString("%1_%2.qm").arg(QCoreApplication::applicationName(), language));
    switchTranslator(*_translatorQt, QString("qt_%1.qm").arg(language));
    //ui.statusBar->showMessage(tr("Current Language changed to %1").arg(languageName));
}


void HKCore::switchTranslator(QTranslator& translator, const QString& filename)
{
    qApp->removeTranslator(&translator);
    if(translator.load(filename))
        qApp->installTranslator(&translator);
}



void HKCore::loadPlugins()
{
    auto plugins = _db->plugins();


    QStringList pluginsDirs = {
#ifdef Q_OS_UNIX
        QString("/usr/lib/%1/plugins").arg(QCoreApplication::applicationName()),
#elif Q_OS_WIN
        qApp->applicationDirPath() + "/plugins",
#endif
        QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/plugins"
    };

    qDebug() << pluginsDirs;

    //std::cout << "Try to load plugins in folder " << pluginsDir.path().toStdString() << std::endl;

    for(const QString& path : pluginsDirs) {
        QDir dir(path);
        if(!dir.exists())
            continue;
        for (const QString &fileName : dir.entryList(QDir::Files))
            loadPlugin(fileName, dir, plugins);
    }

    //pluginsDir=QDir::current();
    //pluginsDir.cd("plugins");
    //std::cout << "Try to load plugins in folder " << pluginsDir.path().toStdString() << std::endl;

    /*for (const QString &fileName : pluginsDir.entryList(QStringList({"lib*Importer*.dll", "lib*Importer*.so"}), QDir::Files))
        loadPlugin(fileName, pluginsDir, plugins);

//TODO: remove/replace this snippet
    pluginsDir=QDir(QDir::current().path() + "/../../plugins/");
    for (const QString &dirName : pluginsDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QDir subdir(pluginsDir.path() + "/" + dirName );
        //std::cout << "Try to load plugins in folder " << subdir.path().toStdString() << std::endl;
        for(const QString &fileName : subdir.entryList(QStringList({"lib*Importer*.dll", "lib*Importer*.so"}), QDir::Files))
            loadPlugin(fileName, subdir, plugins);
    }*/
}

void HKCore::loadPlugin(const QString &filename, const QDir &dir, const QMap<QString, Db::Plugin> &plugins)
{
    qDebug("Testing %s", qPrintable(dir.absoluteFilePath(filename)));
    QPluginLoader loader(dir.absoluteFilePath(filename));
    QObject *plugin = loader.instance();
    if (plugin) {
        qDebug("Loading plugin: %s", qPrintable(filename));
        initializePlugin(plugin, filename, plugins);
    }
    else {
        //TODO: emit error
        std::cout << Q_FUNC_INFO << ':' << __LINE__ << " " << loader.errorString().toStdString() << std::endl;
    }
}

void HKCore::initializePlugin(QObject *pluginObject, QString filename, const QMap<QString, Db::Plugin> &plugins)
{
    IFactoryPlugin *plugin = qobject_cast<IFactoryPlugin*>(pluginObject);
    if (!plugin) {
        std::cerr << filename.toStdString() << " is not valid plugin " << std::endl;
        delete pluginObject;
        pluginObject = nullptr;
        return;
    }

    QByteArray systemName = plugin->systemName();
    // validate systemName
    static const QRegularExpression re("^[a-z_]*$");
    QRegularExpressionMatch match = re.match(systemName);
    if(!match.hasMatch()) {
        std::cerr << filename.toStdString() << ": invalid system name '" << systemName.constData() << '\'' << std::endl;
        delete plugin;
        plugin = nullptr;
        pluginObject = nullptr;
        return;
    };

     // now the plugin can be initialized and used
    if(plugins.contains(systemName)) {
        auto p = plugins.value(systemName);
        if(!p.enabled) {
            delete plugin;
            plugin = nullptr;
            pluginObject = nullptr;
            return;
        }
        plugin->pluginId = p.id;
    } else {
        plugin->pluginId = _db->addPlugin(systemName, plugin->version());
    }

    _db->createPluginTables(plugin->pluginId);
    _plugins.insert(plugin->pluginId, plugin);
}


void HKCore::syncAllAccounts()
{
    for(auto &account: _accounts) {
        QMetaObject::invokeMethod(account->importer().get(), "sync", Qt::QueuedConnection);
    }
}


void HKCore::syncAccount(AccountID id)
{
    QMetaObject::invokeMethod(_accounts.value(id)->importer().get(), "sync", Qt::QueuedConnection);
}


void HKCore::createAccount(PluginID p_id, AccountID a_id, const AccountSettings &settings)
{
    IFactoryPlugin* plugin = _plugins[p_id];

    AccountPtr account = std::make_shared<Account>(a_id, plugin->create());
    QThread *thread = new QThread(this);
    account->importer()->moveToThread(thread);
    thread->start();

    thread->setObjectName("a:" + settings.value(ACCOUNT_SETTING_COMMON).value("Name")); //TODO: replace literals with static const vars
    account->setSettings(settings);

    _accounts[a_id] = account;
    _accountsThreads[a_id] = thread;
    _accountsImporters[account->importer().get()] = a_id;
    _contacts[a_id] = _db->accountContacts(a_id);

    account->importer()->init();

    connect(account->importer().get(), &IHistoryImporter::sigError, this, &HKCore::onAccountError);
    connect(account->importer().get(), &IHistoryImporter::newContacts, this, &HKCore::onAccountNewContacts);
    connect(account->importer().get(), &IHistoryImporter::newMessages, this, &HKCore::onAccountNewMessages);
    connect(account->importer().get(), &IHistoryImporter::importStarted, this, &HKCore::onAccountImportStarted);
    connect(account->importer().get(), &IHistoryImporter::importProgress, this, &HKCore::onAccountImportProgress);
    connect(account->importer().get(), &IHistoryImporter::importFinished, this, &HKCore::onAccountImportFinished);
    sigAccountAdd(a_id);
}


void HKCore::addAccount(PluginID p_id, AccountSettings settings)
{
    AccountID a_id = _db->addAccount(p_id, settings);
    if(a_id == -1)
        return;

    createAccount(p_id, a_id, settings);
}


void HKCore::onAccountError(const QString errorString)
{
    IHistoryImporter* a = static_cast<IHistoryImporter*>(sender());
    emit sigAccountError(_accountsImporters.value(a), errorString);
}


void HKCore::deleteAccount(AccountID id)
{
    if(!_db->deleteAccount(id))
        throw std::runtime_error("Can't delete account!");
    _accounts.remove(id);
    emit sigAccountDeleted(id);
}


void HKCore::editAccount(AccountID a_id, AccountSettings settings)
{
    _db->editAccount(a_id, settings);
    _accounts[a_id]->setSettings(settings);
}


QHash<MetacontactID, Metacontact> HKCore::metacontacts() const
{
    return _db->metacontacts();
}

QString HKCore::getHtml(const QVector<Message> &list) const
{
    QString msg;
    if(list.empty()) {
        msg.append((QString("<font color=gray>%1</font>").arg(tr("Messages not found"))));
        return msg;
    }
    QDate date(0,0,0);
    for(const Message &m : list) {
        if(m.date.date() > date) {
            date = m.date.date();
            msg.append(QString("<font color=darkgreen><strong>=== %1 ===</strong></font><br>").arg(date.toString()));
        }

        if(m.out) {
            msg.append(QString("<font color=blue>[%1] %2: </font>").arg(m.date.toString("hh:mm:ss"), "You"));
        }
        else {
            //FIXME:
            /*Contact author = _contacts.value(m.plugin_id).value(m.contact_id);
            qDebug() << m.contact_id;
            msg.append(QString("<font color=red>[%1] %2: </font> ").arg(m.date.toString("hh:mm:ss"), author.name));
            */
        }

        QString body = m.body;
        body.replace('\n', "<br>");
        body.replace(QRegExp("((?:https?|ftp)://\\S+)"),  "<a href=\"\\1\">\\1</a>");
        msg.append(body);
        msg.append("<br>");
    }
    return msg;
}

QString HKCore::getXML(const QVector<Message> &list) const
{
    QString data;
    QXmlStreamWriter stream(&data);
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    stream.writeStartElement("messages");

    for(const Message &m : list) {
        stream.writeStartElement("message");
        //FIXME: stream.writeAttribute("protocol", _plugins.value(m.plugin_id)->prettyName());
        //FIXME: stream.writeTextElement("contact",  m.out ? "You" : _contacts.value(m.plugin_id).value(m.contact_id).name );
        stream.writeTextElement("datetime", m.date.toString("yyyy-MM-dd hh:mm:ss"));
        stream.writeTextElement("title", m.title);
        stream.writeTextElement("body", m.body);
        stream.writeEndElement();
    }

    stream.writeEndElement();
    stream.writeEndDocument();
    return data;
}

QString HKCore::getJSON(const QVector<Message> &list) const
{

    QJsonArray jmessages;

    for(const Message &m : list) {
        QJsonObject jmessage;
        //FIXME: QJsonValue jprotocol(_plugins.value(m.plugin_id)->prettyName());
        QJsonValue jbody(m.body);

        //FIXME: jmessage.insert("protocol", jprotocol);
        jmessage.insert("body", jbody);
        jmessages.append(QJsonValue(jmessage));
    }

    QJsonDocument jdoc(jmessages);
    return jdoc.toJson(QJsonDocument::Indented);
}


void HKCore::onAccountNewContacts(QVector<PluginContact> contacts)
{
    IHistoryImporter* importer = static_cast<IHistoryImporter*>(sender());
    _db->addContacts(_accountsImporters.value(importer), contacts);
}


void HKCore::onAccountNewMessages(QVector<PluginMessage> messages)
{
    IHistoryImporter* importer = static_cast<IHistoryImporter*>(sender());
    _db->addMessages(_accountsImporters.value(importer), messages);
}


void HKCore::onAccountImportStarted()
{
    IHistoryImporter* importer = static_cast<IHistoryImporter*>(sender());
    emit sigAccountImportStarted(_accountsImporters.value(importer));
}


void HKCore::onAccountImportProgress(int8_t progress)
{
    IHistoryImporter* importer = static_cast<IHistoryImporter*>(sender());
    emit sigAccountImportProgress(_accountsImporters.value(importer), progress);
}


void HKCore::onAccountImportFinished()
{
    IHistoryImporter* importer = static_cast<IHistoryImporter*>(sender());
    AccountID a_id = _accountsImporters.value(importer);
    _contacts[a_id] = _db->accountContacts(a_id);
    emit sigAccountImportFinished(a_id);
}


QHash<ContactID, Contact> HKCore::accountContacts(AccountID id) const
{
    return _contacts[id];
}


QVector<Message> HKCore::messages(AccountID accountId, ContactID contactId) const
{
    return _db->messages(accountId, contactId);
}


int HKCore::addMetacontact(const QString& name)
{
    return  _db->addMetacontact(name);
}


void HKCore::metacontactAddContact(MetacontactID metacontactId, AccountID accountId, ContactID contactId)
{
    _db->metacontactAddContact(metacontactId, accountId, contactId);
}


QHash<AccountID, QHash<ContactID, Contact>> HKCore::metacontactContacts(MetacontactID m_id)
{
    QHash<AccountID, QHash<ContactID, Contact>> contacts;
    Metacontact m = _db->metacontact(m_id);
    for(const AccountID &a_id : m.contacts.keys())
        contacts[a_id] = _db->accountContacts(a_id);
    return contacts;
}


QVector<Message> HKCore::metacontactMessages(MetacontactID m_id)
{
    QVector<Message> messages;
    Metacontact m = _db->metacontact(m_id);

    for(auto it = m.contacts.constBegin(); it != m.contacts.constEnd(); ++it) {
        AccountID a_id = it.key();
        for(const ContactID c_id : it.value())
            messages << _db->messages(a_id, c_id);
    }

    qStableSort(messages.begin(), messages.end(), [](const Message &a,const Message &b){return a.date < b.date;} );

    return messages;
}


QIcon* HKCore::accountIcon(AccountID id) const
{
    return _accounts[id]->importer()->plugin()->icon();
}


void HKCore::deleteAllMessages(AccountID a_id)
{
    _db->deleteAllMessages(a_id);
}


void HKCore::syncDevices()
{
    qDebug() << "Sync started.";

    if(!_sync) {
        _sync = std::unique_ptr<Sync>(new Sync);
        _sync->_db = _db;
    }

    _sync->init();
}


void HKCore::createSyncAccount(const QString &login, const QString &password)
{
    _sync->createAccount(login, password);
}

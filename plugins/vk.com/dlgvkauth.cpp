#include "dlgvkauth.h"
#include "ui_dlgvkauth.h"
#include <iostream>

#include <QtCore/QUrlQuery>
#include <QtCore/QSettings>

const int VKCOM_APP_ID = 0000000;
const char VKCOM_API_VERSION[] = "5.2";
const char VKCOM_URI_REDIRECT[] = "https://oauth.vk.com/blank.html";
const char VKCOM_SETTINGS[] = "friends,messages";

DlgVkAuth::DlgVkAuth(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgVkAuth)
{
    ui->setupUi(this);
    connect(ui->webView, &QWebView::urlChanged, this, &DlgVkAuth::onUrlChanged);
}

DlgVkAuth::~DlgVkAuth()
{
    delete ui;
}

void DlgVkAuth::loadAuthPage()
{
    const QString query = QString("https://oauth.vk.com/authorize?"
                                  "client_id=%1&"
                                  "scope=%2&"
                                  "redirect_uri=%3&"
                                  "display=popup&"
                                  "v=%4&"
                                  "response_type=token")
        .arg(VKCOM_APP_ID).arg(VKCOM_SETTINGS).arg(VKCOM_URI_REDIRECT).arg(VKCOM_API_VERSION);
    ui->webView->load(QUrl(query));
    qDebug("DEBUG loading auth page %s", qPrintable(query));
}

void DlgVkAuth::onUrlChanged()
{
    const QUrl url = ui->webView->url();
    qDebug("%s", qPrintable(url.toString()));
    qDebug("HOST %s", qPrintable(url.host()));
    qDebug("PATH %s", qPrintable(url.path()));
    if(url.host() == "oauth.vk.com" && url.path() == "/blank.html") {
        qDebug("PARSE PARAMS");
        qDebug("FRAGMENT %s", qPrintable(url.fragment()));
        QUrl urlFragment("/?"+url.fragment());
        QUrlQuery urlQuery(urlFragment.query());

        if(urlQuery.hasQueryItem("error")) {
            m_ErrorDescription = urlQuery.queryItemValue("error_description");
            reject();
        } else {
            m_AccessToken = urlQuery.queryItemValue("access_token");
            m_UserID = urlQuery.queryItemValue("user_id");
            accept();
        }
       //qDebug("EXPIRES_IN %s", qPrintable(urlFragment.queryItemValue("expires_in")))
    }
}


int DlgVkAuth::exec()
{
    loadAuthPage();
    return QDialog::exec();
}

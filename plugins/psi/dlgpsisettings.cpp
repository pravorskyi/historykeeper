#include "dlgpsisettings.h"
#include "ui_dlgpsisettings.h"

#include <QtCore/QSettings>
#include <QtWidgets/QFileDialog>

DlgPsiSettings::DlgPsiSettings(QWidget *parent) :
    DlgPluginSettings(parent),
    ui(new Ui::DlgPsiSettings)
{
    ui->setupUi(this);
}

DlgPsiSettings::~DlgPsiSettings()
{
    delete ui;
}


bool DlgPsiSettings::isValid() const
{
    return true;
}


QMap<QString, QString> DlgPsiSettings::settings() const
{
    QMap<QString, QString> map;
    
    
    return map;
}


void DlgPsiSettings::setSettings(const QMap<QString, QString> &)
{
}

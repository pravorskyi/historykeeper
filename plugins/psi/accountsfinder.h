#ifndef ACCOUNTSFINDER_H
#define ACCOUNTSFINDER_H

#include <QtCore/QString>

class AccountsFinder
{
public:
    explicit AccountsFinder();
    
    QVector<QString> accounts();
    
private:
    QString _pathAccountsXml;
    QString _pathHistory;
};

#endif // ACCOUNTSFINDER_H
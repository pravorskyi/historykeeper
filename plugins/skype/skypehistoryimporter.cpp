#include "skypehistoryimporter.h"

#include <iostream>

#include <QtCore/QDir>
#include <QtCore/QRegularExpression>
#include <QtCore/QUrl>
#include <QtCore/QVector>
#include <QtCore/QSettings>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlDatabase>
#include <QtCore/QLinkedList>


SkypeHistoryImporter::SkypeHistoryImporter(const IFactoryPlugin *parent) :
    IHistoryImporter(parent)
{
}


SkypeHistoryImporter::~SkypeHistoryImporter()
{
}


void SkypeHistoryImporter::setSettings(const QMap<QByteArray, QString>& settings)
{
    _path = settings["Path"];
}


QVector<PluginMessage> SkypeHistoryImporter::importMessages(const QSqlDatabase &db, const QString &account)
{
    QVector<PluginMessage> list;   
    QSqlQuery q(db);
    QString qStr = "SELECT id, author, dialog_partner, body_xml, timestamp FROM messages";
    if(!q.prepare(qStr)) {
        qWarning("%s:%d %s", Q_FUNC_INFO, __LINE__, qPrintable(q.lastError().text()));
        return list;
    }
    
    if(!q.exec()) {
        qWarning("%s:%d %s", Q_FUNC_INFO, __LINE__, qPrintable(q.lastError().text()));
        return list;
    }
    while(q.next()) {
        PluginMessage m;
        m.typeID = PluginMessage::TypeID::Numeric;
        m.id = q.value(0).toInt();
        m.contact_id = -1;
        m.login = q.value(2).toString();
        m.body = q.value(3).toString();
        m.date = QDateTime::fromTime_t(q.value(4).toInt());
        m.out = (m.login != q.value(1).toString());
        std::cout << m.login.toStdString() << std::endl;
        list.append(m);
    }
    
    return list;
}

QVector<PluginContact> SkypeHistoryImporter::importContacts(const QSqlDatabase &db, const QString &account)
{
    QVector<PluginContact> list;   
    QSqlQuery q(db);
    QString qStr = "SELECT skypename, fullname, displayname FROM contacts";
    q.prepare(qStr);
    
    if(!q.exec()) {
        qWarning("%s:%d %s", Q_FUNC_INFO, __LINE__, qPrintable(q.lastError().text()));
        return list;
    }
    while(q.next()) {
        PluginContact c;
        c.typeID = PluginContact::TypeID::Login;
        c.login = q.value(0).toString();
        c.name = q.value(2).toString() + " [" + q.value(1).toString() + "]";
        list.append(c);
    }

    PluginContact me;
    me.typeID = PluginContact::TypeID::Login;
    me.login = account;
    me.name = account;
    list.append(me);
    
    return list;
}

void SkypeHistoryImporter::sync()
{
    emit importStarted();
    QString filename = _path + "/main.db";
    std::cout << "sync " << filename.toStdString() << std::endl;

    
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "SkypeDB");
    db.setDatabaseName(filename);
    
    if(!db.open()) {
        std::cerr << Q_FUNC_INFO << ":" << __LINE__ 
            << " can't open db ' <<" << filename.toStdString() << "': "
            << db.lastError().text().toStdString() << std::endl;
        return;
    }
    
    QString accountName = QFileInfo(_path).fileName();
    QVector<PluginContact> contacts = importContacts(db, accountName);
    QVector<PluginMessage> messages = importMessages(db, accountName);
    emit newContacts(contacts);
    emit newMessages(messages);
        
    db.close();
    emit importFinished();
}
